using System.Collections.Generic;
using Ce.Core.Ecs;
using Ce.Game.Components.Enum;
using UnityEngine;

namespace Ce.Game.World
{

    public sealed class Tile: Addressable
    {
        public Propbits Propbits { get; private set; }
        
        private readonly List<Entity> _entities = new List<Entity>();

        public void LinkEntity(Entity entity) {
            _entities.Add(entity);
            RegeneratePropbits();
        }

        public void UnlinkEntity(Entity entity) {
            _entities.Remove(entity);
            RegeneratePropbits();
        }
        
        private void RegeneratePropbits() {
            Propbits = Propbits.None;
            foreach (var entity in _entities) {
                if (!entity.HasPropbits()) {
                    Debug.Log(entity);
                    continue;
                }
                Propbits |= entity.GetPropbits();
            }
        }

        public IEnumerable<Entity> Entities => _entities;
    }
}