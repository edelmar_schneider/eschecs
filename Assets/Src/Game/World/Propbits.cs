using System;

namespace Ce.Game.World
{
    /// <summary>
    /// Propbits = Property bits, they define the main properties of an object and they are transferred to the tile working as a cache
    /// so you can easily consult the properties of the entities attached to the tile 
    /// </summary>
    [Flags]
    public enum Propbits: int
    {
        /// <summary> Any item that doesn't have any practical purpose (doesn't affect the actor in any way)</summary>
        None         = 0,
        Actor        = 1 << 0,
        Plant        = 1 << 1,
        Resource     = 1 << 2,
        Blocks       = 1 << 3,
        Building     = 1 << 4,
        Walkable     = 1 << 5,
        Workable     = 1 << 6,
        Area         = 1 << 7,
    }

    public static class PropbitsHelperMethods
    {
        public static bool HasAny(this Propbits value, Propbits mask) {
            return (mask & value) != Propbits.None;
        }
        
        public static bool HasAll(this Propbits value, Propbits mask) {
            return (mask & value) == mask;
        }
        
        public static bool HasNone(this Propbits value, Propbits mask) {
            return (mask & value) == Propbits.None;
        }
        
        
    }
}