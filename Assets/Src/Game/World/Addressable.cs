using Ce.Core.World;

namespace Ce.Game.World {

    public class Addressable {
        
        /// <summary>
        /// The address of an addressable element on a board. 
        /// </summary>
        public Address Address { get; private set; }
        
        public Addressable() { }

        public void Setup(Address address) => Address = address;
        
    }

}