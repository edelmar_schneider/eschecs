using System.Collections.Generic;
using Ce.Core.World;

namespace Ce.Game.World
{
    public class Board<TElement> where TElement:  Addressable, new()
    {
        public BoardParam Param { get; set; }

        private readonly TElement[] _elements;
        
        public Board(BoardParam param) {

            Param = param;
            
            var width = param.Width;
            var depth = param.Depth;
            var height = param.Height;

            var capacity = width * depth * height;
            _elements = new TElement[capacity];

            for (var y = 0; y < height; y++) {
                for (var z = 0; z < depth; z++) {
                    for (var x = 0; x < width; x++) {
                        var address = new Address(x, y, z);
                        
                        var element = new TElement();
                        element.Setup(address);
                        
                        var id = param.ToId(address);
                        _elements[id] = element;
                    }
                }
            }
        }

        public TElement GetElement(int id) {
            return _elements[id];
        }

        public TElement GetElement(Address address) {
            var id = Param.ToId(address);
            return GetElement(id);
        }

        public IEnumerable<TElement> GetAllElements() {
            return _elements;
        }
        
    }
}