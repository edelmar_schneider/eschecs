using Ce.Core.Extensions;

namespace Ce.Game {



    public static class GameConstants {

        public static class GameParameters {
            public static readonly float DistancePerAp = 0.4f; //"meters" you walk per action point
            
            public static readonly float ApPerAttack = 40f; //action points per attack.
            
            public static readonly float ApPerBuild = 90f; //action points per construction
        }

        public static class NamedGroups {
            
            public static readonly string HitPointEntities = "HitPointEntities";
            
            public static readonly string SelectedEntities = "SelectedEntities";
            
            public static readonly string Gods = "Gods";
            
            public static readonly string GameDefinitions = "GameDefinitions";
            
            public static readonly string LocalPlayer = "LocalPlayer";
            
            public static readonly string ServerSideEntity = "ServerSideEntity";
        }

        public static class Resources {
            
            /// <summary>
            /// App resources. Usually menus and pieces of UI.
            /// </summary>
            public static readonly string App =  "App";
            
            /// <summary>
            /// Game resources. In general this should be 3d prefabs used across the game.
            /// </summary>
            public static readonly string Game = "Game";
            
            /// <summary>
            /// These are things that can be feed into the prefabs like portraits, 2d images or sounds in general, etc.
            /// </summary>
            public static readonly string Media = "Media";

            /// <summary>
            /// The root folder for the UI media
            /// </summary>
            public static readonly string UiFolder = App.AppendPath("Ui");
        }
    }
}