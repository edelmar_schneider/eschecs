using System;
using Ce.Core.Ecs;
using Ce.Core.Network;
using Ce.Core.System;
using Ce.Core.System.Job;
using Ce.Core.Util;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.Tags;
using Ce.Game.Models;
using Ce.Game.System;
using Ce.Game.System.Interaction;
using UnityEngine;

using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game
{
    /// <summary>
    /// This is where the game starts to run. The main script starts all other systems
    /// </summary>
    public class Main : MonoBehaviour {

        /// <summary>
        /// Set the actual object in the inspector.
        /// </summary>
        public ProxyNetTransport netTransport;

        /// <summary>
        /// Set the NetworkBehaviour in the inspector.
        /// </summary>
        public ComInterface comInterface;

        public Universe Universe { get; private set; }
        
        /// <summary>
        /// Main can be enabled/disabled by switching the enable/disable tick box in the inspector. 
        /// </summary>
        public bool MainEnabled { get; private set; }

        private TimeSystem _timeSystem;

        
        
        private void OnEnable() {
            if (comInterface == null) {
                throw new Exception("Set the ComInterface object in the inspector");
            }
            
            if (MainEnabled) {
                return;
            }
            MainEnabled = true;
            
            Universe = new Universe(GameComponents.Components);

            Universe.RegisterModels<AppEntityModels>();
            Universe.RegisterModels<GameEntityModels>();
            
            Universe.RegisterGroup(Group.New(Matcher.New(Universe).All<LocalPlayerComponent>()), GCG.LocalPlayer);
            Universe.RegisterGroup(Group.New(Universe.GetModels<AppEntityModels>().GameDefinitionAny), GCG.GameDefinitions );
            Universe.RegisterGroup(Group.New(Matcher.New(Universe).All<SelectedToGodComponent>()), GCG.SelectedEntities);
            Universe.RegisterGroup(Group.New(Matcher.New(Universe).All<GodComponent>()), GCG.Gods);
            Universe.RegisterGroup(Group.New(Matcher.New(Universe).All<HitPointsComponent>()), GCG.HitPointEntities);            
            Universe.RegisterGroup(Group.New(Matcher.New(Universe).All<ServerSideComponent>()), GCG.ServerSideEntity);            

            Universe.RegisterSystem<TimeSystem>()
                    .RegisterSystem<MessageSystem>()
                    .RegisterSystem<GameNetworkSystem>();
            
            var ns = Universe.GetSystem<GameNetworkSystem>();
            comInterface.TimeSystem = Universe.GetSystem<TimeSystem>();
            comInterface.MessageSystem = Universe.GetSystem<MessageSystem>();
            ns.ComInterface = comInterface;
            ns.NetTransport = netTransport;

            Universe.RegisterSystem<JobSystem>()
                    .RegisterSystem<MainUiSystem>()
                    .RegisterSystem<GameSystem>()
                    .RegisterSystem<GameObjectSystem>()
                    .RegisterSystem<BoardSystem>()
                    .RegisterSystem<CameraSystem>()
                    .RegisterSystem<InteractionSystem>()
                    .RegisterSystem<InputSystem>()
                    .RegisterSystem<SelectionDisplaySystem>()
                    .RegisterSystem<MovementRangeDisplaySystem>()
                    .RegisterSystem<PathfinderSystem>()
                    .RegisterSystem<ActionSystem>()
                    .RegisterSystem<TurnControlSystem>()
                    .RegisterSystem<CombatSystem>()
                    .RegisterSystem<BuildSystem>()
                    .RegisterSystem<VictoryConditionSystem>()
                    .RegisterSystem<SaveLoadSystem>();

            _timeSystem = Universe.GetSystem<TimeSystem>();
            
            //This will kick-start the process and bring up the game menu 
            Universe.GetModels<AppEntityModels>()
                    .GameDefinitionEmpty
                    .MakeEntity(Universe);
        }



        private void OnDisable() {
            if (!MainEnabled) {
                return;
            }
            MainEnabled = false;
            
            Universe.Terminate();
        }
        
        private void FixedUpdate() {
            _timeSystem.FixedUpdate();;
        }

        private void Update() {
            _timeSystem.Update();
        }
    }
}
