using Ce.Core.Message.Command;
using Ce.Core.Message.Request;
using Ce.Game.Message.Abstract;
using Ce.Game.Message.Command;

namespace Ce.Game.Message.Request {

    public class ActorBuildRequest: BuildMessage, IRequest {

        public ActorBuildRequest() { }
        
        public ActorBuildRequest(ulong actorRid) : base(actorRid) { }

        public ICommand MakeCommand() => new ActorBuildCommand(ActorRid);

        public bool CanMakeCommand() => true;
    }

}