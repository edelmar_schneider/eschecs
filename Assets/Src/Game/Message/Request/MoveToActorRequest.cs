using Ce.Core.Message.Command;
using Ce.Core.Message.Request;
using Ce.Game.Message.Abstract;
using Ce.Game.Message.Command;

namespace Ce.Game.Message.Request {

    public class MoveToActorRequest : MoveToMessage, IRequest {

        public MoveToActorRequest() { }

        public MoveToActorRequest(ulong actorRid, int fromAdrId, int toAdrId) : base(actorRid, fromAdrId, toAdrId) { }

        public ICommand MakeCommand() => new MoveToActorCommand(ActorRid, FromAdrId, ToAdrId );

        public bool CanMakeCommand() => true;


    }
}