using Ce.Core.Message.Command;
using Ce.Core.Message.Request;
using Ce.Game.Message.Abstract;
using Ce.Game.Message.Command;

namespace Ce.Game.Message.Request {

    public class GodPassTurnRequest: GodPassTurnMessage, IRequest {
        public GodPassTurnRequest() { }

        public GodPassTurnRequest(ulong godRid) : base(godRid) { }

        public ICommand MakeCommand() => new GodPassTurnCommand(GodRid);

        public bool CanMakeCommand() => true;


    }

}