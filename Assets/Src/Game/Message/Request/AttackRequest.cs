using Ce.Core.Message.Command;
using Ce.Core.Message.Request;
using Ce.Game.Message.Abstract;
using Ce.Game.Message.Command;

namespace Ce.Game.Message.Request {

    public class AttackRequest: AttackMessage, IRequest {

        public AttackRequest() { }
        public AttackRequest(ulong actorRid, ulong defenderRid, int damage) : base(actorRid, defenderRid, damage) { }

        public ICommand MakeCommand() => new AttackCommand(ActorRid, DefenderRid, Damage);

        public bool CanMakeCommand() => true;
    }

}