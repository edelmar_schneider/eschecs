using Ce.Core.Message.Command;
using Ce.Core.Message.Request;
using Ce.Game.Message.Abstract;
using Ce.Game.Message.Command;

namespace Ce.Game.Message.Request {

    public class SaveLoadGameRequest: SaveLoadGameMessage, IRequest {

        public SaveLoadGameRequest() { }
        public SaveLoadGameRequest(string fileName, bool isSave) : base(fileName, isSave) { }

        public ICommand MakeCommand() => new SaveLoadGameCommand(FileName, IsSave);

        public bool CanMakeCommand() => true;

    }

}