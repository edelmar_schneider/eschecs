using Ce.Core.Message;
using Ce.Core.Message.Command;

namespace Ce.Game.Message.Request {

    /// <summary>
    /// The server uses this command when it decides that it is time to pass the turn.
    /// </summary>
    public class PassTurnCommand : AMessage, ICommand{
        
        
    }

}