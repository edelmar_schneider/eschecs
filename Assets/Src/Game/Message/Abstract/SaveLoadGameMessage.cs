using System.Xml.Linq;
using Ce.Core.Message;

namespace Ce.Game.Message.Abstract {

    public abstract class SaveLoadGameMessage : AMessage
    {
        public string FileName { get; set; }
        
        public bool IsSave { get; set; }

        protected SaveLoadGameMessage() { }

        protected SaveLoadGameMessage(string fileName, bool isSave) {
            FileName = fileName;
            IsSave = isSave;
        }

        public override XElement ToXml(string elementName) {

            var e = base.ToXml(elementName);

            e.Add(new XAttribute("FileName", FileName));
            e.Add(new XAttribute("IsSave", IsSave.ToString()));
            

            return e;
        }

        public override void FromXml(XElement element) {
            base.FromXml(element);

            var a1 = element.Attribute("FileName");
            var a2 = element.Attribute("IsSave");

            FileName = a1.Value;
            IsSave = bool.Parse(a2.Value);
        }        
    }

}