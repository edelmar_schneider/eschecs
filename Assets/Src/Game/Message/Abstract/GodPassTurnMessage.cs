using System.Xml.Linq;
using Ce.Core.Message;

namespace Ce.Game.Message.Abstract {

    public abstract class GodPassTurnMessage : AMessage
    {
        public ulong GodRid { get; set; }
        protected GodPassTurnMessage() { }

        public GodPassTurnMessage(ulong godRid) {
            GodRid = godRid;
        }

        public override XElement ToXml(string elementName) {

            var e = base.ToXml(elementName);

            e.Add(new XAttribute("GodRid", GodRid.ToString()));

            return e;
        }

        public override void FromXml(XElement element) {
            base.FromXml(element);

            var godRidAttribute = element.Attribute("GodRid");
            GodRid = ulong.Parse(godRidAttribute.Value);
        }        
    }

}