using System.Xml.Linq;
using Ce.Core.Message;

namespace Ce.Game.Message.Abstract {

    public class BuildMessage : AMessage
    {
        public ulong ActorRid { get; set; }
        
        public ulong NewEntityRid { get; set; }

        public BuildMessage() { }
        
        public BuildMessage(ulong actorRid) => ActorRid = actorRid;

        public override XElement ToXml(string elementName) {

            var e = base.ToXml(elementName);

            e.Add(new XAttribute("ActorRid", ActorRid.ToString()));
            e.Add(new XAttribute("NewEntityRid", NewEntityRid.ToString()));

            return e;
        }

        public override void FromXml(XElement element) {
            base.FromXml(element);

            var actorRidAttribute = element.Attribute("ActorRid");
            var newEntityRidAttribute = element.Attribute("NewEntityRid");

            ActorRid = ulong.Parse(actorRidAttribute.Value);
            NewEntityRid = ulong.Parse(newEntityRidAttribute.Value);
        }        
    }

}