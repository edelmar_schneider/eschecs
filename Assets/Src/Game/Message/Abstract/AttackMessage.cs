using System.Xml.Linq;
using Ce.Core.Message;

namespace Ce.Game.Message.Abstract {

    public abstract class AttackMessage : AMessage
    {
        public ulong ActorRid { get; set; }
        public ulong DefenderRid { get; set; }
        
        public int Damage { get; set; }
        
        protected AttackMessage() { }

        protected AttackMessage(ulong actorRid, ulong defenderRid, int damage) {
            ActorRid = actorRid;
            DefenderRid = defenderRid;
            Damage = damage;
        }

        public override XElement ToXml(string elementName) {

            var e = base.ToXml(elementName);

            e.Add(new XAttribute("ActorRid", ActorRid.ToString()));
            e.Add(new XAttribute("DefenderRid", DefenderRid.ToString()));
            e.Add(new XAttribute("Damage", Damage.ToString()));

            return e;
        }

        public override void FromXml(XElement element) {
            base.FromXml(element);

            var actorRidAttribute = element.Attribute("ActorRid");
            var defenderRidAttribute = element.Attribute("DefenderRid");
            var damage = element.Attribute("Damage");

            ActorRid = ulong.Parse(actorRidAttribute.Value);
            DefenderRid = ulong.Parse(defenderRidAttribute.Value);
            Damage = int.Parse(damage.Value);
        }        
    }

}