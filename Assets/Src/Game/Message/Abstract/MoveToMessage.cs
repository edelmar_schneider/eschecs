using System.Xml.Linq;
using Ce.Core.Message;

namespace Ce.Game.Message.Abstract {

    public abstract class MoveToMessage : AMessage
    {
        public ulong ActorRid { get; set; }
        public int FromAdrId { get; set; }
        public int ToAdrId { get; set; }

        protected MoveToMessage() { }

        public MoveToMessage(ulong actorRid, int fromAdrId, int toAdrId) {
            ActorRid = actorRid;
            FromAdrId = fromAdrId;
            ToAdrId = toAdrId;
        }


        public override XElement ToXml(string elementName) {

            var e = base.ToXml(elementName);

            e.Add(new XAttribute("ActorRid", ActorRid.ToString()));
            e.Add(new XAttribute("FromAdrId", FromAdrId.ToString()));
            e.Add(new XAttribute("ToAdrId", ToAdrId.ToString()));

            return e;
        }

        public override void FromXml(XElement element) {
            base.FromXml(element);

            var actorRidAttribute = element.Attribute("ActorRid");
            var fromAdrIdAttribute = element.Attribute("FromAdrId");
            var toAdrIdAttribute = element.Attribute("ToAdrId");

            ActorRid = ulong.Parse(actorRidAttribute.Value);
            FromAdrId = int.Parse(fromAdrIdAttribute.Value);
            ToAdrId = int.Parse(toAdrIdAttribute.Value);
        }        
    }

}