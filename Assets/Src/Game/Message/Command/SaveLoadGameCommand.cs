using Ce.Core.Message.Command;
using Ce.Game.Message.Abstract;

namespace Ce.Game.Message.Command {

    public class SaveLoadGameCommand: SaveLoadGameMessage, ICommand {

        public SaveLoadGameCommand() { }
        public SaveLoadGameCommand(string fileName, bool isSave) : base(fileName, isSave) { }

    }

}