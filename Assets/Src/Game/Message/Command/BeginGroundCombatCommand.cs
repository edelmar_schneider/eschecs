using Ce.Core.Message;
using Ce.Core.Message.Command;

namespace Ce.Game.Message.Command {

    /// <summary>
    /// Means that the game is ready for the players to start playing.
    /// </summary>
    public class BeginGroundCombatCommand : AMessage, ICommand {
        
    }

}