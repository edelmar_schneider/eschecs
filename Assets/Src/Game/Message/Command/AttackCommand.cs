using Ce.Core.Message.Command;
using Ce.Game.Message.Abstract;

namespace Ce.Game.Message.Command {

    public class AttackCommand : AttackMessage, ICommand {

        public AttackCommand() { }
        public AttackCommand(ulong actorRid, ulong defenderRid, int damage) : base(actorRid, defenderRid, damage) { }

    }

}