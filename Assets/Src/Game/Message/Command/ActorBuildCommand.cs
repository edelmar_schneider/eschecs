using Ce.Core.Message.Command;
using Ce.Game.Components;
using Ce.Game.Message.Abstract;

namespace Ce.Game.Message.Command {

    public class ActorBuildCommand : BuildMessage, ICommand, ISetRidAtServerSide {

        public ActorBuildCommand() { }
        public ActorBuildCommand(ulong actorRid) : base(actorRid) { }

        public void SetRid(ulong rid) {
            this.NewEntityRid = rid;
        }


    }

}