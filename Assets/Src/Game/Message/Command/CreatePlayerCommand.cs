using System.Xml.Linq;
using Ce.Core.Message;
using Ce.Core.Message.Command;

namespace Ce.Game.Message.Command {
    
    public class CreatePlayerCommand: AMessage, ICommand {
        
        public ulong NetworkClientId { get; set; }
        
        public string Name { get; set; }
        
        public bool IsAi { get; set; }
        
        public string God { get; set; }
        
        public string Totem { get; set; }
        
        public CreatePlayerCommand() { }

        public CreatePlayerCommand(ulong networkClientId, string name, bool isAi, string god = "God_S0", string totem = "Totem_S0") {
            NetworkClientId = networkClientId;
            Name = name;
            IsAi = isAi;
            God = god;
        }

        public override XElement ToXml(string elementName) {
            
            var e = base.ToXml(elementName);
            
            e.Add(new XAttribute("Id", NetworkClientId.ToString()));
            e.Add(new XAttribute("Name", Name));
            e.Add(new XAttribute("IsAi", IsAi.ToString()));
            e.Add(new XAttribute("God", God));
            e.Add(new XAttribute("Totem", God));
            
            return e;
        }

        public override void FromXml(XElement element) {
            base.FromXml(element);

            var idAttribute = element.Attribute("Id");
            var nameAttribute = element.Attribute("Name");
            var isAiAttribute = element.Attribute("IsAi");
            var godAttribute = element.Attribute("God");
            var totemAttribute = element.Attribute("Totem");
            
            NetworkClientId = ulong.Parse(idAttribute.Value);
            Name = nameAttribute.Value;
            IsAi = bool.Parse(isAiAttribute.Value);
            God = godAttribute.Value;
            Totem = totemAttribute.Value;
        }

    }
}