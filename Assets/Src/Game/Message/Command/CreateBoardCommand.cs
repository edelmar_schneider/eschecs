using System.Xml.Linq;
using Ce.Core.Message;
using Ce.Core.Message.Command;

namespace Ce.Game.Message.Command {

    public class CreateBoardCommand: AMessage, ICommand {
        
        public int Size { get; set; }
        
        public int Players { get; set; }
        
        public CreateBoardCommand() { }

        public CreateBoardCommand(int size, int players) {
            Size = size;
            Players = players;
        }

        public override XElement ToXml(string elementName) {
            
            var e = base.ToXml(elementName);
            
            e.Add(new XAttribute("Size", Size));
            e.Add(new XAttribute("Players", Players));
            
            return e;
        }

        public override void FromXml(XElement element) {
            base.FromXml(element);

            var sizeAttribute = element.Attribute("Size");
            var playersAttribute = element.Attribute("Players");

            Size = int.Parse(sizeAttribute.Value);
            Players = int.Parse(playersAttribute.Value);
        }

    }
}