using System.Xml.Linq;
using Ce.Core.Message;
using Ce.Core.Message.Command;


namespace Ce.Game.Message.Command {

    public class FinishGameCommand : AMessage, ICommand{
        
        public ulong WinnerGodRid { get; set; }
        public bool ThereIsWinner { get; set; } = false;
        public FinishGameCommand() { }

        public FinishGameCommand(ulong winnerGodRid) {
            WinnerGodRid = winnerGodRid;
            ThereIsWinner = true;
        }

        public override XElement ToXml(string elementName) {

            var e = base.ToXml(elementName);

            e.Add(new XAttribute("GodRid", WinnerGodRid.ToString()));
            e.Add(new XAttribute("ThereIsWinner", ThereIsWinner.ToString()));

            return e;
        }

        public override void FromXml(XElement element) {
            base.FromXml(element);

            var godRidAttribute = element.Attribute("GodRid");
            var thereIsWinnerAttribute = element.Attribute("ThereIsWinner");
            
            WinnerGodRid = ulong.Parse(godRidAttribute.Value);
            ThereIsWinner = bool.Parse(thereIsWinnerAttribute.Value);
        }        
        
    }

}