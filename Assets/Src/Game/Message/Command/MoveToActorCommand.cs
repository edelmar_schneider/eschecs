using Ce.Game.Components;
using Ce.Game.Message.Abstract;

namespace Ce.Game.Message.Command {

    public class MoveToActorCommand : MoveToMessage, IActorCommand {

        public MoveToActorCommand() { }
        public MoveToActorCommand(ulong actorRid, int fromAdrId, int toAdrId) : base(actorRid, fromAdrId, toAdrId) { }

    }
}