using Ce.Core.Message.Command;
using Ce.Game.Message.Abstract;

namespace Ce.Game.Message.Command {

    public class GodPassTurnCommand : GodPassTurnMessage, ICommand{
        public GodPassTurnCommand() { }
        public GodPassTurnCommand(ulong godRid) : base(godRid) { }

    }

}