using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// The entity defines a client connected to the server.
    /// The entity will only exist at the server side.
    /// </summary>
    public class ServerSideComponent : TagComponent<ServerSideComponent> { }
    
    public static class ServerSideComponentHelpers {
        
        public static Entity SetServerSideTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<ServerSideComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasServerSideTag(this Entity e) => e.Has<ServerSideComponent>();

        public static Entity RemoveServerSideTag(this Entity e) => e.Remove<ServerSideComponent>();
    }


}