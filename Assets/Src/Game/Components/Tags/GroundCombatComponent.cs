using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Adding this tag to the game definitions signals that the game should move to ground combat.
    /// </summary>
    public class GroundCombatComponent : TagComponent<GroundCombatComponent> { }
    
    public static class GroundCombatComponentHelpers {
        
        public static Entity SetGroundCombatTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<GroundCombatComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasGroundCombatTag(this Entity e) => e.Has<GroundCombatComponent>();

        public static Entity RemoveGroundCombatTag(this Entity e) => e.Remove<GroundCombatComponent>();
    }

}