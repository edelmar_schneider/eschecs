using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Defines an entity that will hold properties of a new game
    /// </summary>
    public class GameDefinitionsComponent : TagComponent<GameDefinitionsComponent> { }
    
    public static class GameDefinitionsComponentHelpers {
        
        public static Entity SetGameDefinitionsTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<GameDefinitionsComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasGameDefinitionsTag(this Entity e) => e.Has<GameDefinitionsComponent>();

        public static Entity RemoveGameDefinitionsTag(this Entity e) => e.Remove<GameDefinitionsComponent>();
    }

}