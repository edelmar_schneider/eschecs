using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Can be used in god entities to signal that they finished their turn 
    /// </summary>
    public class TurnDoneComponent : TagComponent<TurnDoneComponent> { }
    
    public static class TurnDoneComponentHelpers {
        
        public static Entity SetTurnDoneTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<TurnDoneComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasTurnDoneTag(this Entity e) => e.Has<TurnDoneComponent>();

        public static Entity RemoveTurnDoneTag(this Entity e) => e.Remove<TurnDoneComponent>();
    }


}