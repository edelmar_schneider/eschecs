using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Defines the entity as a local player which mean that it is the individual controlling the local
    /// computer with mouse and keyboard.
    /// </summary>
    public class LocalPlayerComponent : TagComponent<LocalPlayerComponent> { }
    
    public static class LocalPlayerComponentHelpers {
        
        public static Entity SetLocalPlayerTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<LocalPlayerComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasLocalPlayerTag(this Entity e) => e.Has<LocalPlayerComponent>();

        public static Entity RemoveLocalPlayerTag(this Entity e) => e.Remove<LocalPlayerComponent>();
    }


}