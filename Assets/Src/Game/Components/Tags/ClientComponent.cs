using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Defines that the entity will be a client in a server/client relationship
    /// </summary>
    public class ClientComponent : TagComponent<ClientComponent> { }
    
    public static class ClientComponentHelpers {
        
        public static Entity SetClientTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<ClientComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasClientTag(this Entity e) => e.Has<ClientComponent>();

        public static Entity RemoveClientTag(this Entity e) => e.Remove<ClientComponent>();
    }


}