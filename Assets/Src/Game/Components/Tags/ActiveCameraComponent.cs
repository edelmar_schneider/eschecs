using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Defines an entity as the active camera 
    /// </summary>
    public class ActiveCameraComponent : TagComponent<ActiveCameraComponent> { }
    
    public static class ActiveCameraComponentHelpers {
        
        public static Entity SetActiveCameraTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<ActiveCameraComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasActiveCameraTag(this Entity e) => e.Has<ActiveCameraComponent>();

        public static Entity RemoveActiveCameraTag(this Entity e) => e.Remove<ActiveCameraComponent>();
    }


}