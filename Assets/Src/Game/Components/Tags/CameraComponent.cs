using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Defines an entity that is a game camera
    /// </summary>
    public class CameraComponent : TagComponent<CameraComponent> { }
    
    public static class CameraComponentHelpers {
        
        public static Entity SetCameraTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<CameraComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasCameraTag(this Entity e) => e.Has<CameraComponent>();

        public static Entity RemoveCameraTag(this Entity e) => e.Remove<CameraComponent>();
    }


}