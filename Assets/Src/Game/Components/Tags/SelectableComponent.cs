using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Means that the entity can be selected by clicking 
    /// </summary>
    public class SelectableComponent : TagComponent<SelectableComponent> { }
    
    public static class SelectableComponentHelpers {
        
        public static Entity SetSelectableTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<SelectableComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasSelectableTag(this Entity e) => e.Has<SelectableComponent>();

        public static Entity RemoveSelectableTag(this Entity e) => e.Remove<SelectableComponent>();
    }


}