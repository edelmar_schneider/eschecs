using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Defines an entity that should be registered on the board
    /// </summary>
    public class OnBoardComponent : TagComponent<OnBoardComponent> {
    }
    
    public static class OnBoardComponentHelpers {
        
        public static Entity SetOnBoardTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<OnBoardComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasOnBoardTag(this Entity e) => e.Has<OnBoardComponent>();

        public static Entity RemoveOnBoardTag(this Entity e) => e.Remove<OnBoardComponent>();
    }

}