using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Defines that the entity will be a server in a server/client relationship
    /// </summary>
    public class ServerComponent : TagComponent<ServerComponent> { }
    
    public static class ServerComponentHelpers {
        
        public static Entity SetServerTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<ServerComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasServerTag(this Entity e) => e.Has<ServerComponent>();

        public static Entity RemoveServerTag(this Entity e) => e.Remove<ServerComponent>();
    }


}