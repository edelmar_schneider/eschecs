using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Tags {

    /// <summary>
    /// Defines the entity as a god.
    /// </summary>
    public class GodComponent : TagComponent<GodComponent> { }
    
    public static class GodComponentHelpers {
        
        public static Entity SetGodTag(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<GodComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasGodTag(this Entity e) => e.Has<GodComponent>();

        public static Entity RemoveGodTag(this Entity e) => e.Remove<GodComponent>();
    }


}