using System.Collections.Generic;
using Ce.Core.Ecs;
using Ce.Core.World;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Collection {
    
    /// <summary>
    /// The Shape defines the shape of the footprint in local coordinates
    /// </summary>
    public class ShapeComponent: SetComponent<Address, ShapeComponent> {
        public override bool IsSerializable => true;
    }
    
    public static class ShapeComponentHelpers {
        public static Entity SetShape(this Entity e, IEnumerable<Address> value) {
            var u = e.Universe;
            var c = u.AcquireComponent<ShapeComponent>();
            foreach (var address in value) {
                c.Value.Add(address);
            }
            e.Update(c);
            return e;
        }

        public static ShapeComponent GetShape(this Entity e) => e.Get<ShapeComponent>();

        public static bool HasShape(this Entity e) => e.Has<ShapeComponent>();

        public static Entity RemoveShape(this Entity e) => e.Remove<ShapeComponent>();
    }

}