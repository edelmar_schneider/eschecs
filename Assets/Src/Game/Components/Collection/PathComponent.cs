using System.Collections.Generic;
using Ce.Core.Ecs;
using Ce.Game.Components.Generic;
using Ce.Game.System;

namespace Ce.Game.Components.Collection {

    /// <summary>
    /// Should host a path from some point to another point.
    /// </summary>
    public class PathComponent: SetComponent<Waypoint, PathComponent> {
    }
    
    public static class PathComponentHelpers {
        public static Entity SetPath(this Entity e, IEnumerable<Waypoint> value) {
            var u = e.Universe;
            var c = u.AcquireComponent<PathComponent>();
            foreach (var address in value) {
                c.Value.Add(address);
            }
            e.Update(c);
            return e;
        }

        public static PathComponent GetPath(this Entity e) => e.Get<PathComponent>();

        public static bool HasPath(this Entity e) => e.Has<PathComponent>();

        public static Entity RemovePath(this Entity e) => e.Remove<PathComponent>();
    }

}