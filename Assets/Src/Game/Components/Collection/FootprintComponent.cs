using System.Collections.Generic;
using Ce.Core.Ecs;
using Ce.Core.World;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Collection {

    /// <summary>
    /// The Footprint is defines the coordinates of the shape of the object in world coordinates.
    /// This doesn't need to be serialized because it will be recreated when the object is moved or when the level is loaded
    /// </summary>
    public class FootprintComponent: SetComponent<Address, FootprintComponent> {
        public override bool IsSerializable => false;
    }
    
    public static class FootprintComponentHelpers {
        public static Entity SetFootprint(this Entity e, IEnumerable<Address> value) {
            var u = e.Universe;
            var c = u.AcquireComponent<FootprintComponent>();
            foreach (var address in value) {
                c.Value.Add(address);
            }
            e.Update(c);
            return e;
        }

        public static FootprintComponent GetFootprint(this Entity e) => e.Get<FootprintComponent>();

        public static bool HasFootprint(this Entity e) => e.Has<FootprintComponent>();

        public static Entity RemoveFootprint(this Entity e) => e.Remove<FootprintComponent>();
    }

}