using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Extensions;
using Ce.Core.System;
using Ce.Core.System.Job;
using Ce.Core.World;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Data;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.Value;
using Ce.Game.Message.Command;
using Ce.Game.System;
using Ce.Game.World;
using UnityEngine;

namespace Ce.Game.Components.Job {

    public class TravelJob : AJob {

        private MoveToActorCommand _command;
        private Entity _actor;
        private Universe _u;
        private TileBoard _tb;
        private PathfinderSystem _ps;

        private Address _begin;
        private Address _end;

        /// <summary>
        /// The path that I will execute.
        /// As the executions progresses the way-points are then removed one by one.
        /// </summary>
        private Queue<Waypoint> _path;

        public TravelJob SetActor(Entity a) {
            _actor = a;
            _u = a.Universe;
            _tb = _u.GetSystem<BoardSystem>().Board;
            _ps = _u.GetSystem<PathfinderSystem>();
            return this;
        }
        
        public TravelJob SetCommand(MoveToActorCommand c) {
            _command = c;
            return this;
        }

        protected override void OnActivate() {
            base.OnActivate();

            _begin = _tb.GetElement(_command.FromAdrId).Address;
            _end = _tb.GetElement(_command.ToAdrId).Address;

            //Regenerate the path (this might be at the client side)
            _path = _ps.FindPath(_actor, _begin, _end).ToQueue();
            
            //I can remove the first waypoint because that waypoint is where I'm now.
            _path.Dequeue();
            
            //Engage the first task in the path execution
            EngageNextTask();
        }

        private void EngageNextTask() {

            if (!_path.Any()) {
                Machine.Trigger(JobTrigger.Success);
                return;
            }

            //I can have two types of actions, Or I can move or I need to rotate.
            //TODO Implement move and rotate at the same time.
            
            //I only dequeue if rotation is done or not needed.
            var wp = _path.Peek();

            var curPos = _actor.GetAddress().Pos();
            var curRot = _actor.GetTransform().Rotation;
            var curDir = curRot * Vector3.forward;

            var nxtTl = _tb.GetElement(wp.AddressId);
            var nxtPos = nxtTl.Address.Pos();

            var tgtDir = (nxtPos - curPos).normalized;
            
            var ts = _u.TimeSystem();
            var t0 = ts.Time;

            var curAngle = Vector3.Angle(Vector3.forward, curDir);
            var tgtAngle = Vector3.Angle(Vector3.forward, tgtDir);

            var angDif = tgtAngle - curAngle;
            
            //Do we need to rotate?
            if (angDif.NearlyDifferent(0)) {
                var tf = 8f;
                
                //Queue rotation
                _u.RegisterJob<LambdaJob>()
                        .OnTick(job => {
                                    var dt = ts.Time - t0;
                                    var t = dt * tf;
                                    var dir = Vector3.Slerp(curDir, tgtDir, t);
                                    var rot = Quaternion.LookRotation(dir, Vector3.up);
                                    
                                    _actor.SetTransform(curPos, rot);
                                    if (t >= 1f) {
                                        job.TriggerAfter(JobTrigger.Success);
                                    }
                                })
                        .OnSuccess(job => { EngageNextTask(); });
                
            }
            else { //No! I need to move only because I am already looking at the right direction.

                var deltaAp = (float)wp.IndividualDistance / GameConstants.GameParameters.DistancePerAp * -1f;
                _actor.SetActionPointsDelta(deltaAp);

                _path.Dequeue();
                var tf = 4f;
                
                //Queue movement
                _u.RegisterJob<LambdaJob>()
                  .OnTick(job => {
                              var dt = ts.Time - t0;
                              var t = dt * tf;
                              var pos = Vector3.Lerp(curPos, nxtPos, t);
                              _actor.SetTransform(pos);
                              if (t >= 1f) {
                                  job.TriggerAfter(JobTrigger.Success);
                              }
                          })
                  .OnSuccess(job => { EngageNextTask(); });
                
            }
            
            State = JobState.Tick;
        }

        protected override void OnSuccess() {
            _actor.RemoveAction();
            
            base.OnSuccess();
        }
    }

}