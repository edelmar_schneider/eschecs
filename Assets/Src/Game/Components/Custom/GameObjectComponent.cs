using Ce.Core.Ecs;
using Ce.Game.Components.Generic;
using UnityEngine;
using NotImplementedException = System.NotImplementedException;

namespace Ce.Game.Components.Custom {

    public class GameObjectComponent : DataComponent<GameObject>
    {
        
        public bool Serializable => false;

        protected override string ToString(GameObject value) => throw new NotImplementedException();

        protected override GameObject ParseValue(string value) => throw new NotImplementedException();

        public override bool IsSerializable => false;

        public override IComponent DeepCopy() {
            if (Value == null) {
                return new GameObjectComponent {Value = null};
            }
            
            return new GameObjectComponent {Value = Object.Instantiate(Value)};
        }

        public override IComponent Default() => new GameObjectComponent{Value = null};

        public override void CleanUp() {
            if (Value == null) {
                return;
            }

            Object.Destroy(Value);
            Value = null;
        }

    }
    
    public static class GameObjectComponentHelpers {
        public static Entity SetGameObject(this Entity e, GameObject value) {
            var u = e.Universe;
            var c = u.AcquireComponent<GameObjectComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static GameObject GetGameObject(this Entity e) => e.Get<GameObjectComponent>().Value;

        public static bool HasGameObject(this Entity e) => e.Has<GameObjectComponent>();

        public static Entity RemoveGameObject(this Entity e) => e.Remove<GameObjectComponent>();
    }

}