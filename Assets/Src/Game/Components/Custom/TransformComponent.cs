using System.Globalization;
using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.Util;
using UnityEngine;

namespace Ce.Game.Components.Custom {

    /// <summary>
    /// Holds the transformation of an entity
    /// Position, Rotation, LocalScale
    /// </summary>
    public sealed class TransformComponent : IComponent {
        
        public Vector3 Position;
        
        public Quaternion Rotation;
        
        public Vector3 Scale;
        
        public bool IsSerializable => true;

        public XElement ToXml(string elementName) {
            return new XElement(elementName,
                                new XAttribute("pX", Position.x.ToString("0.000", CultureInfo.InvariantCulture)),
                                new XAttribute("pY", Position.y.ToString("0.000", CultureInfo.InvariantCulture)),
                                new XAttribute("pZ", Position.z.ToString("0.000", CultureInfo.InvariantCulture)),
                
                                new XAttribute("rX", Rotation.x.ToString("0.000", CultureInfo.InvariantCulture)),
                                new XAttribute("rY", Rotation.y.ToString("0.000", CultureInfo.InvariantCulture)),
                                new XAttribute("rZ", Rotation.z.ToString("0.000", CultureInfo.InvariantCulture)),
                                new XAttribute("rW", Rotation.w.ToString("0.000", CultureInfo.InvariantCulture)),
                
                                new XAttribute("sX", Scale.x.ToString("0.000", CultureInfo.InvariantCulture)),
                                new XAttribute("sY", Scale.y.ToString("0.000", CultureInfo.InvariantCulture)),
                                new XAttribute("sZ", Scale.z.ToString("0.000", CultureInfo.InvariantCulture))
                               );
        }

        public void FromXml(XElement element) {
            
            var pXAttribute = element.Attribute("pX");
            var pYAttribute = element.Attribute("pY");
            var pZAttribute = element.Attribute("pZ");

            var pX = float.Parse(pXAttribute.Value, CultureInfo.InvariantCulture);
            var pY = float.Parse(pYAttribute.Value, CultureInfo.InvariantCulture);
            var pZ = float.Parse(pZAttribute.Value, CultureInfo.InvariantCulture);
            
            Position = new Vector3(pX, pY, pZ);
            
            
            var rXAttribute = element.Attribute("rX");
            var rYAttribute = element.Attribute("rY");
            var rZAttribute = element.Attribute("rZ");
            var rWAttribute = element.Attribute("rW");

            var rX = float.Parse(rXAttribute.Value, CultureInfo.InvariantCulture);
            var rY = float.Parse(rYAttribute.Value, CultureInfo.InvariantCulture);
            var rZ = float.Parse(rZAttribute.Value, CultureInfo.InvariantCulture);
            var rW = float.Parse(rWAttribute.Value, CultureInfo.InvariantCulture);
            
            Rotation = new Quaternion(rX, rY, rZ, rW);
            
            var sXAttribute = element.Attribute("sX");
            var sYAttribute = element.Attribute("sY");
            var sZAttribute = element.Attribute("sZ");

            var sX = float.Parse(sXAttribute.Value, CultureInfo.InvariantCulture);
            var sY = float.Parse(sYAttribute.Value, CultureInfo.InvariantCulture);
            var sZ = float.Parse(sZAttribute.Value, CultureInfo.InvariantCulture);
            
            Scale = new Vector3(sX, sY, sZ);       
        }

        public TransformComponent DeepCopy() {
            return new TransformComponent() {
                Position = Position,
                Rotation = Rotation,
                Scale = Scale
            };
        }

        object IDeepCopy.DeepCopy() => DeepCopy();


        public IComponent Default() {
            return new TransformComponent {
                Position = Vector3.zero,
                Rotation = Quaternion.identity,
                Scale = Vector3.one
            };
        }

        public void CleanUp() {
            Position = Vector3.zero;
            Rotation = Quaternion.identity;
            Scale = Vector3.one;
        }
    }
    
    public static class TransformComponentHelpers {
        public static Entity SetTransform(this Entity e, Vector3 pos, Quaternion rot, Vector3 scale) {
            var u = e.Universe;
            var c = u.AcquireComponent<TransformComponent>();
            c.Position = pos;
            c.Rotation = rot;
            c.Scale = scale;
            e.Update(c);
            return e;
        }
        
        public static Entity SetTransform(this Entity e, Vector3 pos) {
            var u = e.Universe;
            var c = u.AcquireComponent<TransformComponent>();

            var p = pos;
            var r = Quaternion.identity;
            var s = Vector3.one;

            if (e.HasTransform()) {
                var old = e.Get<TransformComponent>();
                
                r = old.Rotation;
                s = old.Scale;
            }
            
            c.Position = p;
            c.Rotation = r;
            c.Scale = s;
            e.Update(c);
            return e;
        }
        
        public static Entity SetTransform(this Entity e, Vector3 pos, Quaternion rot) {
            var u = e.Universe;
            var c = u.AcquireComponent<TransformComponent>();

            var p = pos;
            var r = rot;
            var s = Vector3.one;

            if (e.HasTransform()) {
                var old = e.Get<TransformComponent>();
                s = old.Scale;
            }
            
            c.Position = p;
            c.Rotation = r;
            c.Scale = s;
            e.Update(c);
            return e;
        }        
        
        public static Entity SetTransform(this Entity e, Quaternion rot) {
            var u = e.Universe;
            var c = u.AcquireComponent<TransformComponent>();

            var p = Vector3.zero;
            var r = rot;
            var s = Vector3.one;

            if (e.HasTransform()) {
                var old = e.Get<TransformComponent>();
                p = old.Position;
                s = old.Scale;
            }
            
            c.Position = p;
            c.Rotation = r;
            c.Scale = s;
            e.Update(c);
            return e;
        }        

        public static TransformComponent GetTransform(this Entity e) => e.Get<TransformComponent>();

        public static bool HasTransform(this Entity e) => e.Has<TransformComponent>();

        public static Entity RemoveTransform(this Entity e) => e.Remove<TransformComponent>();
    }


}