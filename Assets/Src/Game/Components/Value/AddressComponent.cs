using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.World;
using Ce.Game.Components.Generic;
using NotImplementedException = System.NotImplementedException;

namespace Ce.Game.Components.Value {

    /// <summary>
    /// The address (board coordinates) of an entity on the board of the game.
    /// 3D coordinates (Transform) are translated to addresses so we can locate any entity on the board of the game 
    /// </summary>
    public class AddressComponent: ValueComponent<Address, AddressComponent> {

        public override IComponent DeepCopy() => new AddressComponent{Value = Value.DeepCopy()};

        public override IComponent Default() => new AddressComponent{Value = Address.Zero};
        public override XElement ToXml(string elementName) => Value.ToXml(elementName);

        public override void FromXml(XElement element) {
            Value.FromXml(element);
        }

        protected override string ToString(Address value) => throw new NotImplementedException();

        protected override Address ParseValue(string value) => throw new NotImplementedException();

        public override bool IsSerializable => false;

    }
    
    public static class AddressComponentHelpers {
        public static Entity SetAddress(this Entity e, Address value) {
            var u = e.Universe;
            var c = u.AcquireComponent<AddressComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static Address GetAddress(this Entity e) => e.Get<AddressComponent>().Value;

        public static bool HasAddress(this Entity e) => e.Has<AddressComponent>();

        public static Entity RemoveAddress(this Entity e) => e.Remove<AddressComponent>();
    }

}