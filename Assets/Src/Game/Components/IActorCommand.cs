using Ce.Core.Message.Command;

namespace Ce.Game.Components {

    public interface IActorCommand: ICommand {

        /// <summary>
        /// The actor who should perform the task.
        /// </summary>
        ulong ActorRid { get; }

    }

}