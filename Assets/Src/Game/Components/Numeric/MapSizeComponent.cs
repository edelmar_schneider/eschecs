using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Numeric {

    /// <summary>
    /// Defines the size of a map as an integer
    /// </summary>
    public class MapSizeComponent : ValueComponent<int, MapSizeComponent> {
        protected override int ParseValue(string value) => int.Parse(value);
        
        protected override string ToString(int value) => value.ToString();
    }
    
    public static class MapSizeComponentHelpers {
        public static Entity SetMapSize(this Entity e, int size) {
            var u = e.Universe;
            var c = u.AcquireComponent<MapSizeComponent>();
            c.Value = size;
            e.Update(c);
            return e;
        }

        public static int GetMapSize(this Entity e) => e.Get<MapSizeComponent>().Value;

        public static bool HasMapSize(this Entity e) => e.Has<MapSizeComponent>();

        public static Entity RemoveMapSize(this Entity e) => e.Remove<MapSizeComponent>();
    }    

}