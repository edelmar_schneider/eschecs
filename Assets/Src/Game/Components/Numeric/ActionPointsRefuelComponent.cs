using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Numeric {

    /// <summary>
    /// The component provides how much of action points each actor will get at the start of a turn.
    /// </summary>
    public class ActionPointsRefuelComponent : ValueComponent<int, ActionPointsRefuelComponent> {
        protected override int ParseValue(string value) => int.Parse(value);
        
        protected override string ToString(int value) => value.ToString();
    }
    
    public static class ActionPointsRefuelComponentHelpers {
        public static Entity SetActionPointsRefuel(this Entity e, int size) {
            var u = e.Universe;
            var c = u.AcquireComponent<ActionPointsRefuelComponent>();
            c.Value = size;
            e.Update(c);
            return e;
        }

        public static int GetActionPointsRefuel(this Entity e) => e.Get<ActionPointsRefuelComponent>().Value;

        public static bool HasActionPointsRefuel(this Entity e) => e.Has<ActionPointsRefuelComponent>();

        public static Entity RemoveActionPointsRefuel(this Entity e) => e.Remove<ActionPointsRefuelComponent>();
    }    

}