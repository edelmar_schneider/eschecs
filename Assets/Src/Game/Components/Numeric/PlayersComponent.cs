using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Numeric {

    /// <summary>
    /// Defines a quantity of players
    /// </summary>
    public class PlayersComponent : ValueComponent<int, PlayersComponent> {
        protected override int ParseValue(string value) => int.Parse(value);
        
        protected override string ToString(int value) => value.ToString();
    }
    
    public static class PlayersComponentHelpers {
        public static Entity SetPlayers(this Entity e, int value) {
            var u = e.Universe;
            var c = u.AcquireComponent<PlayersComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static int GetPlayers(this Entity e) => e.Get<PlayersComponent>().Value;

        public static bool HasPlayers(this Entity e) => e.Has<PlayersComponent>();

        public static Entity RemovePlayers(this Entity e) => e.Remove<PlayersComponent>();
    }    

}