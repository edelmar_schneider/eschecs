using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Numeric {

    /// <summary>
    /// Every actor has an amount of action points that they can use to perform actions during the turn.
    /// </summary>
    public class ActionPointsComponent : ValueComponent<float, ActionPointsComponent> {
        protected override float ParseValue(string value) => float.Parse(value);
        
        protected override string ToString(float value) => value.ToString();
    }
    
    public static class ActionPointsComponentHelpers {
        public static Entity SetActionPoints(this Entity e, float value) {
            var u = e.Universe;
            var c = u.AcquireComponent<ActionPointsComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }
        
        public static Entity SetActionPointsDelta(this Entity e, float delta) {
            var ov = e.GetActionPoints();
            var nv = ov + delta;
            e.SetActionPoints(nv);
            return e;
        }

        public static float GetActionPoints(this Entity e) => e.Get<ActionPointsComponent>().Value;

        public static bool HasActionPoints(this Entity e) => e.Has<ActionPointsComponent>();

        public static Entity RemoveActionPoints(this Entity e) => e.Remove<ActionPointsComponent>();
    }    

}