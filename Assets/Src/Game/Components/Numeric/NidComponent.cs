using Ce.Core.Ecs;
using Ce.Game.Components.Generic;
using NotImplementedException = System.NotImplementedException;

namespace Ce.Game.Components.Numeric {

    /// <summary>
    /// Nid means Network ID.
    /// Objects that need to have the same identification in the network must have the same Nid so different player
    /// can communicate about them
    /// 
    /// </summary>
    public class NidComponent : ValueComponent<ulong, NidComponent> {

        protected override ulong ParseValue(string value) => ulong.Parse(value);
        
        protected override string ToString(ulong value) => value.ToString();
        
    }
    
    public static class NidComponentHelpers {
        public static Entity SetNid(this Entity e, ulong value) {
            var u = e.Universe;
            var c = u.AcquireComponent<NidComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static ulong GetNid(this Entity e) => e.Get<NidComponent>().Value;

        public static bool HasNid(this Entity e) => e.Has<NidComponent>();

        public static Entity RemoveNid(this Entity e) => e.Remove<NidComponent>();
        
        
    }    


}