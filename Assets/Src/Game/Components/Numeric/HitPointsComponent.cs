using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Numeric {

    /// <summary>
    /// Defines how much "life" an entity still has.
    /// When HP reaches 0 the entity is killed or destroyed
    /// </summary>
    public class HitPointsComponent : ValueComponent<int, HitPointsComponent> {
        protected override int ParseValue(string value) => int.Parse(value);
        
        protected override string ToString(int value) => value.ToString();
    }
    
    public static class HitPointsComponentHelpers {
        public static Entity SetHitPoints(this Entity e, int value) {
            var u = e.Universe;
            var c = u.AcquireComponent<HitPointsComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static int GetHitPoints(this Entity e) => e.Get<HitPointsComponent>().Value;

        public static bool HasHitPoints(this Entity e) => e.Has<HitPointsComponent>();

        public static Entity RemoveHitPoints(this Entity e) => e.Remove<HitPointsComponent>();
    }    

}