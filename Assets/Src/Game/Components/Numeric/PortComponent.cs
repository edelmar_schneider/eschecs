using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Numeric {

    /// <summary>
    /// Defines the port for the network system
    /// </summary>
    public class PortComponent : ValueComponent<int, PortComponent> {
        protected override int ParseValue(string value) => int.Parse(value);
        
        protected override string ToString(int value) => value.ToString();
    }
    
    public static class PortsComponentHelpers {
        public static Entity SetPort(this Entity e, int value) {
            var u = e.Universe;
            var c = u.AcquireComponent<PortComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static int GetPort(this Entity e) => e.Get<PortComponent>().Value;

        public static bool HasPort(this Entity e) => e.Has<PortComponent>();

        public static Entity RemovePort(this Entity e) => e.Remove<PortComponent>();
    }    

}