using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Numeric {

    /// <summary>
    /// This is assigned based on the client id in the network system 
    /// </summary>
    public class NetworkClientIdComponent : ValueComponent<ulong, NetworkClientIdComponent> {

        protected override ulong ParseValue(string value) => ulong.Parse(value);
        
        protected override string ToString(ulong value) => value.ToString();
        
    }
    
    public static class NetworkClientIdComponentHelpers {
        public static Entity SetNetworkClientId(this Entity e, ulong value) {
            var u = e.Universe;
            var c = u.AcquireComponent<NetworkClientIdComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static ulong GetNetworkClientId(this Entity e) => e.Get<NetworkClientIdComponent>().Value;

        public static bool HasNetworkClientId(this Entity e) => e.Has<NetworkClientIdComponent>();

        public static Entity RemoveNetworkClientId(this Entity e) => e.Remove<NetworkClientIdComponent>();
        
        
    }    


}