using Ce.Core.Ecs;
using Ce.Core.Message;
using Ce.Core.Message.Command;
using Ce.Game.Components.Generic;
using NotImplementedException = System.NotImplementedException;

namespace Ce.Game.Components.Data {

    /// <summary>
    /// When you set and action into an entity the action system will read that and create a job to perform
    /// the task defined in the Message
    /// </summary>
    public class ActionComponent : DataComponent<AMessage> {
        public override IComponent DeepCopy() => new ActionComponent {
            Value = AMessage.Instantiate(Value.ToXml("any"))
        };

        public override IComponent Default() => new ActionComponent{Value = new EmptyCommand()}; 

        public override void CleanUp() => Value = new EmptyCommand();

        protected override string ToString(AMessage value) => throw new NotImplementedException();

        protected override AMessage ParseValue(string value) => throw new NotImplementedException();

        public override bool IsSerializable => false;
    }
    
    public static class ActionComponentHelpers {
        public static Entity SetAction(this Entity e, AMessage value) {
            var u = e.Universe;
            var c = u.AcquireComponent<ActionComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static AMessage GetAction(this Entity e) => e.Get<ActionComponent>().Value;

        public static bool HasAction(this Entity e) => e.Has<ActionComponent>();

        public static Entity RemoveAction(this Entity e) => e.Remove<ActionComponent>();
    }


    /// <summary>
    /// Does nothing, exists only to avoid having ActionComponents with null values.
    /// </summary>
    public class EmptyCommand : AMessage, ICommand {}

}