using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.String {

    /// <summary>
    /// Used to store a name for the entity that can be shown to the player.
    /// </summary>
    public class DisplayNameComponent : StringComponent<DisplayNameComponent> {
        
        public override bool IsSerializable => true;
    }
    
    
    public static class DisplayNameComponentHelpers {
        public static Entity SetDisplayName(this Entity e, string value) {
            var u = e.Universe;
            var c = u.AcquireComponent<DisplayNameComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static string GetDisplayName(this Entity e) => e.Get<DisplayNameComponent>().Value;

        public static bool HasDisplayName(this Entity e) => e.Has<DisplayNameComponent>();

        public static Entity RemoveDisplayName(this Entity e) => e.Remove<DisplayNameComponent>();
    }

}