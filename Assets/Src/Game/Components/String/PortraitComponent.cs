using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.String {

    /// <summary>
    /// Store a reference to the profile image of an entity.
    /// </summary>
    public class PortraitComponent : StringComponent<PortraitComponent> {}
    
    public static class PortraitComponentHelpers {
        public static Entity SetPortrait(this Entity e, string value) {
            var u = e.Universe;
            var c = u.AcquireComponent<PortraitComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static string GetPortrait(this Entity e) => e.Get<PortraitComponent>().Value;

        public static bool HasPortrait(this Entity e) => e.Has<PortraitComponent>();

        public static Entity RemovePortrait(this Entity e) => e.Remove<PortraitComponent>();
    }


}