using Ce.Core.Ecs;
using Ce.Game.Components.Generic;
using NotImplementedException = System.NotImplementedException;

namespace Ce.Game.Components.String {

    /// <summary>
    /// Used to store a name for the entity that can be shown to the player.
    /// </summary>
    public class IpComponent : StringComponent<IpComponent> {

        
    }
    
    
    public static class IpComponentHelpers {
        public static Entity SetIp(this Entity e, string value) {
            var u = e.Universe;
            var c = u.AcquireComponent<IpComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static string GetIp(this Entity e) => e.Get<IpComponent>().Value;

        public static bool HasIp(this Entity e) => e.Has<IpComponent>();

        public static Entity RemoveIp(this Entity e) => e.Remove<IpComponent>();
    }

}