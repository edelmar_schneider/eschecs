using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.String {

    /// <summary>
    /// Store a reference to the prefab file used to make the 3d GameObject of the entity.
    /// </summary>
    public class PrefabComponent : StringComponent<PrefabComponent> {}
    
    
    public static class PrefabComponentHelpers {
        public static Entity SetPrefab(this Entity e, string value) {
            var u = e.Universe;
            var c = u.AcquireComponent<PrefabComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static string GetPrefab(this Entity e) => e.Get<PrefabComponent>().Value;

        public static bool HasPrefab(this Entity e) => e.Has<PrefabComponent>();

        public static Entity RemovePrefab(this Entity e) => e.Remove<PrefabComponent>();
    }


}