using Ce.Core.Ecs;

namespace Ce.Game.Components.Generic {

    /// <summary>
    /// This must be a single value, as in not a reference type.
    /// </summary>
    public abstract class ValueComponent<TData, TComponent> : DataComponent<TData>
        where TData: struct
        where TComponent : ValueComponent<TData, TComponent>,  new()
    {
        public override IComponent DeepCopy() => new TComponent{Value = Value};

        public override IComponent Default() => new TComponent {Value = default};

        public override bool IsSerializable => true;
    }

}