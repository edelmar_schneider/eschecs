using Ce.Core.Ecs;
using Ce.Core.Util;

namespace Ce.Game.Components.Generic {

    /// <summary>
    /// A single value object component
    /// </summary>
    public abstract class ReferenceComponent<TData, TComponent> : DataComponent<TData>
        where TData : class, IDeepCopy
        where TComponent : ReferenceComponent<TData, TComponent>, new() {

        public override IComponent DeepCopy() => new TComponent{Value = (TData)Value.DeepCopy()};
    }

}