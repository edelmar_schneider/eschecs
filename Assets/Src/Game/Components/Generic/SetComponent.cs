using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.Util;
using NotImplementedException = System.NotImplementedException;

namespace Ce.Game.Components.Generic {

    public class SetComponent<TData, TComponent> : DataComponent<HashSet<TData>>
        where TComponent : SetComponent<TData, TComponent>, new()
        where TData : IDeepCopy, IXmlConversion, new() {

        protected SetComponent() {
            Value = new HashSet<TData>();
        }

        public override bool IsSerializable => true;

        protected override string ToString(HashSet<TData> value) => throw new NotImplementedException();

        protected override HashSet<TData> ParseValue(string value) => throw new NotImplementedException();

        public override XElement ToXml(string elementName)
        {
            var root = new XElement(elementName);
            foreach (var value in Value) {
                root.Add(value.ToXml("Item"));
            }

            return root;
        }

        public override void FromXml(XElement element)
        {
            Value = new HashSet<TData>();
            foreach (var childElement in element.Elements())
            {
                var item = new TData();
                item.FromXml(childElement);
                Value.Add(item);
            }
        }
        

        public override IComponent DeepCopy() {
            var newComp = (TComponent)Default();
            foreach (var data in Value) {
                newComp.Add((TData)data.DeepCopy());
            }

            return newComp;
        }

        public override IComponent Default() => new TComponent{Value = new HashSet<TData>()};

        public override void CleanUp() {
            Value.Clear();
        }

        public IEnumerator<TData> GetEnumerator() {
            return Value.GetEnumerator();
        }

        public bool Add(TData item) {
            return Value.Add(item);
        }

        public bool Remove(TData item) {
            return Value.Remove(item);
        }

        public bool Contains(TData item) {
            return Value.Contains(item);
        }

        public int Count() {
            return Value.Count;
        }

        public void Clear() {
            Value.Clear();
        }

        //Set Operations

        public bool IsEmpty() {
            return Value.Count == 0;
        }

        public bool SetEquals(IEnumerable<TData> other) {
            return Value.SetEquals(other);
        }

        public bool SequenceEquals(IEnumerable<TData> other) {
            return Value.SequenceEqual(other);
        }

        public bool Overlaps(IEnumerable<TData> other) {
            return Value.Overlaps(other);
        }

        public void IntersectWith(IEnumerable<TData> other) {
            Value.IntersectWith(other);
        }

        public void UnionWith(IEnumerable<TData> other) {
            Value.UnionWith(other);
        }

        public void SymmetricExceptWith(IEnumerable<TData> other) {
            Value.SymmetricExceptWith(other);
        }

        public void ExceptWith(IEnumerable<TData> other) {
            Value.ExceptWith(other);
        }

        public bool IsSupersetOf(IEnumerable<TData> other) {
            return Value.IsSupersetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<TData> other) {
            return Value.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<TData> other) {
            return Value.IsSubsetOf(other);
        }

        public bool IsProperSubsetOf(IEnumerable<TData> other) {
            return Value.IsProperSubsetOf(other);
        }

        public bool Equals(HashSet<TData> other) {
            return SetEquals(other);
        }

        public bool Equals(DataComponent<HashSet<TData>> other) {
            if (ReferenceEquals(other.Value, Value)) {
                return true;
            }

            if (ReferenceEquals(other.Value, null)) {
                return false;
            }

            if (ReferenceEquals(Value, null)) {
                return false;
            }

            return SetEquals(other.Value);
        }

        public override int GetHashCode() {
            return Value != null ? Value.Aggregate(397, (current, p) => current ^ p.GetHashCode()) : 0;
        }

        public override string ToString() {
            return $"{GetType().Name.Replace("Component", "")} - {(Value == null ? "null" : Value.Count.ToString())} Values: {Value}";
        }
    }
}