using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.Util;

namespace Ce.Game.Components.Generic {

    /// <summary>
    /// A tag component can be used when you want to set a property to an object based on the presence or absence of a tag
    /// "Selected" is a good example of such property
    /// </summary>
    public abstract class TagComponent<TComponent> : IComponent
        where TComponent : TagComponent<TComponent>, new() {

        public TComponent DeepCopy() => new TComponent();
        
        object IDeepCopy.DeepCopy() => DeepCopy();
        
        IComponent IComponent.Default() => new TComponent();
        
        public virtual void CleanUp() {}

        public bool IsSerializable => true;
        
        public XElement ToXml(string elementName) => new XElement(elementName);

        public void FromXml(XElement element) { }
    }

}