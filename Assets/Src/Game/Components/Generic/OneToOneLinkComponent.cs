using System.Linq;
using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.Util;

namespace Ce.Game.Components.Generic {

    public abstract class OneToOneLinkComponent<TMe, TOther> : IComponent, ILink 
    where TMe: OneToOneLinkComponent<TMe, TOther>, new()
    where TOther: OneToOneLinkComponent<TOther, TMe>, new()
    {
        public Entity Other;

        /// <summary>
        /// This is used only during loading time.
        /// </summary>
        private ulong _otherRid;

        public virtual bool IsSerializable => true;
        
        public static Entity CreateLink(Entity me, Entity other) {
            var u = me.Universe;
            
            //First destroy the link if it exists already
            if (HasLink(me)) {
                DestroyLink(me);
            }
            
            //Link me to the other
            {
                if (me.Has<TMe>()) {
                    var oldLink = me.Get<TMe>();
                    oldLink.Other.Remove<TOther>();
                }
                
                var c = u.AcquireComponent<TMe>();
                c.Other = other;
                me.Update(c);
            }
            //Link the other to me
            {
                var c = u.AcquireComponent<TOther>();
                c.Other = me;
                other.Update(c);
            }
            
            return me;
        }

        public static Entity GetLinkTarget(Entity me) {
            return me.Get<TMe>().Other;
        }

        public static Entity DestroyLink(Entity me) {
            var other = me.Get<TMe>().Other;
            me.Remove<TMe>();
            other.Remove<TOther>();
            return me;
        }

        public static bool HasLink(Entity me) {
            return me.Has<TMe>();
        }
        
        public OneToOneLinkComponent() {
        } 

        public TMe DeepCopy() {
            return new TMe {
                Other = Other
            };
        }

        object IDeepCopy.DeepCopy() => DeepCopy();

        public IComponent Default() {
            return new TMe {Other = default};
        }

        public void CleanUp() {
                Other = null;
        }
        
        public void RelinkToOther(Universe u, Entity me){
            var other = u.Entities().First(e => e.Rid == _otherRid);
            CreateLink(me, other);
        }
        
        public  XElement ToXml(string elementName) {
            return new XElement(elementName, new XAttribute("OtherRid", Other.Rid.ToString()));
        }

        public void FromXml(XElement element)
        {
            var attribute = element.Attribute("OtherRid");
            if (attribute != null) {
                _otherRid = ulong.Parse(attribute.Value);
            }
        }
    }


}