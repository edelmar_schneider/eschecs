using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.Util;

namespace Ce.Game.Components.Generic {

    /// <summary>
    /// Any type of single value component can derive from this
    /// </summary>
    public abstract class DataComponent<TData> : IComponent {
        
        public TData Value;

        public abstract IComponent DeepCopy();

        object IDeepCopy.DeepCopy() => DeepCopy();


        public abstract IComponent Default();


        public virtual void CleanUp() => Value = default;

        protected abstract string ToString(TData value);
        
        protected abstract TData ParseValue(string value);
        
        public virtual XElement ToXml(string elementName) {
            return new XElement(elementName, new XAttribute("Data", ToString(Value)));
        }

        public virtual void FromXml(XElement element)
        {
            var attribute = element.Attribute("Data");
            if (attribute != null) {
                Value = ParseValue(attribute.Value);
            }
        }

        public abstract bool IsSerializable { get; }

    }

}