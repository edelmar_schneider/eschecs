using Ce.Core.Ecs;

namespace Ce.Game.Components.Generic {

    public abstract class StringComponent<TComponent> : DataComponent<string>
        where TComponent: StringComponent<TComponent>, new()
    {

        public override IComponent DeepCopy() => new TComponent{Value = string.Copy(Value)};

        public override IComponent Default() => new TComponent{Value = string.Empty};

        public override void CleanUp() => Value = string.Empty;
        
        protected override string ParseValue(string value) =>value;

        protected override string ToString(string value) => value;

        public override bool IsSerializable => true;


    }

}