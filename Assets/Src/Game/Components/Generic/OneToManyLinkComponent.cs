using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.Extensions;
using Ce.Core.Util;

namespace Ce.Game.Components.Generic {

    public abstract class OneToManyLinkComponent<TMe, TOther> : IComponent
        where TMe: OneToManyLinkComponent<TMe, TOther>, new()
        where TOther: ManyToOneLinkComponent<TOther, TMe>, new()
    {
        public HashSet<Entity> Others;
        
        public virtual bool IsSerializable => false;
        
        public XElement ToXml(string name) => throw new NotImplementedException();

        public void FromXml(XElement element) { throw new NotImplementedException(); }
        
        public static Entity CreateLink(Entity me, Entity other) {
            var u = me.Universe;
            
            //TODO double check if links between other and me are being destroyed properly
            
            //Link me to the other
            {
                var c = u.AcquireComponent<TMe>();
                //Try and copy the links that I have already
                if (me.Has<TMe>()) {
                    var oc = me.Get<TMe>();
                    foreach (var ocOther in oc.Others) {
                        c.Others.Add(ocOther);
                    }
                }
                c.Others.Add(other);
                me.Update(c);
            }
            //Link the other to me
            {
                var c = u.AcquireComponent<TOther>();
                c.Other = me;
                other.Update(c);
            }
            
            return me;
        }

        public static HashSet<Entity> GetLinkTarget(Entity me) {
            return me.Get<TMe>().Others;
        }

        public static Entity DestroyLink(Entity me) {
            
            //Delete reference to me in others
            var c = me.Get<TMe>();
            foreach (var other in c.Others) {
                other.Remove<TOther>();
            }
            
            //Delete reference to others in me.
            me.Remove<TMe>();
            
            return me;
        }

        public static bool HasLink(Entity me) {
            return me.Has<TMe>();
        }

        
        //TODO. Fix this, not really a deep copy.
        public TMe DeepCopy() {
            return new TMe {
                Others = Others.ToHashSet()
            };
        }
        object IDeepCopy.DeepCopy() => DeepCopy();

        public IComponent Default() {
            return new TMe {Others = new HashSet<Entity>()};
        }

        public void CleanUp() {
            Others.Clear();
        }

    }

}