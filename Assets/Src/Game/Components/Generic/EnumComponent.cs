using System;

namespace Ce.Game.Components.Generic {

    /// <summary>
    /// Abstract class that can be used for enum values which adds support for Has, Overlaps, Is
    /// </summary>
    public abstract class EnumComponent<TData, TComponent> : ValueComponent<TData, TComponent>
        where TData : struct, /* Can't have a constraint on Enum, next best thing */ IConvertible
        where TComponent : ValueComponent<TData, TComponent>, new() {

        protected override string ToString(TData value) => value.ToString();

        protected override TData ParseValue(string value) => (TData)global::System.Enum.Parse(typeof(TData), value);


    }

}