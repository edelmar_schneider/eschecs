using System.Linq;
using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.Util;

namespace Ce.Game.Components.Generic {

    public abstract class ManyToOneLinkComponent<TMe, TOther> : IComponent, ILink
        where TMe: ManyToOneLinkComponent<TMe, TOther>, new()
        where TOther: OneToManyLinkComponent<TOther, TMe>, new()
    {
        
        public Entity Other;
        
        /// <summary>
        /// This is used only during loading time.
        /// </summary>
        private ulong _otherRid;
        
        public virtual bool IsSerializable => true;
        
        public static Entity CreateLink(Entity me, Entity other) {
            var u = me.Universe;
            
            //TODO double check if links between other and me are being destroyed properly
            
            //Link me to the other
            {
                var c = u.AcquireComponent<TMe>();
                c.Other = other;
                me.Update(c);
            }
            //Link the other to me
            {
                var c = u.AcquireComponent<TOther>();
                //Copy the values already present at the existent component.
                if (other.Has<TOther>()) {
                    var oc = other.Get<TOther>();
                    foreach (var ocOther in oc.Others) {
                        c.Others.Add(ocOther);
                    }
                }

                //Add myself and update
                c.Others.Add(me);
                other.Update(c);
            }
            
            return me;
        }

        public static Entity GetLinkTarget(Entity me) {
            return me.Get<TMe>().Other;
        }

        public static Entity DestroyLink(Entity me) {

            //Get reference to other
            var other = me.Get<TMe>().Other;
            
            //Remove the reference in me
            me.Remove<TMe>();

            //The other has list that might have many references
            var listInOther = other.Get<TOther>()
                                   .Others;

            //I ask the other to remove me from the list.
            listInOther.Remove(me);

            //If the list of the other is empty I remove the component
            var listIsEmpty = !listInOther.Any();
            if (listIsEmpty) {
                other.Remove<TOther>();
            }
            else {
                //Otherwise I update the component
                var u = me.Universe;
                var c = u.AcquireComponent<TOther>();
                foreach (var entity in listInOther) {
                    c.Others.Add(entity);
                }
                
                //The list has all original values except me
                other.Update(c);
            }
            
            return me;
        }

        public static bool HasLink(Entity me) {
            return me.Has<TMe>();
        }
        
        public TMe DeepCopy() {
            return new TMe {
                Other = Other
            };
        }

        object IDeepCopy.DeepCopy() => DeepCopy();

        public IComponent Default() {
            return new TMe {Other = default};
        }

        public void CleanUp() {
            Other = null;
        }
        
        public void RelinkToOther(Universe u, Entity me){
            var other = u.Entities().First(e => e.Rid == _otherRid);
            CreateLink(me, other);
        }
        
        public  XElement ToXml(string elementName) {
            return new XElement(elementName, new XAttribute("OtherRid", Other.Rid.ToString()));
        }

        public void FromXml(XElement element)
        {
            var attribute = element.Attribute("OtherRid");
            if (attribute != null) {
                _otherRid = ulong.Parse(attribute.Value);
            }
        }

    }

}