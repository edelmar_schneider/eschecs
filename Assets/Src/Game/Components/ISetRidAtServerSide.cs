namespace Ce.Game.Components {

    public interface ISetRidAtServerSide {
        void SetRid(ulong rid);
    }

}