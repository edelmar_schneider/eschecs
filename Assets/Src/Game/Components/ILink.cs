using Ce.Core.Ecs;

namespace Ce.Game.Components {

    /// <summary>
    /// During loading all Links have to be processed after all entities have been created.
    /// </summary>
    public interface ILink {

        void RelinkToOther(Universe u, Entity me);

    }

}