using Ce.Core.Ecs;
using Ce.Game.Components.Generic;
 
namespace Ce.Game.Components.Links {
 
    public class AreaToGodComponent : OneToOneLinkComponent<AreaToGodComponent, GodToAreaComponent> {
    }
 
    public static class AreaToGodHelpers {
         
        public static Entity LinkAreaToGod(this Entity me, Entity other) {
            return AreaToGodComponent.CreateLink(me, other);
        }
 
        public static Entity GetLinkAreaToGod(this Entity me) {
            return AreaToGodComponent.GetLinkTarget(me);
        }
 
        public static bool HasLinkAreaToGod(this Entity me) {
            return AreaToGodComponent.HasLink(me);
        }
        
        public static Entity RemoveLinkAreaToGod(this Entity me) {
            return AreaToGodComponent.DestroyLink(me);
        }
    }    
}