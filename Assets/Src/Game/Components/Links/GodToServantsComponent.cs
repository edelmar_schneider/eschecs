using System.Collections.Generic;
using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Links {

    public class GodToServantsComponent : OneToManyLinkComponent<GodToServantsComponent, ServantsToGodComponent> {
    }

    public static class GotToServantsHelpers {
        
        /// <summary>
        /// Link a got to his servant
        /// </summary>
        public static Entity LinkGodToServants(this Entity me, Entity other) {
            return GodToServantsComponent.CreateLink(me, other);
        }

        /// <summary>
        /// Get all servants of a god
        /// </summary>
        public static HashSet<Entity> GetLinkGodToServants(this Entity me) {
            return GodToServantsComponent.GetLinkTarget(me);
        }

        /// <summary>
        /// Has servants
        /// </summary>
        public static bool HasLinkGodToServants(this Entity me) {
            return GodToServantsComponent.HasLink(me);
        }
        
        public static Entity RemoveLinkGodToServants(this Entity me) {
            return GodToServantsComponent.DestroyLink(me);
        }        
    }    
}