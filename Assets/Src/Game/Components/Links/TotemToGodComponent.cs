using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Links {

    public class TotemToGodComponent : OneToOneLinkComponent<TotemToGodComponent,GodToTotemComponent> {
    }
    
    public static class TotemToGodHelpers {
        public static Entity LinkTotemToGod(this Entity me, Entity other) {
            return TotemToGodComponent.CreateLink(me, other);
        }

        public static Entity GetLinkTotemToGod(this Entity me) {
            return TotemToGodComponent.GetLinkTarget(me);
        }

        public static bool HasLinkTotemToGod(this Entity me) {
            return TotemToGodComponent.HasLink(me);
        }
        
        public static Entity RemoveLinkTotemToGod(this Entity me) {
            return TotemToGodComponent.DestroyLink(me);
        }           
    }

}