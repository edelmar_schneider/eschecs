using Ce.Core.Ecs;
using Ce.Game.Components.Generic;
 
namespace Ce.Game.Components.Links {
 
    public class GodToPlayerComponent : OneToOneLinkComponent<GodToPlayerComponent, PlayerToGodComponent> {
    }
 
    public static class GodToPlayerHelpers {
         
        public static Entity LinkGodToPlayer(this Entity me, Entity other) {
            return GodToPlayerComponent.CreateLink(me, other);
        }
 
        public static Entity GetLinkGodToPlayer(this Entity me) {
            return GodToPlayerComponent.GetLinkTarget(me);
        }
 
        public static bool HasLinkGodToPlayer(this Entity me) {
            return GodToPlayerComponent.HasLink(me);
        }
        
        public static Entity RemoveLinkGodToPlayer(this Entity me) {
            return GodToPlayerComponent.DestroyLink(me);
        }
    }    
}