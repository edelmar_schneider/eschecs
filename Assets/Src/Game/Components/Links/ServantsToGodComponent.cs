using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Links {

    public class ServantsToGodComponent : ManyToOneLinkComponent<ServantsToGodComponent, GodToServantsComponent> {
    }

    public static class ServantsToGodHelpers {
        
        /// <summary>
        /// Link this servant to his god
        /// </summary>
        public static Entity LinkServantsToGod(this Entity me, Entity other) {
            return ServantsToGodComponent.CreateLink(me, other);
        }

        /// <summary>
        /// Get the god of this servant
        /// </summary>
        public static Entity GetLinkServantsToGod(this Entity me) {
            return ServantsToGodComponent.GetLinkTarget(me);
        }

        /// <summary>
        /// Has this servant a god?
        /// </summary>
        public static bool HasLinkServantsToGod(this Entity me) {
            return ServantsToGodComponent.HasLink(me);
        }
        
        public static Entity RemoveLinkServantsToGod(this Entity me) {
            return ServantsToGodComponent.DestroyLink(me);
        }    
    }    
}