using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Links {

    public class GodToTotemComponent : OneToOneLinkComponent<GodToTotemComponent, TotemToGodComponent> {
    }

    public static class GodToTotemHelpers {
        
        public static Entity LinkGodToTotem(this Entity me, Entity other) {
            return GodToTotemComponent.CreateLink(me, other);
        }

        public static Entity GetLinkGodToTotem(this Entity me) {
            return GodToTotemComponent.GetLinkTarget(me);
        }

        public static bool HasLinkGodToTotem(this Entity me) {
            return GodToTotemComponent.HasLink(me);
        }
        
        public static Entity RemoveLinkGodToTotem(this Entity me) {
            return GodToTotemComponent.DestroyLink(me);
        }        
    }    
}