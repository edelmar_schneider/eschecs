using Ce.Core.Ecs;
 using Ce.Game.Components.Generic;
 
 namespace Ce.Game.Components.Links {
 
     public class PlayerToGodComponent : OneToOneLinkComponent<PlayerToGodComponent, GodToPlayerComponent> {
     }
 
     public static class PlayerToGodHelpers {
         
         public static Entity LinkPlayerToGod(this Entity me, Entity other) {
             return PlayerToGodComponent.CreateLink(me, other);
         }
 
         public static Entity GetLinkPlayerToGod(this Entity me) {
             return PlayerToGodComponent.GetLinkTarget(me);
         }
 
         public static bool HasLinkPlayerToGod(this Entity me) {
             return PlayerToGodComponent.HasLink(me);
         }
         
         public static Entity RemoveLinkPlayerToGod(this Entity me) {
             return PlayerToGodComponent.DestroyLink(me);
         }    
     }    
 }