using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Links {

    public class GodToSelectedComponent : OneToOneLinkComponent<GodToSelectedComponent, SelectedToGodComponent> {
        
        public override bool IsSerializable => false;
    }

    public static class GodToSelectedHelpers {
        
        public static Entity LinkGodToSelected(this Entity me, Entity other) {
            return GodToSelectedComponent.CreateLink(me, other);
        }

        public static Entity GetLinkGodToSelected(this Entity me) {
            return GodToSelectedComponent.GetLinkTarget(me);
        }

        public static bool HasLinkGodToSelected(this Entity me) {
            return GodToSelectedComponent.HasLink(me);
        }
        
        public static Entity RemoveLinkGodToSelected(this Entity me) {
            return GodToSelectedComponent.DestroyLink(me);
        }        
    }    
}