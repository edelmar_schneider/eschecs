using Ce.Core.Ecs;
using Ce.Game.Components.Generic;
 
namespace Ce.Game.Components.Links {
 
    public class GodToAreaComponent : OneToOneLinkComponent<GodToAreaComponent, AreaToGodComponent> {
    }
 
    public static class GodToAreaHelpers {
         
        public static Entity LinkGodToArea(this Entity me, Entity other) {
            return GodToAreaComponent.CreateLink(me, other);
        }
 
        public static Entity GetLinkGodToArea(this Entity me) {
            return GodToAreaComponent.GetLinkTarget(me);
        }
 
        public static bool HasLinkGodToArea(this Entity me) {
            return GodToAreaComponent.HasLink(me);
        }

        public static Entity RemoveLinkGodToArea(this Entity me) {
            return GodToAreaComponent.DestroyLink(me);
        }
    }    
}