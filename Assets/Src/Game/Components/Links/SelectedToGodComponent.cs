using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Links {

    public class SelectedToGodComponent : OneToOneLinkComponent<SelectedToGodComponent,GodToSelectedComponent> {

        public override bool IsSerializable => false;

    }
    
    public static class SelectedToGodHelpers {
        public static Entity LinkSelectedToGod(this Entity me, Entity other) {
            return SelectedToGodComponent.CreateLink(me, other);
        }

        public static Entity GetLinkSelectedToGod(this Entity me) {
            return SelectedToGodComponent.GetLinkTarget(me);
        }

        public static bool HasLinkSelectedToGod(this Entity me) {
            return SelectedToGodComponent.HasLink(me);
        }
        
        public static Entity RemoveLinkSelectedToGod(this Entity me) {
            return SelectedToGodComponent.DestroyLink(me);
        }           
    }

}