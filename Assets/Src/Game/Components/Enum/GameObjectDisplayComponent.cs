using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Enum {


    public enum DisplayTypes {
        /// <summary> GameObject should no be instantiated </summary>
        None,
        /// <summary> GameObject should be instantiated but invisible </summary>
        Hidden,
        /// <summary> GameObject should be instantiated and visible </summary>
        Shown,
    }

    /// <summary>
    /// Controls how to display the gameobject.
    /// </summary>
    public class GameObjectDisplayComponent: EnumComponent<DisplayTypes, GameObjectDisplayComponent> {
    }
    
    public static class GameObjectDisplayComponentHelpers {
        public static Entity SetGameObjectDisplay(this Entity e, DisplayTypes value) {
            var u = e.Universe;
            var c = u.AcquireComponent<GameObjectDisplayComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static DisplayTypes GetGameObjectDisplay(this Entity e) => e.Get<GameObjectDisplayComponent>().Value;

        public static bool HasGameObjectDisplay(this Entity e) => e.Has<GameObjectDisplayComponent>();

        public static Entity RemoveGameObjectDisplay(this Entity e) => e.Remove<GameObjectDisplayComponent>();
    }  


}