using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Enum {

    public enum IntModes {
        Select,
        StructureSelected,
        ActorSelected,
        WaitingTurnToPass,
        EndGame
    }

    /// <summary>
    /// Defines the operation that is ongoing with the p 
    /// </summary>
    public class InteractionModeComponent: EnumComponent<IntModes, InteractionModeComponent> {
    }
    
    public static class InteractionModeComponentComponentHelpers {
        public static Entity SetInteractionMode(this Entity e, IntModes value) {
            var u = e.Universe;
            var c = u.AcquireComponent<InteractionModeComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static IntModes GetInteractionMode(this Entity e) => e.Get<InteractionModeComponent>().Value;

        public static bool HasInteractionMode(this Entity e) => e.Has<InteractionModeComponent>();

        public static Entity RemoveInteractionMode(this Entity e) => e.Remove<InteractionModeComponent>();
    }  

}