using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Enum {
    
    /// <summary>
    /// Game class. Try to use components and propbits first to differentiate entities and then the GClass
    /// for example to differentiate a warrior of an worker
    /// </summary>
    public enum GClasses {
        Undefined,
        Warrior,
        Worker,
        Totem,
        Tower,
        Rock,
        Wood
    }

    /// <summary>
    /// Defines the high level character of an entity. 
    /// </summary>
    public class GClassComponent: EnumComponent<GClasses, GClassComponent> {
    }
    
    public static class GClassComponentComponentHelpers {
        public static Entity SetGClass(this Entity e, GClasses value) {
            var u = e.Universe;
            var c = u.AcquireComponent<GClassComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static GClasses GetGClass(this Entity e) => e.Get<GClassComponent>().Value;

        public static bool HasGClass(this Entity e) => e.Has<GClassComponent>();

        public static Entity RemoveGClass(this Entity e) => e.Remove<GClassComponent>();
    }  

}