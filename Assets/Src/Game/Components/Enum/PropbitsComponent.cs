using Ce.Core.Ecs;
using Ce.Game.Components.Generic;
using Ce.Game.World;

namespace Ce.Game.Components.Enum {


    /// <summary>
    /// Propbits are bit flags that define properties of entity that can be cached into the world map. 
    /// </summary>
    public class PropbitsComponent: EnumComponent<Propbits, PropbitsComponent> {
    }
    
    public static class PropbitsComponentHelpers {
        public static Entity SetPropbits(this Entity e, Propbits value) {
            var u = e.Universe;
            var c = u.AcquireComponent<PropbitsComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static Propbits GetPropbits(this Entity e) => e.Get<PropbitsComponent>().Value;

        public static bool HasPropbits(this Entity e) => e.Has<PropbitsComponent>();

        public static Entity RemovePropbits(this Entity e) => e.Remove<PropbitsComponent>();
    }  


}