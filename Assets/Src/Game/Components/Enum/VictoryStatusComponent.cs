using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Enum {
    
    public enum VictoryStatuses {
        Lost,
        Won,
    }

    /// <summary>
    /// Add to a god to define if he lost or won the game. 
    /// </summary>
    public class VictoryStatusComponent: EnumComponent<VictoryStatuses, VictoryStatusComponent> {
    }
    
    public static class VictoryStatusComponentComponentHelpers {
        public static Entity SetVictoryStatus(this Entity e, VictoryStatuses value) {
            var u = e.Universe;
            var c = u.AcquireComponent<VictoryStatusComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static VictoryStatuses GetVictoryStatus(this Entity e) => e.Get<VictoryStatusComponent>().Value;

        public static bool HasVictoryStatus(this Entity e) => e.Has<VictoryStatusComponent>();

        public static Entity RemoveVictoryStatus(this Entity e) => e.Remove<VictoryStatusComponent>();
    }  

}