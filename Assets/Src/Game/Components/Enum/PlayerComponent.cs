using Ce.Core.Ecs;
using Ce.Game.Components.Generic;

namespace Ce.Game.Components.Enum {

    public enum PlayerTypes {
        Human,
        Ai
    }

    /// <summary>
    /// Defines an entity as a Player and sets who should control it: an human or Ai
    /// </summary>
    public class PlayerComponent: EnumComponent<PlayerTypes, PlayerComponent> {
    }
    
    public static class PlayerComponentHelpers {
        public static Entity SetPlayer(this Entity e, PlayerTypes value) {
            var u = e.Universe;
            var c = u.AcquireComponent<PlayerComponent>();
            c.Value = value;
            e.Update(c);
            return e;
        }

        public static PlayerTypes GetPlayer(this Entity e) => e.Get<PlayerComponent>().Value;

        public static bool HasPlayer(this Entity e) => e.Has<PlayerComponent>();

        public static Entity RemovePlayer(this Entity e) => e.Remove<PlayerComponent>();
    }  

}