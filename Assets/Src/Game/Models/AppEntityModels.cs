using Ce.Core.Ecs;
using Ce.Core.Util;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.String;
using Ce.Game.Components.Tags;

namespace Ce.Game.Models {

    /// <summary>
    /// For entities that are related to the app managing a are not game entities like warriors and dragons.
    /// </summary>
    public class AppEntityModels: IModels {
        
        /// <summary>
        /// The game definition entity contains information about the properties to start the game e.g. MapSize, NumOfPlayers...
        /// </summary>
        public Model GameDefinitionEmpty{ get; private set; }
        public Model GameDefinitionNoPortServer{ get; private set; }
        public Model GameDefinitionNoMapServer { get; private set; }
        public Model GameDefinitionReadyServer { get; private set; }
        
        public Model GameDefinitionNoIpClient { get; private set; }
        
        public Model GameDefinitionReadyClient { get; private set; }
        public Model GameDefinitionAny { get; private set; }
        
        public Model GameDefinitionGroundCombat { get; private set; }
        
        /// <summary>
        /// Every client connected to the server will generate one of these at the server side only.
        /// This is how you know who is connected to the server.
        /// </summary>
        public Model ServerClient { get; private set; }

        public AppEntityModels() {
        }

        public void Setup(Universe universe) {
            
            

            ServerClient = Model
                .New(Matcher.New(universe)
                            .All<ServerSideComponent, NetworkClientIdComponent>()
                    );
            
            GameDefinitionGroundCombat = Model
                .New(Matcher.New(universe)
                            .All<GameDefinitionsComponent, GroundCombatComponent>()
                    );

            GameDefinitionAny = Model
                .New(Matcher.New(universe)
                            .All<GameDefinitionsComponent>()
                    );


            GameDefinitionEmpty = Model
                .New(Matcher.New(universe)
                            .All<GameDefinitionsComponent>()
                            .None<MapSizeComponent, PlayersComponent, PortComponent,
                                ServerComponent, ClientComponent>()
                    );

            GameDefinitionNoPortServer = Model
                .New(Matcher.New(universe)
                            .All<GameDefinitionsComponent, ServerComponent>()
                            .None<MapSizeComponent, PlayersComponent, PortComponent>()
                    );

            GameDefinitionNoMapServer = Model
                .New(Matcher.New(universe)
                            .All<GameDefinitionsComponent, ServerComponent, PortComponent
                            >()
                            .None<MapSizeComponent, PlayersComponent>()
                    );

            GameDefinitionReadyServer = Model
                .New(Matcher.New(universe)
                            .All<GameDefinitionsComponent, ServerComponent, PortComponent,
                                MapSizeComponent,
                                PlayersComponent>()
                    );

            GameDefinitionNoIpClient = Model
                .New(Matcher.New(universe)
                            .All<GameDefinitionsComponent, ClientComponent, PortComponent>()
                            .None<IpComponent>()
                    );

            GameDefinitionReadyClient = Model
                .New(Matcher.New(universe)
                            .All<GameDefinitionsComponent, ClientComponent, PortComponent, IpComponent>()
                    );
        }

    }

}