using Ce.Core.Ecs;
using Ce.Core.Util;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.String;
using Ce.Game.Components.Tags;

namespace Ce.Game.Models {

    public class GameEntityModels : IModels{
        
        /// <summary>
        /// Every client of the game will have a copy of the players.
        /// Some player will be humans others will be AIs
        /// </summary>
        public Model Player { get; private set; }
        
        public Model LocalPlayer { get; private set; }
        
        public Model God { get; private set; }
        
        /// <summary>
        /// An entity that can be displayed as an 3d object.
        /// </summary>
        public Model Entity3dPrefab { get; private set; }
        
        /// <summary>
        /// An entity that is being displayed as an 3d object.
        /// </summary>
        public Model Entity3dObject { get; private set; }

        public GameEntityModels() { }

        public void Setup(Universe universe) {
            
            Player = Model
                .New(Matcher.New(universe)
                            .All<PlayerComponent, NidComponent, DisplayNameComponent>()
                    ).AddDefault(new NidComponent{Value = ulong.MaxValue});

            LocalPlayer = Model.New(Player, Matcher.New(universe)
                                                   .All<LocalPlayerComponent>());
            
            God = Model
                .New(Matcher.New(universe)
                            .All<GodComponent, DisplayNameComponent>()
                    );
            
            Entity3dPrefab = Model
                .New(Matcher.New(universe)
                            .All<TransformComponent, PrefabComponent, GameObjectDisplayComponent>()
                     ).AddDefault(new GameObjectDisplayComponent{Value = DisplayTypes.None});
            
            Entity3dObject = Model
                .New(Matcher.New(universe)
                            .All<TransformComponent, GameObjectComponent>()
                    );
            
        }
    }

}