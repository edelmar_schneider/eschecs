using System.Linq;
using Ce.Core.Ecs;
using Ce.Game.Components.Data;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Components.Tags;
using Ce.Game.World;
    
using GCG = Ce.Game.GameConstants.NamedGroups;
    
namespace Ce.Game.Extensions {

    public static class GameExtensions {

        public static Entity GetLocallySelected(this Universe u) {
            var lg = u.GetLocalGod();
            return lg.HasLinkGodToSelected() ? lg.GetLinkGodToSelected() : null;
        }

        /// <summary>
        /// Easy method to find the local player
        /// </summary>
        public static Entity GetLocalPlayer(this Universe u) {
            var g = u.GetGroupByName(GCG.LocalPlayer);
            //In general we expect that if the game is running we have a local player defined. 
            return g.Entities.First();
        }
        
        /// <summary>
        /// The god controlled by the local player
        /// </summary>
        public static Entity GetLocalGod(this Universe u) {
            var lp = u.GetLocalPlayer();
            var lg = lp.GetLinkPlayerToGod();
            return lg;
        }

        /// <summary>
        /// Check whether a god is controlled by an Ai player
        /// </summary>
        public static bool IsAiGod(this Entity god) {
            var player = god.GetLinkGodToPlayer();
            return player.GetPlayer() == PlayerTypes.Ai;
        }

        /// <summary>
        /// Check if an actor or building is controlled by the local player.
        /// </summary>
        public static bool IsLocalEntity(this Entity e) {

            if (!e.HasLinkServantsToGod()) {
                return false;
            }
            
            var lp = e.Universe.GetLocalGod();
            var isLocal = e.GetLinkServantsToGod().Rid == lp.Rid;
            return isLocal;
        }

        public static bool IsActor(this Entity e) {
            if (!e.HasPropbits()) {
                return false;
            }

            return e.GetPropbits().HasAll(Propbits.Actor);
        }
        
        public static bool IsWarrior(this Entity e) {
            return e.HasGClass() && e.GetGClass() == GClasses.Warrior;
        }
        
        public static bool IsWorker(this Entity e) {
            return e.HasGClass() && e.GetGClass() == GClasses.Worker;
        }
        
        public static bool IsBuilding(this Entity e) {
            if (!e.HasPropbits()) {
                return false;
            }

            return e.GetPropbits().HasAll(Propbits.Building);
        }
        
        public static bool IsWalkable(this Entity e) {
            if (!e.HasPropbits()) {
                return false;
            }

            return e.GetPropbits().HasAll(Propbits.Walkable);
        }
        
        public static bool IsResource(this Entity e) {
            if (!e.HasPropbits()) {
                return false;
            }

            return e.GetPropbits().HasAll(Propbits.Resource);
        }
        
        public static bool IsSelectable(this Entity e) {
            return e.HasSelectableTag();
        }
        
        /// <summary>
        /// Selected by the local god/player
        /// </summary>
        public static bool IsLocallySelected(this Entity e) {
            if (!e.HasLinkSelectedToGod()) {
                return false;
            }

            var selectorGod = e.GetLinkSelectedToGod();
            var localGod = e.Universe.GetLocalGod();

            var isSame = selectorGod.Rid == localGod.Rid;
            return isSame;
        }

        /// <summary>
        /// Based on GameDefinitionsAny model
        /// </summary>
        public static Entity GetGameDefinitionsEntity(this Universe u) {
            return u.GetGameDefinitionsGroup().Entities.FirstOrDefault();
        }
        
        /// <summary>
        /// Based on GameDefinitionsAny model
        /// </summary>
        public static Group GetGameDefinitionsGroup(this Universe u) {
            var g = u.GetGroupByName(GCG.GameDefinitions);
            return g;
        }

        /// <summary>
        /// Check whether all actors are not doing anything or if they are executing some task
        /// </summary>
        public static bool IsThereAnyAction(this Universe u) {
            var g = u.GetGroupByName(GCG.HitPointEntities);
            return g.Any(e => e.HasAction());
        }
        
    }

}