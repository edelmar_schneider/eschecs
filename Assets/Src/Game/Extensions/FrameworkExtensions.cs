using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Core.World;
using Ce.Game.System;

using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game.Extensions {

    public static class FrameworkExtensions {

        public static NetworkSystem Send(this Universe u) {
            return u.GetSystem<GameNetworkSystem>();
        }

        public static int AdrToId(this Universe u, Address adr) {
            return u.Param.ToId(adr);
        }
        
        public static int AdrToId(this Address adr, Universe u) {
            return u.Param.ToId(adr);
        }
        
        public static bool IsServer(this Universe u) {
            var g = u.GetGroupByName(GCG.ServerSideEntity);
            //In general we expect that if the game is running we have a local player defined. 
            return g.Entities.Any();

        }
        
    }

}