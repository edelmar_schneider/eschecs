using System;
using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Extensions;
using Ce.Core.System.Job;
using Ce.Core.Ui;
using Ce.Core.Util;
using Ce.Game.Components.Enum;
using Ce.Game.Extensions;
using Ce.Game.Models;
using Ce.Game.Ui;
using Ce.Game.Ui.Client;
using Ce.Game.Ui.Game;
using Ce.Game.Ui.Server;
using UnityEngine;
using Object = UnityEngine.Object;
using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game.System {

    /// <summary>
    /// The initial menu of the game that allows you to select the game mode, load games, etc...
    /// </summary>
    public class MainUiSystem :BaseSystem {

        private readonly string _initialMenuUiSheet = GameConstants.Resources.UiFolder.AppendPath("InitialMenuUiSheet");

        private readonly string _gamePropertiesServerUiSheet = GameConstants.Resources.UiFolder.AppendPath("GamePropertiesServerUiSheet");
        private readonly string _serverLobbyUiSheet = GameConstants.Resources.UiFolder.AppendPath("ServerLobbyUiSheet");
        
        private readonly string _gamePropertiesClientUiSheet = GameConstants.Resources.UiFolder.AppendPath("GamePropertiesClientUiSheet");
        private readonly string _clientLobbyUiSheet = GameConstants.Resources.UiFolder.AppendPath("ClientLobbyUiSheet");
        
        private readonly string _groundCombatUiSheet = GameConstants.Resources.UiFolder.AppendPath("GroundCombatUiSheet");
        
        private readonly string _winnerUiSheet = GameConstants.Resources.UiFolder.AppendPath("WinnerUiSheet");
        private readonly string _loserUiSheet = GameConstants.Resources.UiFolder.AppendPath("LoserUiSheet");
        private readonly string _loadingUiSheet= GameConstants.Resources.UiFolder.AppendPath("LoadingUiSheet");
        
        /// <summary>
        /// All the instanced ui sheets (active/inactive) will stored and cached here to be reused if needed. 
        /// </summary>
        private readonly Dictionary<Type/**/, BaseUiSheet> _instancedUiSheets = new Dictionary<Type, BaseUiSheet>(256);
        
        private readonly List<BaseUiSheet> _activeUiSheets = new List<BaseUiSheet>(256);
                
        public enum UiState
        {
            /// <summary> Initial state before the game started </summary>
            Initial,
            /// <summary> Main menu, you can select to be a server or a client </summary>
            InitialMenu,
            /// <summary> Set the properties such as port, map, number of players </summary>
            Server,
            /// <summary> Where you await for other player and start the game </summary>
            ServerLobby,
            /// <summary> Set properties such as server ip:port </summary>
            Client,
            /// <summary> Where you await for the game to begin</summary>
            ClientLobby,
            /// <summary> The actual game is running</summary>
            GroundCombat,
            /// <summary> Winner screen</summary>
            Winner,
            /// <summary> Loose screen</summary>
            Loser,
            /// <summary> Loading screen</summary>
            Loading,
            /// <summary> The app is ready to be close </summary>
            Terminated
        }

        public enum UiTrigger {
            
            /// <summary> Go to Server menu path </summary>
            Server,
            /// <summary> Go to Client menu path </summary>
            Client,
            /// <summary> Advance towards the game </summary>
            Next,
            /// <summary> Return to previous menu</summary>
            Return,
            /// <summary> Show the win screen</summary>
            Win,
            /// <summary> Show the loose screen</summary>
            Lose,
            /// <summary> Show the loading screen</summary>
            Loading
        }

        /// <summary>
        /// Used during loading operation to avoid having the ui responding to entities being created. 
        /// </summary>
        public void LoadingOperation(bool loading) {
            if (loading) {
                Trigger(UiTrigger.Loading);
            }
            else {
                Universe.QueueTask(() => {Trigger(UiTrigger.Return);}, 2f);
                
            }
        }

        private StateMachine<UiState, UiTrigger> _stateMachine;

        private Group _gods;

        protected override void OnInitialize() {
            base.OnInitialize();
            
            _stateMachine = new StateMachine<UiState, UiTrigger>(UiState.Initial);
            
            _stateMachine.Configure()
                        .State(UiState.Initial)
                        .Trigger(UiTrigger.Next, UiState.InitialMenu);

            _stateMachine.Configure()
                        .State(UiState.InitialMenu)
                        .Trigger(UiTrigger.Server, UiState.Server)
                        .Trigger(UiTrigger.Client, UiState.Client)
                        .Trigger(UiTrigger.Return, UiState.Terminated)
                        .OnEnter((oldState, newState) => ReplaceUiSheet<InitialMenuUiSheet>(_initialMenuUiSheet));

            _stateMachine.Configure()
                        .State(UiState.Server)
                        .Trigger(UiTrigger.Next, UiState.ServerLobby)
                        .Trigger(UiTrigger.Return, UiState.InitialMenu)
                        .OnEnter((oldState, newState) => ReplaceUiSheet<GamePropertiesServerUiSheet>(_gamePropertiesServerUiSheet));
            
            _stateMachine.Configure()
                        .State(UiState.ServerLobby)
                        .Trigger(UiTrigger.Next, UiState.GroundCombat)
                        .Trigger(UiTrigger.Return, UiState.Server)
                        .OnEnter((oldState, newState) => ReplaceUiSheet<ServerLobbyUiSheet>(_serverLobbyUiSheet));
            
            _stateMachine.Configure()
                        .State(UiState.GroundCombat)
                        .Trigger(UiTrigger.Return, UiState.InitialMenu)
                        .Trigger(UiTrigger.Win, UiState.Winner)
                        .Trigger(UiTrigger.Lose, UiState.Loser)
                        .Trigger(UiTrigger.Loading, UiState.Loading)
                        .OnEnter((oldState, newState) => ReplaceUiSheet<GroundCombatUiSheet>(_groundCombatUiSheet));
            
            _stateMachine.Configure()
                         .State(UiState.Winner)
                         .Trigger(UiTrigger.Next, UiState.InitialMenu)
                         .OnEnter((oldState, newState) => ReplaceUiSheet<WinUiSheet>(_winnerUiSheet));
            
            _stateMachine.Configure()
                         .State(UiState.Loser)
                         .Trigger(UiTrigger.Next, UiState.InitialMenu)
                         .OnEnter((oldState, newState) => ReplaceUiSheet<LoseUiSheet>(_loserUiSheet));        
            
            _stateMachine.Configure()
                         .State(UiState.Loading)
                         .Trigger(UiTrigger.Return, UiState.GroundCombat)
                         .Trigger(UiTrigger.Next, UiState.GroundCombat)
                         .OnEnter((oldState, newState) => ReplaceUiSheet<LoadingUiSheet>(_loadingUiSheet));   
            
            _stateMachine.Configure()
                        .State(UiState.Client)
                        .Trigger(UiTrigger.Next, UiState.ClientLobby)
                        .Trigger(UiTrigger.Return, UiState.InitialMenu)
                        .OnEnter((oldState, newState) => ReplaceUiSheet<GamePropertiesClientUiSheet>(_gamePropertiesClientUiSheet));
            
            _stateMachine.Configure()
                        .State(UiState.ClientLobby)
                        .Trigger(UiTrigger.Next, UiState.GroundCombat)
                        .Trigger(UiTrigger.Return, UiState.Client)
                        .OnEnter((oldState, newState) => ReplaceUiSheet<ClientLobbyUiSheet>(_clientLobbyUiSheet));

            var sm = Universe.GetModels<AppEntityModels>();

            _gods = Universe.GetGroupByName(GCG.Gods)
                            .OnComponentAdded<VictoryStatusComponent>((g, e, comp) => {
                                                                          var lg = Universe.GetLocalGod();
                                                                          if (e.Rid == lg.Rid) {
                                                                              //It's me
                                                                              if (comp.Value == VictoryStatuses.Won) {
                                                                                  Trigger(UiTrigger.Win);
                                                                              }
                                                                              else {
                                                                                  Trigger(UiTrigger.Lose);
                                                                              }
                                                                              
                                                                          }
                                                                      });
            
            RegisterGroup(sm.GameDefinitionGroundCombat, "MainUiSystem")
                .OnEntityAdded(() => {
                                   if (_stateMachine.State() == UiState.Loading) return;
                                   Trigger(UiTrigger.Next);
                               });
            
            //A game definition with no map size will bring up the server ui
            RegisterGroup(sm.GameDefinitionNoIpClient, "MainUiSystem")
                .OnEntityAdded(() => {
                                   if (_stateMachine.State() == UiState.Loading) return;
                                   if (_stateMachine.State() == UiState.ClientLobby) {
                                       Trigger(UiTrigger.Return);
                                   }
                                   else {
                                       Trigger(UiTrigger.Client);
                                   }
                               });
            
            //A ready game definition will bring up the server lobby 
            RegisterGroup(sm.GameDefinitionReadyClient, "MainUiSystem")
                .OnEntityAdded(() => {
                                   if (_stateMachine.State() == UiState.Loading) return;
                                   Trigger(UiTrigger.Next);
                               });
            
            
            //An empty game definition will bring up the initial menu
            RegisterGroup(sm.GameDefinitionEmpty, "MainUiSystem")
                .OnEntityAdded(() => {
                                   if (_stateMachine.State() == UiState.Loading) return;
                                   if (_stateMachine.State() == UiState.Initial) {
                                       Trigger(UiTrigger.Next);
                                   }
                                   else {
                                       Trigger(UiTrigger.Return);
                                   }
                               });
            
            //A game definition with no map size will bring up the server ui
            RegisterGroup(sm.GameDefinitionNoMapServer, "MainUiSystem")
                .OnEntityAdded(() => {
                                   if (_stateMachine.State() == UiState.Loading) return;
                                   if (_stateMachine.State() == UiState.ServerLobby) {
                                       Trigger(UiTrigger.Return);
                                   }
                                   else {
                                       Trigger(UiTrigger.Server);
                                   }
                                       
                               });
            
            //A ready game definition will bring up the server lobby 
            RegisterGroup(sm.GameDefinitionReadyServer, "MainUiSystem")
                .OnEntityAdded(() => {
                                   if (_stateMachine.State() == UiState.Loading) return;
                                   Trigger(UiTrigger.Next);
                               });
            
            //A game definition with no map size will bring up the server ui
            RegisterGroup(sm.GameDefinitionNoMapServer, "MainUiSystem")
                .OnEntityAdded(() => {
                                   if (_stateMachine.State() == UiState.Loading) return;
                                   if (_stateMachine.State() == UiState.ServerLobby) {
                                       Trigger(UiTrigger.Return);
                                   }
                                   else {
                                       Trigger(UiTrigger.Server);
                                   }
                               });            

        }

        protected override void OnTerminate() {
            DeleteAllSheets();
            base.OnTerminate();
        }

        public void Trigger(UiTrigger trigger) {
            _stateMachine.Trigger(trigger);
        }

        private void ReplaceUiSheet<T>(string fullPath) where T : BaseUiSheet {
            
            HideActiveUiSheets();
            //Try and find the type T in the dictionary
            var uiType = typeof(T);
            if (_instancedUiSheets.ContainsKey(uiType)) {
                var uiSheet = _instancedUiSheets[uiType];
                _activeUiSheets.Add(uiSheet);
                uiSheet.Show();
            }
            else {
                var prefab = Resources.Load(fullPath, typeof(GameObject));
                var newUiSheetGo = Object.Instantiate(prefab) as GameObject;
                var newUiSheet = newUiSheetGo.GetComponent<T>()
                            .Setup(Universe)
                            .Show();
                
                _activeUiSheets.Add(newUiSheet);
                _instancedUiSheets[uiType] = newUiSheet;
            }
        }

        public void HideActiveUiSheets() {
            foreach (var activeUiSheet in _activeUiSheets) {
                activeUiSheet.Hide();
            }
            
            _activeUiSheets.Clear();
        }

        private void DeleteAllSheets() {
            
            foreach (var sheet in _instancedUiSheets.Values.ToArray()) {
                if (sheet != null) {
                    Object.Destroy(sheet.gameObject);
                }
            }
            
            _instancedUiSheets.Clear();
        }

    }

}