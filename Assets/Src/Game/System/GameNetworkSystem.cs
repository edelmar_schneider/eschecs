using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.Tags;
using Ce.Game.Models;
using MLAPI;
using UnityEngine;

namespace Ce.Game.System {

    public class GameNetworkSystem : NetworkSystem {

        public bool Hosting { get; private set; }
        private bool _connectedToServer;

        private Group _clientsGroup;

        protected override void OnInitialize() {
            base.OnInitialize();
            
            var sm = Universe.GetModels<AppEntityModels>();
            var gem = Universe.GetModels<GameEntityModels>();
            
            RegisterGroup(gem.Player, "All Player")
                .OnEntityAdded(PlayerAdded)
                .OnComponentAdded<NetworkClientIdComponent>((group, e, arg3) => PlayerAdded(e))
                .OnComponentUpdated<NetworkClientIdComponent>(OnUpdate);
            
            //Update the GameDefinitionNoPortServer entity with the default port. Later the player can update this manually
            RegisterGroup(sm.GameDefinitionEmpty, "GameDefinitionEmpty on Network" )
                .OnEntityAdded((e) => e.SetPort(NetTransport.ServerListenPort));
            
            RegisterGroup(sm.GameDefinitionReadyServer, "GameDefinitionReadyServer on Network")
                .OnEntityAdded(StartHosting)
                .OnEntityRemoved(StopHosting);
            
            RegisterGroup(sm.GameDefinitionReadyClient, "GameDefinitionReadyClient on Network")
                .OnEntityAdded(ConnectToServer)
                .OnEntityRemoved(DisconnectFromServer);

            

            _clientsGroup = RegisterGroup(sm.ServerClient, "ServerClient on Network");
        }

        private void OnUpdate(Group arg1, Entity arg2, NetworkClientIdComponent arg3, NetworkClientIdComponent arg4) {
            PlayerAdded(arg2);
        }

        private void PlayerAdded(Entity e) {
            //I need to set if the player is a local player or not.
            //There will be only one local player.
            
            if (!e.HasNetworkClientId()) {
                return;
            }
            
            var networkId = e.GetNetworkClientId();
            var localClientId = NetworkManager.Singleton.LocalClientId;
            if (networkId == localClientId) {
                e.SetLocalPlayerTag();
                Debug.Log($"Local player has the network id: {networkId} ");
            }
            else {
                if (e.HasLocalPlayerTag()) {
                    e.RemoveLocalPlayerTag();
                }
            }
        }

        protected override void OnTerminate() {
            base.OnTerminate();
            StopHosting();
            DisconnectFromServer();
        }

        public void StartHosting() {
            if (Hosting) {
                return;
            }
            
            Debug.Log("Starting hosting");
            Hosting = true;
            
            NetworkManager.Singleton.OnClientConnectedCallback += SingletonOnOnClientConnectedCallback;
            NetworkManager.Singleton.OnClientDisconnectCallback += SingletonOnOnClientDisconnectCallback;
            NetworkManager.Singleton.StartHost();
            
            //We might have clients connected already
            var clients = NetworkManager.Singleton
                                        .ConnectedClients
                                        .Select(p => p.Value)
                                        .ToList();
            
            foreach (var networkClient in clients) {
                Universe.AcquireEntity()
                        .SetNetworkClientId(networkClient.ClientId)
                        .SetServerSideTag()
                        .DefineName("Network client");
            }
            
        }
        
        
        public void StopHosting() {
            if (!Hosting) {
                return;
            }

            Debug.Log("Stopping hosting");
            if (NetworkManager.Singleton != null) {
                NetworkManager.Singleton.StopHost();
                NetworkManager.Singleton.OnClientConnectedCallback -= SingletonOnOnClientConnectedCallback;
                NetworkManager.Singleton.OnClientDisconnectCallback -= SingletonOnOnClientDisconnectCallback;
            }

            Hosting = false;
        }
        
        public void ConnectToServer() {
            if (_connectedToServer) {
                return;
            }
            NetworkManager.Singleton.StartClient();
            _connectedToServer = true;
        }

        public void DisconnectFromServer() {
            if (!_connectedToServer) {
                return;
            }
            NetworkManager.Singleton.StopClient();
            _connectedToServer = false;
        }        
        
        private void SingletonOnOnClientConnectedCallback(ulong clientId) {
            Universe.AcquireEntity()
                    .SetNetworkClientId(clientId)
                    .SetServerSideTag()
                    .DefineName("Network client");
        }

        private void SingletonOnOnClientDisconnectCallback(ulong clientId) {
            var ec = _clientsGroup.Entities.First(e => e.GetNetworkClientId() == clientId);
            Universe.RecycleEntity(ec);
        }

    }

}