using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Game.Components;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Components.Tags;
using Ce.Game.Extensions;
using Ce.Game.Message.Command;
using UnityEngine;

namespace Ce.Game.System {

    public class SaveLoadSystem: BaseSystem {

        private MessageSystem _messageSystem;

        private MainUiSystem _mainUiSystem;

        private VictoryConditionSystem _victoryCondition;

        protected override void OnInitialize() {
            base.OnInitialize();
            _victoryCondition = Universe.GetSystem<VictoryConditionSystem>();
            _mainUiSystem = Universe.GetSystem<MainUiSystem>();
            _messageSystem = Universe.GetSystem<MessageSystem>();
            _messageSystem.SubscribeToMessage<SaveLoadGameCommand>(this, HandleSaveLoadCommand);
        }

        private void HandleSaveLoadCommand(SaveLoadGameCommand c) {
            if (c.IsSave) {
                SaveGame(c.FileName);
            } else {
                LoadGame(c.FileName);
            }
        }
        
        private void SaveGame(string fileName) {

            var directory = Path.Combine(Application.dataPath, "Save");
            var fullPath = Path.Combine(Application.dataPath, "Save", fileName);

            //Create directory if it doesn't exist
            if (!string.IsNullOrEmpty(directory)){
                if (!Directory.Exists(directory)) {
                    Directory.CreateDirectory(directory);
                }
            }
            
            Debug.Log($"Saving in {fullPath}");
            
            var root = new XElement("Entities");
            
            foreach (var e in Universe.Entities()) {
                var eXml = e.ToXml("Ent");
                eXml.Add(new XAttribute("Rid", e.Rid));
                eXml.Add(new XAttribute("Name", e.Name));
                root.Add(eXml);
                
            }
            
            var document = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),root);
            document.Save(fullPath);
        }
        
        private void LoadGame(string fileName) {

            _mainUiSystem.LoadingOperation(true);
            _victoryCondition.LoadingOperation(true);
            
            var lg = Universe.GetLocalGod();
            if (lg.HasLinkGodToSelected()) {
                lg.RemoveLinkGodToSelected();
            }
            Universe.GetGameDefinitionsEntity().SetInteractionMode(IntModes.Select);
            
            UnloadGame();
            
            var fullPath = Path.Combine(Application.dataPath, "Save", fileName);
            var maxRid = ulong.MinValue;
            
            var document = XDocument.Load(fullPath);
            var xmlEntities = document.Element("Entities");

            var linkLate = new List<(Entity, ILink)>(4096);
            
            //Create entities and most of the components
            foreach (var xEntity in xmlEntities.Elements()) {
                var e = Universe.AcquireEntity();
                var name = xEntity.Attribute("Name").Value;
                var ridStr = xEntity.Attribute("Rid").Value;
                var rid = ulong.Parse(ridStr);
                e.ResetRid(rid);
                e.DefineName(name);
                if (rid > maxRid) {
                    maxRid = rid;
                }

                foreach (var xElement in xEntity.Elements()) {
                    var typeStr = xElement.Attribute("Tid").Value;
                    var componentSid = int.Parse(typeStr);
                    var c = Universe.AcquireComponent(componentSid);
                    c.FromXml(xElement);
                    //ILinks can only be added after all entities are created.
                    if (c is ILink l) {
                        linkLate.Add((e, l));
                    }
                    else {
                        e.Update(c, componentSid);
                    }
                }
            }
            
            //Now it's time to relink
            foreach (var (entity, link) in linkLate) {
                link.RelinkToOther(Universe, entity);
            }
            
            Universe.ResetNidGenerator(maxRid);
            
            _mainUiSystem.LoadingOperation(false);
            _victoryCondition.LoadingOperation(false);
            Universe.GetGameDefinitionsEntity().SetInteractionMode(IntModes.Select);
            
        }

        private void UnloadGame() {
            var entities = Universe.Entities().ToList();
            entities.OrderByDescending(e => e.Rid);
            foreach (var entity in entities) {
                if (entity.HasCameraTag()) {
                    continue;
                }
                Universe.RecycleEntity(entity);
            }
        }

    }
}