using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Extensions;
using Ce.Core.System;
using Ce.Core.Util;
using Ce.Game.Components.Collection;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Enum;
using Ce.Game.Components.String;
using Ce.Game.Components.Tags;
using Ce.Game.Message.Command;
using Ce.Game.World;
using Ce.Core.World;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.Value;
using UnityEngine;
using NotImplementedException = System.NotImplementedException;

namespace Ce.Game.System {

    public class BoardSystem: BaseSystem {
        
        public TileBoard Board { get; private set; }
        
        private MessageSystem _messageSystem;
        
        /// <summary>
        /// Helper list to avoid memory allocation
        /// </summary>
        private readonly List<Address> _addresses = new List<Address>(512);

        protected override void OnInitialize() {
            base.OnInitialize();

            _messageSystem = Universe.GetSystem<MessageSystem>();
            
            _messageSystem.SubscribeToMessage<CreateBoardCommand>(this, HandleCommandCreateBoard);

            RegisterGroup(Matcher.New(Universe)
                                 .All<PropbitsComponent, TransformComponent, OnBoardComponent, ShapeComponent>(), "BoardSystemTransform")
                .OnEntityAdded(OnTransformUpdate)
                .OnEntityRemoved(RemovedFromTheBoard)
                .OnComponentUpdated<TransformComponent>((g, e, oc, nc) => OnTransformUpdate(e));
            //TODO implement below functions 
            //.OnComponentUpdated<ShapeComponent>((g, e, oc, nc) => OnShapeUpdate(e))
            //.OnComponentUpdated<PropbitsComponent>((g, e, oc, nc) => OnPropbitsUpdate(e));

        }

        private void RemovedFromTheBoard(Entity e) {
            foreach (var sp in e.GetFootprint()) {
                if(Universe.Param.IsValid(sp)){
                    Board.GetElement(sp).UnlinkEntity(e);
                }
            }
        }

        protected override void OnTerminate() {
            
            _messageSystem.UnsubscribeFromMessage<CreateBoardCommand>(this);
            
            base.OnTerminate();
        }
        
        private void HandleCommandCreateBoard(CreateBoardCommand m) {
            
            CreateBoard(m.Size, m.Players);
            PopulateBoard();
        
        }

        private void PopulateBoard() {
            
        }
        
        public enum EntityShape {
            None,
            CenteredCircle,
            CenteredRectangle,
            CornerRectangle
        }

        public enum EntitySize: int {
            One = 1, 
            Three = 3,
            Five = 5,
            Seven = 7,
            Nine = 9,
            Eleven = 11,
            Thirteen = 13,
            Fifteen = 15,
            Seventeen = 17,
            Nineteen = 19,
            TwentyNine = 29,
            ThirtyNine = 39,
            FortyNine = 49,
        }

        
        public static HashSet<Address> GenerateShape(EntitySize entitySize, EntityShape shapeGenerator) {
            
            var shape = new HashSet<Address>();

            var size = (int) entitySize;
            var fhs = (size / 2f).FloorToInt(); //Floor half size.

            if (entitySize == EntitySize.One) {
                shape.Add(Address.Zero);
            }
            else {

                switch (shapeGenerator) {
                    case EntityShape.CenteredCircle: {
                        foreach (var address in AddressUtil.GetAddressesWithinCircle(Address.Zero, fhs, null)) {
                            shape.Add(address);
                        }
                    }
                        break;
                    case EntityShape.CenteredRectangle: {
                        foreach (var address in AddressUtil.Rectangle(new Address(-fhs, 0, -fhs),
                                                                      new Address(fhs, 0, fhs))) {
                            shape.Add(address);
                        }

                    }
                        break;
                    case EntityShape.CornerRectangle: {
                        foreach (var address in AddressUtil.Rectangle(Address.Zero, new Address(size, 0, size))) {
                            shape.Add(address);
                        }
                    }
                        break;
                }
            }

            return shape;
        }

        private void CreateBoard(int size, int players) {
            
            var boardParam = new BoardParam(size, 1, size);
            
            Board = new TileBoard(boardParam);
            Universe.Param = boardParam;
            Universe.Rand = new global::System.Random(8);
            
            var gods = Universe.Entities()
                               .Where(e => e.HasGodTag())
                               .ToList();

            //Create basic floor
            {
                foreach (var adr in Board.GetAllElements().Select(t=>t.Address)) {
                    if (adr.X % 9 == 0 && adr.Z % 9 == 0) {
                        Universe.AcquireEntity()
                                .SetNid(Universe.RidGenerator())
                                .SetPrefab("Floors/Floor_S0")
                                .SetTransform(adr.Pos())
                                .SetShape(GenerateShape(EntitySize.Nine, EntityShape.CornerRectangle))
                                .SetPropbits(Propbits.Walkable)
                                .SetOnBoardTag()
                                .SetGameObjectDisplay(DisplayTypes.Shown)
                                .DefineName("Floor_S0");

                    }
                }
            }
            
            //Produce areas (not linked to players yet)
            var mapCenter = new Address(size/2, 0, size/2);
            var areaEntities = new Queue<Entity>();
            {
                var angleIncrement = 360f / players;
                var radius = (size / 2f) * 0.75f;
                var pointer = new Vector3(radius, 0, 0);
                pointer = Quaternion.AngleAxis(Universe.Rand.Next(0,360), Vector3.up) * pointer;

                var points = new List<Address>();
                for (var i = 0; i < players; i++) {
                    var point = mapCenter + pointer.ToAddress();
                    points.Add(point);

                    pointer = Quaternion.AngleAxis(angleIncrement, Vector3.up) * pointer;
                }

                points = points.Shuffle(Universe.Rand).ToList();
                
                foreach (var adr in points) {
                    var e = Universe.AcquireEntity()
                                    .SetNid(Universe.NidGenerator())
                                    .SetTransform(adr.Pos())
                                    .SetShape(GenerateShape(EntitySize.Eleven, EntityShape.CenteredCircle))
                                    .SetPropbits(Propbits.Area)
                                    .SetOnBoardTag()
                                    .DefineName("Area");
                    
                    areaEntities.Enqueue(e);
                }
            }
            
            //Let's add the first building, a totem for each god.
            {
                foreach (var god in gods) {
                    //This is also where an area is linked to a god
                    var area = areaEntities.Dequeue();
                    area.LinkAreaToGod(god);
                    
                    var areaCenter = area.GetAddress();
                    var toMapCenter = (mapCenter.Pos() - areaCenter.Pos()).normalized;

                    var rot = Quaternion.FromToRotation(Vector3.forward, (Vector3.back + new Vector3(toMapCenter.x, 0,0)).normalized);
                    var pos = areaCenter.Pos();
                    
                    Universe.AcquireEntity()
                            .SetNid(Universe.NidGenerator())
                            .SetDisplayName("Totem")
                            .SetGClass(GClasses.Totem)
                            .SetHitPoints(110)
                            .SetPortrait("Media/Portraits/Totem_S0")
                            .SetPrefab("Buildings/Totens/Totem_S0")
                            .SetTransform(pos, rot)
                            .SetShape(GenerateShape(EntitySize.One, EntityShape.CenteredCircle))
                            .SetPropbits(Propbits.Blocks | Propbits.Building)
                            .SetOnBoardTag()
                            .LinkTotemToGod(god)
                            .LinkServantsToGod(god)
                            .SetSelectableTag()
                            .SetGameObjectDisplay(DisplayTypes.Shown)
                            .DefineName("Totem");
                }
            }
            
            //And a small population
            {
                
                foreach (var god in gods) {
                    var totem = god.GetLinkGodToTotem();
                    var area = god.GetLinkGodToArea();

                    var warriorCounter = 4;

                    for (var i = 0; i <= 6; i++) {
                        
                        

                        Address a;
                        do {
                            a = area.GetFootprint().Value.RandomlyPick(Universe.Rand);
                            if (!boardParam.IsValid(a)) {
                                continue;
                            }
                            var t = Board.GetElement(a);
                            if (t.Propbits.HasAny(Propbits.Actor | Propbits.Blocks)) {
                                continue;
                            }

                            break;

                        } while (true);

                        var toTotemDir = (totem.GetAddress().Pos() - a.Pos()).normalized;
                        var rot = Quaternion.FromToRotation(Vector3.forward, toTotemDir);
                        var pos = a.Pos();

                        var isWarrior = warriorCounter >= 0;
                        var name = isWarrior ? "Warrior" : "Worker";
                        
                        Universe.AcquireEntity()
                                .SetNid(Universe.NidGenerator())
                                .SetDisplayName(name)
                                .SetGClass(isWarrior ? GClasses.Warrior : GClasses.Worker)
                                .SetHitPoints(100)
                                .SetPortrait(isWarrior? "Media/Portraits/Actor_S0" : "Media/Portraits/Actor_S1")
                                .SetPrefab(isWarrior? "Actors/Actor_S0" : "Actors/Actor_S1")
                                .SetTransform(pos, rot)
                                .SetShape(GenerateShape(EntitySize.One, EntityShape.CenteredCircle))
                                .SetPropbits(Propbits.Actor | Propbits.Blocks)
                                .SetOnBoardTag()
                                .LinkServantsToGod(god)
                                .SetSelectableTag()
                                .SetActionPoints(100)
                                .SetActionPointsRefuel(100)
                                .SetGameObjectDisplay(DisplayTypes.Shown)
                                .DefineName(name);


                        warriorCounter--;
                    }
                }
            }

            
            //Populate floor with rocks
            {
                var scale = 0.1f;

                foreach (var adr in Board.GetAllElements().Select(t=>t.Address)) {
                    var area = Board.GetElement(adr).Propbits.HasAny(Propbits.Area);
                    if (area) {
                        continue;
                    }
                    
                    var h = Mathf.PerlinNoise(adr.X * scale, adr.Z * scale);
                    if (h <= 0.25f) {
                        if ((float)Universe.Rand.NextDouble() <= 0.45f) {

                            var pos = adr.Pos();
                            var rot = Quaternion.Euler(0, (float)Universe.Rand.NextDouble() * 360, 0);
                            var localScale = (Vector3.one*0.75f) + (Vector3.one * ((float)Universe.Rand.NextDouble()*1.0f));
                            
                            
                            Universe.AcquireEntity()
                                    .SetNid(Universe.NidGenerator())
                                    .SetPrefab("Rocks/Boulders/Boulder_S0")
                                    .SetTransform(pos, rot, localScale)
                                    .SetShape(GenerateShape(EntitySize.Three, EntityShape.CenteredCircle))
                                    .SetPropbits(Propbits.Blocks | Propbits.Resource)
                                    .SetOnBoardTag()
                                    .SetGameObjectDisplay(DisplayTypes.Shown)
                                    .DefineName("Boulder_S0");
                        }
                    }
                }
            }
            
            //Now with trees
            {
                var disp = 1000f;
                var scale = 0.1f;

                foreach (var adr in Board.GetAllElements().Select(t=>t.Address)) {
                    var blocked = Board.GetElement(adr).Propbits.HasAny(Propbits.Blocks);
                    if (blocked) {
                        continue;
                    }
                    
                    var area = Board.GetElement(adr).Propbits.HasAny(Propbits.Area);
                    if (area) {
                        continue;
                    }
                    
                    var h = Mathf.PerlinNoise(disp+adr.X * scale, disp+adr.Z * scale);
                    if (h <= 0.35f) {
                        if ((float)Universe.Rand.NextDouble() <= 0.35f) {
                            
                            var pos = adr.Pos();
                            var rot = Quaternion.Euler(0, (float)Universe.Rand.NextDouble() * 360, 0);
                            var localScale = (Vector3.one*0.75f) + (Vector3.one * ((float)Universe.Rand.NextDouble()*1.5f));
                            
                            Universe.AcquireEntity()
                                    .SetNid(Universe.NidGenerator())
                                    .SetPrefab("Plants/Trees/Tree_S0")
                                    .SetTransform(pos, rot, localScale)
                                    .SetShape(GenerateShape(EntitySize.Three, EntityShape.CenteredCircle))
                                    .SetPropbits(Propbits.Blocks | Propbits.Plant)
                                    .SetOnBoardTag()
                                    .SetGameObjectDisplay(DisplayTypes.Shown)
                                    .DefineName("Tree_S0");     
                
                        }
                    }
                }
            }
        }

        private void OnTransformUpdate(Entity entity) {

            var bEnt = entity;
            var skipUnlink = false;
            
            var address = entity.GetTransform().Position.ToAddress();
            
            //0. Create a footprint from the shape if the footprint is not set yet.
            if (!bEnt.HasFootprint()) {
                skipUnlink = true;
                _addresses.Clear();
                foreach (var sp in bEnt.GetShape()) {
                    _addresses.Add(address + sp.DeepCopy() );
                }

                bEnt.SetFootprint(_addresses);
            }
            
            //1. Update the Address of the Entity
            var entAddress = bEnt.HasAddress() ? bEnt.GetAddress() : Address.NegativeExtreme;
            if (address == entAddress) {
                //Nothing changed
                return;
            }
            bEnt.SetAddress(address);
            
            //2. Update the Footprint
            
            //2.1 Remove the entity from the table (update footprint)
            if (!skipUnlink) {
                foreach (var sp in bEnt.GetFootprint()) {
                    if(Universe.Param.IsValid(sp)){
                        Board.GetElement(sp).UnlinkEntity(entity);
                    }
                }
            }

            //2.2 Update the footprint
            _addresses.Clear();
            foreach (var sp in bEnt.GetShape()) {
                _addresses.Add(address + sp.DeepCopy() );
            }

            bEnt.SetFootprint(_addresses);
            
            //2.3 Try and "Re" add to the board.
            foreach (var sp in _addresses) {
                if(Universe.Param.IsValid(sp)){
                    Board.GetElement(sp).LinkEntity(entity);
                }
            }
        }
    }
}