using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Core.Util;
using Ce.Core.World;
using Ce.Game.Message.Command;
using Ce.Game.World;
using UnityEngine;

namespace Ce.Game.System {

    /// <summary>
    /// The system responsible for finding the path between two points in the board.
    /// Using the standard A* Algorithm 
    /// </summary>
    public class PathfinderSystem: BaseSystem {

        private NodeBoard _nodeBoard;
        private BoardParam _boardParam;
        
        private MessageSystem _messageSystem;
        private BoardSystem _boardSystem;
        private TileBoard _tileBoard;

        private readonly List<int> _open = new List<int>(512*512);
        private readonly List<int> _closed = new List<int>(512*512);
        
        private readonly List<PathNode> _helperList = new List<PathNode>(8);
        
        protected override void OnInitialize() {
            base.OnInitialize();

            _boardSystem = Universe.GetSystem<BoardSystem>();
            _messageSystem = Universe.GetSystem<MessageSystem>();
            _messageSystem.SubscribeToMessage<CreateBoardCommand>(this, HandleCommandCreateBoard);
        }

        protected override void OnTerminate() {
            _messageSystem.UnsubscribeFromMessage<CreateBoardCommand>(this);
            base.OnTerminate();
        }
        
        private void HandleCommandCreateBoard(CreateBoardCommand m) {
            CreateBoard(m.Size, m.Players);
        }

        private void CreateBoard(int size, int players) {
            _boardParam = new BoardParam(size, 1, size);
            _nodeBoard = new NodeBoard(_boardParam);
            foreach (var node in _nodeBoard.GetAllElements()) {
                node.AddressId = _boardParam.ToId(node.Address);
            }
        }

        public List<Waypoint> FindPath(Entity actor, Address begin, Address end,
                                           List<Waypoint> result = null) {
            if (result == null) {
                result = new List<Waypoint>(1024);
            }

            if (_tileBoard == null) {
                _tileBoard = _boardSystem.Board;
            }
            
            _open.Clear();
            _closed.Clear();
            
            _nodeBoard.GetElement(begin).Total = 0;

            var beginId = _boardParam.ToId(begin);
            var finalId = _boardParam.ToId(end);

            //Clean up the first node
            {
                var node = _nodeBoard.GetElement(beginId);
                node.Total = node.IndividualCost = node.FromStartDistance = 0;
                node.ToEndDistance = Vector2.Distance(begin.Pos(), end.Pos());
                node.WayBack = default;
            }


            _open.Add(beginId);
            var found = false;

            while (_open.Any()) {
                var current = PickNodeWithLowestCost(_open);
                
                if (current.AddressId == finalId) {
                    found = true;
                    break;
                }
                
                _open.Remove(current.AddressId);
                _closed.Add(current.AddressId);

                GetNeighbours(current, _helperList);
                
                foreach (var nb in _helperList) {
                    var fromStartDistance = current.FromStartDistance + nb.IndividualCost;
                    var toEndDistance = Vector2.Distance(nb.Address.Pos(), end.Pos());
                    var total = fromStartDistance + toEndDistance;
                    if (!_open.Contains(nb.AddressId)) {
                        nb.Total = total;
                        nb.FromStartDistance = fromStartDistance;
                        nb.ToEndDistance = toEndDistance;
                        nb.WayBack = current;
                        _open.Add(nb.AddressId);
                    }
                    else {
                        //If open list contains already I will update the distance if the new total distance is shorter.
                        if (total < nb.Total) {
                            nb.Total = total;
                            nb.FromStartDistance = fromStartDistance;
                            nb.ToEndDistance = toEndDistance;
                            nb.WayBack = current;
                        }
                    }
                }
            }

            if (found) {
                var terminate = false;
                var current = _nodeBoard.GetElement(end);
                //Produce the path
                while(true)
                {
                    result.Add(new Waypoint {
                        IndividualDistance = current.IndividualCost,
                        TotalDistance = current.FromStartDistance,
                        AddressId = current.AddressId
                    });

                    if (terminate) {
                        break;
                    }

                    var next = current.WayBack;
                    if (next.AddressId == beginId) {
                        terminate = true;
                    }

                    current = next;
                }

                result.Reverse();
            }

            return result;
        }

        /// <summary>
        /// Find all valid neighbours that are walkable and not blocked.
        /// And are not in the CLOSED list
        /// </summary>
        private void GetNeighbours(PathNode current, List<PathNode> neighbours) {
            neighbours.Clear();
            var a = current.Address;
            for (var i = 0; i < 8; i++) {
                var dir = ((Directions) i);
                var nbAdd = a + dir.DirToAdd();
                
                if (_boardParam.IsValid(nbAdd)) {
                    var tile = _tileBoard.GetElement(nbAdd);
                    var propbits = tile.Propbits;
                    
                    if (propbits.HasAll(Propbits.Walkable)) {
                        if (propbits.HasNone(Propbits.Blocks)) {
                
                            var nb = _nodeBoard.GetElement(nbAdd);
                            if (!_closed.Contains(nb.AddressId)) {
                                nb.IndividualCost = GetCost(dir);
                                neighbours.Add(nb);
                            }
                        }   
                    }
                }
            }
        }

        public float GetCost(Directions dir) {
            switch (dir) {
                case Directions.Forward:       return 1.0f;
                case Directions.ForwardRight:  return 1.4f;
                case Directions.Right:         return 1.0f;
                case Directions.BackwardRight: return 1.4f;
                case Directions.Backward:      return 1.0f;
                case Directions.BackwardLeft:  return 1.4f;
                case Directions.Left:          return 1.0f;
                case Directions.ForwardLeft:   return 1.4f;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        
        }

        private PathNode PickNodeWithLowestCost(List<int> open) {
            var lowestSoFar = float.MaxValue;
            var nodeId = open[0];
            
            foreach (var id in open) {
                var node = _nodeBoard.GetElement(id);
                
                if (node.Total < lowestSoFar) {
                    lowestSoFar = node.Total;
                    nodeId = node.AddressId;
                }
            }

            var chosenId = nodeId;
            var chosenNode = _nodeBoard.GetElement(chosenId);

            return chosenNode;
        }
    }
    
    public class NodeBoard : Board<PathNode> {
        public NodeBoard(BoardParam param) : base(param) { }
    }
    
    
    public class PathNode: Addressable {

        public int AddressId;

        public float IndividualCost;
        
        /// <summary>
        /// How long we traveled since we started
        /// This might change until added to the close list.
        /// </summary>
        public float FromStartDistance;
        
        /// <summary>
        /// Guess how long until the end
        /// </summary>
        public float ToEndDistance;

        /// <summary>
        /// The sum of Star to End;
        /// </summary>
        public float Total;

        /// <summary>
        /// The node before us we this got added to the close list.
        /// </summary>
        public PathNode WayBack;
    }

    public struct Waypoint : IDeepCopy, IXmlConversion{
        
        
        public int AddressId;
        public float TotalDistance;
        public float IndividualDistance;

        public XElement ToXml(string elementName)
        {
            return new XElement(elementName,
                                new XAttribute("AddressId", AddressId.ToString("0", CultureInfo.InvariantCulture)),
                                new XAttribute("TotalDistance", TotalDistance.ToString("0", CultureInfo.InvariantCulture)),
                                new XAttribute("IndividualDistance", IndividualDistance.ToString("0", CultureInfo.InvariantCulture))
                               );
        }

        public void FromXml(XElement element)
        {
            var xAttribute = element.Attribute("AddressId");
            var yAttribute = element.Attribute("TotalDistance");
            var zAttribute = element.Attribute("IndividualDistance");

            AddressId = int.Parse(xAttribute.Value, CultureInfo.InvariantCulture);
            TotalDistance = float.Parse(yAttribute.Value, CultureInfo.InvariantCulture);
            IndividualDistance = float.Parse(zAttribute.Value, CultureInfo.InvariantCulture);
        }

        public object DeepCopy() => new Waypoint {
            AddressId = AddressId,
            IndividualDistance = IndividualDistance,
            TotalDistance = TotalDistance
        };
    }

}