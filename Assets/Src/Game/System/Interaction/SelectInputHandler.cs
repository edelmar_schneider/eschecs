using Ce.Core.Ecs;
using Ce.Core.World;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Extensions;
using UnityEngine;
using UnityEngine.Assertions;

namespace Ce.Game.System.Interaction {

    public class SelectInputHandler  : NullInputHandler {

        public SelectInputHandler(Universe universe) : base(universe) {}
        
        public override void ClickOnEntity(ButtonType btt, Entity e, Vector3 clickPos, Vector3 surfaceNormal, Address clickAdr) {
            if (btt == ButtonType.Left) {
                if (e.IsSelectable()) {
                    if (e.IsLocalEntity()) {
                        if (e.IsActor()) {
                            if (e.IsLocallySelected()) {
                                Assert.IsTrue(false, "We shouldn't be in Select mode and have anyone selected");
                            }
                            else {
                                var lg = Universe.GetLocalGod();
                                lg.LinkGodToSelected(e);
                                Universe.GetGameDefinitionsEntity().SetInteractionMode(IntModes.ActorSelected);
                                return;
                            }
                        }
                        else if(e.IsBuilding()){
                            if (e.IsLocallySelected()) {
                                Assert.IsTrue(false, "We shouldn't be in Select mode and have anyone selected");
                            }
                            else {
                                var lg = Universe.GetLocalGod();
                                lg.LinkGodToSelected(e);
                                Universe.GetGameDefinitionsEntity().SetInteractionMode(IntModes.StructureSelected);
                                return;
                            }
                        }
                    }
                }
            }
            else if (btt == ButtonType.Right) {
                
            } 
                
        }
    }

}