using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Core.Util;
using Ce.Core.World;
using Ce.Game.Components.Enum;
using Ce.Game.Extensions;
using Ce.Game.Message.Command;
using UnityEngine;

namespace Ce.Game.System.Interaction {

    public enum ButtonType {
        Left,
        Right
    }
    
    /// <summary>
    /// In-game clicks on the ground or on entities.
    /// </summary>
    public class InteractionSystem: BaseSystem {
        
        private MessageSystem _messageSystem;

        /// <summary>
        /// Means we are not in the initial menu. The game has started and it's running
        /// </summary>
        private bool _interactionActive;
        
        private IInputHandler _inputHandler;
        
        private StateMachine<IntModes, IntModes> _stateMachine;

        protected override void OnInitialize() {
            base.OnInitialize();
            
            _inputHandler = new NullInputHandler(Universe);
            
            SetupStateMachine();

            _messageSystem = Universe.GetSystem<MessageSystem>();
            _messageSystem.SubscribeToMessage<BeginGroundCombatCommand>(this, HandleBeginGroundCombatCommand);

            Universe.GetGameDefinitionsGroup()
                    .OnComponentAdded<InteractionModeComponent>((group, entity, arg4) => InteractionModeChanged(arg4))
                    .OnComponentUpdated<InteractionModeComponent>((group, entity, arg3, arg4) => InteractionModeChanged(arg4));
        }

        protected override void OnTerminate() {
            _interactionActive = false;
            _messageSystem.UnsubscribeFromMessage<BeginGroundCombatCommand>(this);
            _inputHandler = new NullInputHandler(Universe);
            
            base.OnTerminate();
        }
        
        private void InteractionModeChanged(InteractionModeComponent c) {
            Trigger(c.Value);
        }
        
        public void Trigger(IntModes trigger) {
            _stateMachine.Trigger(trigger);
        }


        private void HandleBeginGroundCombatCommand(BeginGroundCombatCommand c) {
            _interactionActive = true;
        }
        
        public void DragOnGroundStarted(Address initial, Address current) {
            Debug.Log("Drag event started!");
        }
        
        public void DragOnGroundOngoing(Address initial, Address current) {
            Debug.Log("Drag event happening!");
        }

        public void DragOnGroundTerminated(Address initial, Address final) {
            Debug.Log("Drag event happened!");
        }
        
        public void MouseOverEntity( Entity e, Vector3 clickPos, Vector3 surfaceNormal, Address clickAdr) {
            _inputHandler.MouseOverEntity( e, clickPos, surfaceNormal, clickAdr);    
        }
        
        public void ClickOnEntity(ButtonType btt, Entity e, Vector3 clickPos, Vector3 surfaceNormal, Address clickAdr) {
            _inputHandler.ClickOnEntity(btt, e, clickPos,  surfaceNormal, clickAdr);    
        }
        
        public void ReadKeyboardInput() {
            _inputHandler.ReadKeyboardInput(Universe);
        }
        
        private void SetupStateMachine() {
            _stateMachine = new StateMachine<IntModes, IntModes>(IntModes.Select);
            _stateMachine.Configure()
                         .State(IntModes.Select)
                         .Trigger(IntModes.Select, IntModes.Select)
                         .Trigger(IntModes.StructureSelected, IntModes.StructureSelected)
                         .Trigger(IntModes.ActorSelected, IntModes.ActorSelected)
                         .Trigger(IntModes.WaitingTurnToPass, IntModes.WaitingTurnToPass)
                         .Trigger(IntModes.EndGame, IntModes.EndGame)
                         .OnEnter((om, nm) => _inputHandler = new SelectInputHandler(Universe))
                         .Return()
                         .State(IntModes.StructureSelected)
                         .Trigger(IntModes.Select, IntModes.Select)
                         .Trigger(IntModes.StructureSelected, IntModes.StructureSelected)
                         .Trigger(IntModes.ActorSelected, IntModes.ActorSelected)
                         .Trigger(IntModes.WaitingTurnToPass, IntModes.WaitingTurnToPass)
                         .Trigger(IntModes.EndGame, IntModes.EndGame)
                         .OnEnter((om, nm) => _inputHandler = new SelectedInputHandler(Universe))
                         .Return()
                         .State(IntModes.ActorSelected)
                         .Trigger(IntModes.Select, IntModes.Select)
                         .Trigger(IntModes.StructureSelected, IntModes.StructureSelected)
                         .Trigger(IntModes.ActorSelected, IntModes.ActorSelected)
                         .Trigger(IntModes.WaitingTurnToPass, IntModes.WaitingTurnToPass)
                         .Trigger(IntModes.EndGame, IntModes.EndGame)
                         .OnEnter((om, nm) => _inputHandler = new SelectedInputHandler(Universe))
                         .Return()
                         .State(IntModes.WaitingTurnToPass)
                         .Trigger(IntModes.Select, IntModes.Select)
                         .Trigger(IntModes.StructureSelected, IntModes.StructureSelected)
                         .Trigger(IntModes.ActorSelected, IntModes.ActorSelected)
                         .Trigger(IntModes.WaitingTurnToPass, IntModes.WaitingTurnToPass)
                         .Trigger(IntModes.EndGame, IntModes.EndGame)
                         .OnEnter((om, nm) => _inputHandler = new NullInputHandler(Universe))
                         .Return()
                         .State(IntModes.EndGame)
                         .Trigger(IntModes.Select, IntModes.Select)
                         .Trigger(IntModes.StructureSelected, IntModes.StructureSelected)
                         .Trigger(IntModes.ActorSelected, IntModes.ActorSelected)
                         .Trigger(IntModes.WaitingTurnToPass, IntModes.WaitingTurnToPass)
                         .Trigger(IntModes.EndGame, IntModes.EndGame)
                         .OnEnter((om, nm) => _inputHandler = new NullInputHandler(Universe))
                         .Return();
        }

    }

}