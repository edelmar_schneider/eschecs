using Ce.Core.Ecs;
using Ce.Core.World;
using UnityEngine;

namespace Ce.Game.System.Interaction {

    public class NullInputHandler : IInputHandler {

        protected readonly Universe Universe;

        public NullInputHandler(Universe universe) => Universe = universe;

        public virtual void MouseOverEntity(Entity e, Vector3 clickPos, Vector3 surfaceNormal, Address clickAdr) {
            
        }

        public virtual void ClickOnEntity(ButtonType btt, Entity e, Vector3 clickPos, Vector3 surfaceNormal, Address clickAdr) {
            
        }

        public virtual void ReadKeyboardInput(Universe universe) {
         
        }
    }

}