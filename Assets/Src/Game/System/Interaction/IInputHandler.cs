using Ce.Core.Ecs;
using Ce.Core.World;
using UnityEngine;

namespace Ce.Game.System.Interaction {

    public interface IInputHandler {
        void MouseOverEntity(Entity e, Vector3 clickPos, Vector3 surfaceNormal, Address clickAdr);
        void ClickOnEntity(ButtonType btt, Entity e, Vector3 clickPos, Vector3 surfaceNormal, Address clickAdr);
        void ReadKeyboardInput(Universe universe);
    }

}