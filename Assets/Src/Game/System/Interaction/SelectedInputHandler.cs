using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Extensions;
using Ce.Core.World;
using Ce.Game.Components.Collection;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.Value;
using Ce.Game.Extensions;
using Ce.Game.Message.Request;
using Ce.Game.Scripts;
using UnityEngine;

namespace Ce.Game.System.Interaction {

    /// <summary>
    /// If we are in this operation mode we have an selected entity that can be an actor or a building.
    /// </summary>
    public class SelectedInputHandler  : NullInputHandler {

        public SelectedInputHandler(Universe universe) : base(universe) {}
        
        public override void ClickOnEntity(ButtonType btt, Entity e, Vector3 clickPos, Vector3 surfaceNormal, Address clickAdr) {
            
            //Fir of all let's grab the selected entity
            var ls = Universe.GetLocallySelected();
            
            if (btt == ButtonType.Left) { //Left button

                if (Attack(ls, e, clickAdr)) {
                    return;
                }
                
                if (e.IsWalkable()) { //Clicked on walkable
                    
                    if (ls.IsActor()) { //If the selected is an actor and not a building
                        if (ls.HasPath()) { //If has path let's see if we click on one of the addresses of the path to move there
                            
                            if (IsAddressInPath(clickAdr, ls.GetPath().Value)) { //If I click on the address I want to move there
                                
                                //I don't need the path anymore because it will be generated on every client again
                                ls.RemovePath();
                                
                                //This has to be a command because we want to see this happening in all clients
                                Universe.Send().Request(new MoveToActorRequest(ls.GetNid()
                                                                               , ls.GetAddress().AdrToId(Universe)
                                                                               , clickAdr.AdrToId(Universe)));

                                return;
                            }
                            //Otherwise I want to create a new path
                        }
                        //Remove any existent path
                        if (ls.HasPath()) {
                            ls.RemovePath();
                        }

                        //Create a new path
                        var pf = Universe.GetSystem<PathfinderSystem>();
                        var beginAdd = ls.GetAddress();
                        var endAdd = clickAdr;
                        var path = pf.FindPath(ls, beginAdd, endAdd);
                        var ap = ls.GetActionPoints();
                        var maxDistance = ap * GameConstants.GameParameters.DistancePerAp;
                        var filteredPath = path.TakeWhile(w => w.TotalDistance <= maxDistance).ToHashSet();
                        //Path has to be longer than 1
                        if (filteredPath.Count > 1) {
                            ls.SetPath(filteredPath);
                        }
                        
                        return;
                    } 
                    
                }else if (e.IsSelectable()) { //Clicked on selectable
                    if (e.IsLocalEntity()) {//Is local entity
                        if (e.IsActor()) { //Is an actor
                            if (e.IsLocallySelected()) { //Move the camera to an entity that was alredy selected
                                //TODO make the camera move to actor.
                            }
                            else { //Select the entity if it wasn't selected
                                var lg = Universe.GetLocalGod();
                                lg.LinkGodToSelected(e);
                                return;
                            }
                        }
                        else if(e.IsBuilding()){ //Clicked on building
                            if (e.IsLocallySelected()) { //Move the camera to an entity that was alredy selected
                                //TODO make the camera move to building.
                            }
                            else { //Select the entity if it wasn't selected
                                var lg = Universe.GetLocalGod();
                                lg.LinkGodToSelected(e);
                                return;
                            }
                        }
                    }
                }
            }
            else if (btt == ButtonType.Right) {
                //Remove the path if the entity has it
                if (ls.HasPath()) {
                    ls.RemovePath();
                    return;
                }
                else { //Or deselect the entity and return to Select mode
                    ls.RemoveLinkSelectedToGod();
                    Universe.GetGameDefinitionsEntity().SetInteractionMode(IntModes.Select);
                }
            } 
        }

        /// <summary>
        /// If you have a local actor selected and click on an enemy actor or building that is next to you it will generate
        /// an attack
        /// </summary>
        private bool Attack(Entity ls, Entity te, Address clickAdr) {
            if (!(te.IsActor() || te.IsBuilding())) {
                //Target Entity is not actor and not building, so it can not be attacked
                return false;
            }

            if (ls.GetGClass() != GClasses.Warrior) {
                //Only warriors can attack
                return false;
            }

            var localGod = ls.GetLinkServantsToGod();
            var targetGod = te.GetLinkServantsToGod();

            if (localGod.Rid == targetGod.Rid) {
                //Can't attack because both entities serve the same god.
                return false;
            }

            var lsAdr = ls.GetAddress();
            var teAdr = clickAdr;
            var dif = teAdr - lsAdr;

            var tooFar = dif.X.Abs() > 1 || dif.Z.Abs() > 1;
            if (tooFar) {
                //Distance is larger than 1
                ShowMessage(te.GetAddress().Pos() + Vector3.up, "Too far to attack");
                return false;
            }

            var isAlive = te.GetHitPoints() > 0;
            if (!isAlive) {
                return false;
            }

            var haveAp = ls.GetActionPoints() >= GameConstants.GameParameters.ApPerAttack;
            if (!haveAp) {
                //I don't have action points enough to do the attack
                ShowMessage(te.GetAddress().Pos() + Vector3.up, "No Action Points");
                return false;
            }
            
            //If I reached this point I can attack
            //Each attack will remove a random amount of hp 
            var damage = Universe.Rand.Next(30, 50);
            
            Universe.Send().Request(new AttackRequest(ls.GetNid(), te.GetNid(), damage));

            return true;
        }

        private bool IsAddressInPath(Address adr, HashSet<Waypoint> path) {
            var id = Universe.Param.ToId(adr);
            return path.Any(w => w.AddressId == id);
        }

        private void ShowMessage(Vector3 pos, string message) {
            var prefab = Resources.Load<GameObject>("Ui/DamageReport");
            var sp = Camera.main.WorldToScreenPoint(pos);
            var dre = Object.Instantiate(prefab, Vector3.zero, Quaternion.identity)
                            .GetComponent<DamageReportScript>();
            dre.SetMessage(message);
            dre.speed = 0.2f;
            dre.body.transform.position = sp;
        } 
    }

}