using Ce.Core.Ecs;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Enum;
using Ce.Game.Components.String;
using Ce.Game.Models;
using Ce.Game.Scripts;
using UnityEngine;
using UnityEngine.Assertions;

namespace Ce.Game.System {

    /// <summary>
    /// Responsible for showing the gameobject of an entity
    /// </summary>
    public class GameObjectSystem : BaseSystem{

        protected override void OnInitialize() {
            base.OnInitialize();
            var gem = Universe.GetModels<GameEntityModels>();

            RegisterGroup(gem.Entity3dPrefab, "Entity3dPrefab on GameObjectSystem")
                .OnEntityAdded(NewGameObject)
                .OnEntityRemoved(RemoveGameObject)
                .OnComponentUpdated<PrefabComponent>(PrefabUpdated)
                .OnComponentUpdated<GameObjectDisplayComponent>(DisplayUpdated);

            RegisterGroup(gem.Entity3dObject, "Entity3dObject on GameObjectSystem")
                .OnComponentUpdated<TransformComponent>(TransformUpdated);
        }

        private void TransformUpdated(Group g, Entity e, TransformComponent oc, TransformComponent nc) {

            var go = e.GetGameObject();
            var t = e.GetTransform();
            var gt = go.transform;
            
            gt.position = t.Position;
            gt.rotation = t.Rotation;
            gt.localScale= t.Scale;
        }

        private void PrefabUpdated(Group g, Entity e, PrefabComponent oc, PrefabComponent nc) {
            if (e.HasGameObject()) {
                e.RemoveGameObject();
            }
            
            NewGameObject(e);
        }
        
        private void DisplayUpdated(Group g, Entity e, GameObjectDisplayComponent oc, GameObjectDisplayComponent nc) {
            
            var gdp = e.GetGameObjectDisplay();

            if (gdp == DisplayTypes.None) {
                if (e.HasGameObject()) {
                    e.RemoveGameObject();
                }

                return;
            }

            if (!e.HasGameObject()) {
                NewGameObject(e);
                return;
            }
            
            var go = e.GetGameObject();
            
            
            if (gdp == DisplayTypes.Hidden) {
                go.SetActive(false);
                return;
            }
            
            if (gdp == DisplayTypes.Shown) {
                go.SetActive(true);
                return;
            }
        }

        private void NewGameObject(Entity e) {
            
            var gdp = e.GetGameObjectDisplay();
            var pr = e.GetPrefab();
            var t = e.GetTransform();

            //Do nothing
            if (gdp == DisplayTypes.None) {
                return;
            }
            
            var prefab = Resources.Load<GameObject>(pr);
            Assert.IsNotNull(prefab);

            var go = Object.Instantiate<GameObject>(prefab, t.Position, t.Rotation);
            go.transform.localScale = t.Scale;
            go.AddComponent<EntityController>().entity = e;
            
            if (gdp == DisplayTypes.Hidden) {
                go.SetActive(false);
            }
            
            if (gdp == DisplayTypes.Shown) {
                go.SetActive(true);
            }

            e.SetGameObject(go);
        }

        private void RemoveGameObject(Entity e) {
            if (!e.Active) return;

            if (e.HasGameObject()) {
                e.RemoveGameObject();
            }
        }
        

        protected override void OnTerminate() {
            base.OnTerminate();
        }

    }

}