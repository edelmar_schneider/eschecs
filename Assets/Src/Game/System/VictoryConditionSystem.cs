using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Extensions;
using Ce.Game.Message.Command;
using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game.System {

    public class VictoryConditionSystem : BaseSystem{
        
        private Group _gods;

        private MessageSystem _messageSystem;

        private bool _loadingOperation;

        protected override void OnInitialize() {
            base.OnInitialize();
        
            _gods = Universe.GetGroupByName(GCG.Gods)
                            .OnComponentRemove<GodToTotemComponent>((group, god, arg3) => OnTotemChanged(god));
            
            _messageSystem = Universe.GetSystem<MessageSystem>();
            _messageSystem.SubscribeToMessage<FinishGameCommand>(this, HandleFinishGameCommand);
            
        }
        
        public void LoadingOperation(bool loading) {
            _loadingOperation = loading;
        }

        private void HandleFinishGameCommand(FinishGameCommand c) {
            
            if (_loadingOperation) {
                return;
            }
            
            var gde = Universe.GetGameDefinitionsEntity();
            gde.SetInteractionMode(IntModes.EndGame);
        
            //Set the winners locally.
            foreach (var god in _gods) {
                if (c.ThereIsWinner) {
                    if (c.WinnerGodRid == god.GetNid()) {
                        god.SetVictoryStatus(VictoryStatuses.Won);
                        continue;
                    }
                }
                
                god.SetVictoryStatus(VictoryStatuses.Lost);
            }
        }

        /// <summary>
        /// If the totem changed we need to check if one of the gods won the game
        /// </summary>
        private void OnTotemChanged(Entity god) {
            if (_loadingOperation) {
                return;
            }
            
            if (!Universe.IsServer()) {
                return;
            }

            if (!god.Active) {
                return;
            }
            
            var godsWithTotems = _gods.Where(g => g.HasLinkGodToTotem()).ToArray();
            
            if (godsWithTotems.Length == 1) {
                //We have a winner
                Universe.Send().Command(new FinishGameCommand(godsWithTotems.First().GetNid()));
            }
            
            if (godsWithTotems.Length == 0) {
                //Everybody loose, there is no winner
                Universe.Send().Command(new FinishGameCommand());
            }
        }

    }

}