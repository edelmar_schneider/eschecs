using System;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.String;
using Ce.Game.Extensions;
using Ce.Game.Message.Command;
using Ce.Game.Scripts;
using UnityEngine;
using GCG = Ce.Game.GameConstants.NamedGroups;
using Object = UnityEngine.Object;

namespace Ce.Game.System {

    public class CombatSystem : BaseSystem {

        private MessageSystem _messageSystem;

        private Group _hpGroup;
        
        protected override void OnInitialize() {
            
            base.OnInitialize();
            _messageSystem = Universe.GetSystem<MessageSystem>();
            _messageSystem.SubscribeToMessage<AttackCommand>(this, HandleAttackCommand);

            _hpGroup = Universe.GetGroupByName(GCG.HitPointEntities)
                               .OnEntityAdded(EvaluateKill)
                               .OnComponentUpdated<HitPointsComponent>((g, e, oldC, newC) => { EvaluateKill(e);});
        }

        /// <summary>
        /// Check if an entity has died.
        /// </summary>
        private void EvaluateKill(Entity e) {
            if (e.GetHitPoints() > 0) {
                return;
            }

            if (e.HasLinkSelectedToGod()) {
                e.RemoveLinkSelectedToGod();
            }

            if (e.HasLinkServantsToGod()) {
                e.RemoveLinkServantsToGod();
            }
            
            if (e.HasLinkTotemToGod()) {
                //This will mean that the god got defeated
                e.RemoveLinkTotemToGod();
            }

            if (e.IsBuilding()) {
                e.SetPrefab("Kill/Rubble_S0");
            }else if (e.IsActor()) {
                e.SetPrefab("Kill/Tombstone_S0");
            }
        }

        private void HandleAttackCommand(AttackCommand c) {
            
            var ae = FindEntity(c.ActorRid);
            var de = FindEntity(c.DefenderRid);
            var damage = c.Damage;
            Debug.Log(de);
            
            //Remove AP from attacker
            var currAp = ae.GetActionPoints();
            var finalAp = currAp - GameConstants.GameParameters.ApPerAttack;
            finalAp = Math.Max(0, finalAp);
            ae.SetActionPoints(finalAp);

            //Cause damage to target
            var currhp = de.GetHitPoints();
            var finalHp = currhp - damage;
            finalHp = Math.Max(0, finalHp); //Prevent Hp < 0;
            de.SetHitPoints(finalHp);

            //Animations
            foreach (var child in ae.GetGameObject().GetComponentsInChildren<Animator>()) {
                child.SetTrigger("Fire");
            }
            
            var prefab = Resources.Load<GameObject>("Ui/DamageReport");
            var sp = Camera.main.WorldToScreenPoint(de.GetTransform().Position + Vector3.up);
            var dre = Object.Instantiate(prefab, Vector3.zero, Quaternion.identity)
                            .GetComponent<DamageReportScript>();
            dre.SetDamage(-damage);
            dre.body.transform.position = sp;

            if (finalHp >= 0) {
                foreach (var child in de.GetGameObject().GetComponentsInChildren<Animator>()) {
                    child.SetTrigger("Hit");
                }
            }
        }

        private Entity FindEntity(ulong actorRid) {
            
            //return Universe.GetEntities().First(e=>e.HasRid() && e.GetRid() == actorRid);
            return _hpGroup.First(e=>e.GetNid() == actorRid);
        }

        protected override void OnTerminate() {
            _messageSystem.UnsubscribeFromMessage<AttackCommand>(this);
            
            base.OnTerminate();
        }
    }
}