using System.Linq;
using Ce.Core.Ecs;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Data;
using Ce.Game.Components.Links;
using Ce.Game.Extensions;
using UnityEngine;
using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game.System {

    /// <summary>
    /// Responsible for showing actors and buildings that are locally selected
    /// </summary>
    public class SelectionDisplaySystem: BaseSystem {

        private GameObject _actorSelectionPrefab;
        private GameObject _buildingSelectionPrefab;

        private Entity _selectedEntity;

        private GameObject _selectionGo;

        private Group _selectedGroup;

        protected override void OnInitialize() {
            base.OnInitialize();

            _actorSelectionPrefab = Resources.Load<GameObject>("Ui/ActorSelection");
            _buildingSelectionPrefab = Resources.Load<GameObject>("Ui/BuildingSelection");

            _selectedGroup = Universe.GetGroupByName(GCG.SelectedEntities)
                    .OnEntityAdded(OnSelectedActorUpdate)
                    .OnEntityRemoved(OnSelectedActorUpdate)
                    .OnComponentUpdated<SelectedToGodComponent>((group, entity, arg3, arg4) => OnSelectedActorUpdate())
                    .OnComponentAdded<ActionComponent>((group, entity, arg3) => OnSelectedActorUpdate())
                    .OnComponentUpdated<ActionComponent>((group, entity, arg3, arg4) => OnSelectedActorUpdate())
                    .OnComponentRemove<ActionComponent>((group, entity, arg3) => OnSelectedActorUpdate());
        }

        private void OnSelectedActorUpdate() {
            
            //Remove current selection reference if it exists.
            if (_selectionGo != null) {
                Object.Destroy(_selectionGo);
                _selectionGo = null;
            }
            
            //Let's produce the selection reference if there is a new locally selected entity (Actor or Building)
            //We don't want to show the selection when the actor is performing any action.
            _selectedEntity = _selectedGroup.FirstOrDefault(e=>e.IsLocalEntity() && !e.HasAction() && e.Active);
            if (_selectedEntity == null) {
                return;
            }

            if (_selectedEntity.IsActor()) {
                _selectionGo = Object.Instantiate(_actorSelectionPrefab);
            }else if (_selectedEntity.IsBuilding()) {
                _selectionGo = Object.Instantiate(_buildingSelectionPrefab);
            }

            if (_selectionGo != null) {
                _selectionGo.transform.position = _selectedEntity.GetTransform().Position;
            }
        }
        

    }

}