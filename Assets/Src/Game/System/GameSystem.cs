using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Message;
using Ce.Core.Message.Request;
using Ce.Core.System;
using Ce.Game.Components;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.String;
using Ce.Game.Components.Tags;
using Ce.Game.Extensions;
using Ce.Game.Message.Command;
using Ce.Game.Models;

namespace Ce.Game.System {

    public class GameSystem: BaseSystem {

        private MessageSystem _messageSystem;

        protected override void OnInitialize() {
            base.OnInitialize();

            _messageSystem = Universe.GetSystem<MessageSystem>();
            _messageSystem.SubscribeToMessage<CreatePlayerCommand>(this, HandleCommandCreatePlayer );
            _messageSystem.SubscribeToMessage<MoveToGroundCommand>(this, HandleCommandMoveToGround );
            
            _messageSystem.SubscribeToAll(this, HandleRequest);
        }

        /// <summary>
        /// Only the server side will process requests and issue commands.
        /// </summary>
        private void HandleRequest(IMessage m) {
            if (!Universe.IsServer()) {
                return;
            }

            if (m is IRequest r) {
                
                //This is where the request is turned into a command and sent out to all players.
                if (CheckRequestSecurity(r)) {
                    if (r.CanMakeCommand()) {
                        var command = r.MakeCommand();
                        if (command is ISetRidAtServerSide setRid) {
                            setRid.SetRid(Universe.RidGenerator());
                        }
                        Universe.Send().Command(command);
                    }
                }
            }
        }

        /// <summary>
        /// This is where I check if the request is valid and it was sent by a valid player in his turn.
        /// </summary>
        public bool CheckRequestSecurity(IRequest r) {
            //TODO This function should be implement if the game is really multi-player. I won't implement now because my multi-player game is only for debugging.  
            return true;
        }

        protected override void OnTerminate() {
            base.OnTerminate();
            _messageSystem.UnsubscribeFromMessage<CreatePlayerCommand>(this);
            _messageSystem.UnsubscribeFromMessage<MoveToGroundCommand>(this);
        }
        
        
        private void HandleCommandCreatePlayer(CreatePlayerCommand c) {

            var tp = c.IsAi 
                ? PlayerTypes.Ai.ToString() 
                : PlayerTypes.Human.ToString();
            
            //Let's create the player
            var p = Universe.AcquireEntity()
                            .DefineName($"Player_{tp}");
            
            if (!c.IsAi) {
                p.SetNetworkClientId(c.NetworkClientId);
            }

            p.SetNid(Universe.NidGenerator())
             .SetPlayer(c.IsAi ? PlayerTypes.Ai : PlayerTypes.Human)
             .SetDisplayName(c.Name);
             
            
            //And the god that the player controls
            var g = Universe.AcquireEntity()
                            .DefineName($"God_{tp}")
                            .SetNid(Universe.NidGenerator())
                            .SetDisplayName(c.God)
                            .SetGodTag()
                            .LinkGodToPlayer(p);

        }

        private void HandleCommandMoveToGround(MoveToGroundCommand c) {
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            
            //This signals to the game that the game must move to ground combat.
            gDef.SetInteractionMode(IntModes.Select)
                .SetGroundCombatTag();
        }
    }

}