using System.Collections.Generic;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Core.World;
using Ce.Game.Extensions;
using Ce.Game.Message.Request;
using Ce.Game.Scripts;
using Ce.Game.System.Interaction;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Ce.Game.System {

    public enum MouseButton {
        Left = 0,
        Right = 1
    }

    /// <summary>
    /// This module will identify that something got clicked, for example - the player clicked on a tree -
    /// but it won't decide if the player is trying to select it or order someone to pick up or chop the tree.
    /// The interpretation of the meaning is delegated to another module and it's based on the context of
    /// the screen and the game state.
    /// </summary>
    public class InputSystem: BaseSystem {

        private InteractionSystem _ugiSystem;
        private BoardSystem _boardSystem;
        private TimeSystem _timeSystem;

        
        private Address _mouseDownAddress;
        private Address _mouseUpAddress;
        private bool _dragIsHappening;


        private Camera _cam;
        private Camera Camera => _cam == null ? _cam = Camera.main : _cam;

        protected override void OnInitialize() {
            //TODO: Pick the current camera
            
            base.OnInitialize();
            _ugiSystem = Universe.GetSystem<InteractionSystem>();
            _boardSystem = Universe.GetSystem<BoardSystem>();
            _timeSystem = Universe.GetSystem<TimeSystem>();
            
            _timeSystem.UpdateEvent += TimeSystemOnUpdateEvent;
        }

        private void TimeSystemOnUpdateEvent(float dTime) {
            ReadMouseInput();
            ReadKeyboardInput();
        }

        protected override void OnTerminate() {
            base.OnTerminate();
            _timeSystem.UpdateEvent -= TimeSystemOnUpdateEvent;
        }

        public bool RegisterDragBegin() {
            
            var mp = Input.mousePosition;
            var ray = Camera.ScreenPointToRay(mp);

            if (!_dragIsHappening) {
                var lmbDown = Input.GetMouseButtonDown((int)MouseButton.Left);
                RaycastHit hit;
                if (lmbDown) {
                    if (Physics.Raycast(ray, out hit, 100.0f)) {
                        var e1 = hit.collider.GetComponent<EntityController>();
                        if (e1 != null) {
                            var hitAdr = Address.FromPosition( hit.point);
                            _mouseDownAddress = hitAdr;
                            return false;
                        }
                    }
                }
                
                var holdLmbDown = Input.GetMouseButton((int) MouseButton.Left);
                if (holdLmbDown) {
                    if (Physics.Raycast(ray, out hit, 100.0f)) {
                        var e1 = hit.collider.GetComponent<EntityController>();
                        if (e1 != null) {
                            var hitAdr = Address.FromPosition( hit.point);
                            if (_mouseDownAddress != hitAdr) {
                                _dragIsHappening = true;
                                return true;
                            }
                        }
                    }
                }
            }
            
            return false;
        }

        public bool RegisterDragEnd() {
            if (!_dragIsHappening) {
                return false;
            }
            
            var lmbUp = Input.GetMouseButtonUp((int)MouseButton.Left);
            if (!lmbUp) {
                return false;
            }
            
            _dragIsHappening = false;
            
            var mp = Input.mousePosition;
            var ray = Camera.ScreenPointToRay(mp);

            if (!Physics.Raycast(ray, out var hit, 100.0f)) {
                
                return false;
            }

            var e = hit.collider.GetComponent<EntityController>();
            if (e == null) {
                return false;
            }

            var hPos = hit.point;
            var adr = Address.FromPosition(hPos);

            if (adr == _mouseDownAddress) {
                return false;
            }

            if (Universe.Param.IsValid(_mouseDownAddress) && Universe.Param.IsValid(adr)) {
                _mouseUpAddress = adr;
                return true;
            }

            return false;
        }
        
        public static bool IsPointerOverUiObject()
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current) {
                position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
            };
            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        public void ReadMouseInput() {
            
            if (IsPointerOverUiObject()) {
                return;
            }
            
            var mp = Input.mousePosition;
            var ray = Camera.ScreenPointToRay(mp);

            if (!Physics.Raycast(ray, out var hit, 100.0f)) {
                return;
            }

            var hPos = hit.point;
            var clickAddress = Address.FromPosition(hPos);
            
            if (RegisterDragBegin()) {
                _ugiSystem.DragOnGroundStarted(_mouseDownAddress, clickAddress);
                return;
            }

            if (RegisterDragEnd()) {
                _ugiSystem.DragOnGroundTerminated(_mouseDownAddress, _mouseUpAddress);
                return;
            }

            if (_dragIsHappening) {
                _ugiSystem.DragOnGroundOngoing(_mouseDownAddress, clickAddress);
            }
            
            var lmbUp = Input.GetMouseButtonUp((int)MouseButton.Left);
            var rmbUp = Input.GetMouseButtonUp((int)MouseButton.Right);

            //If mouse is not over any valid entity then I won't register any click or even the mouse over position
            var ec = hit.collider.GetComponent<EntityController>();
            if (ec == null) {
                return;
            }
            var e = ec.entity;
            _ugiSystem.MouseOverEntity( e,  hit.point, hit.normal, clickAddress);
            
            if (lmbUp || rmbUp) {
                
                var btt = lmbUp ? ButtonType.Left : ButtonType.Right;
                _ugiSystem.ClickOnEntity(btt, e,  hit.point, hit.normal, clickAddress);
            }
        }

        public void ReadKeyboardInput() {
            
            if (Input.GetKeyDown(KeyCode.F5)) {
                Universe.Send().Request(new SaveLoadGameRequest("quick_save.xml", true));
                return;
            }
            
            if (Input.GetKeyDown(KeyCode.F6)) {
                Universe.Send().Request(new SaveLoadGameRequest("quick_save.xml", false));
                return;
            }
            
            _ugiSystem.ReadKeyboardInput();
            
            
            
            // if (Input.GetKeyDown(KeyCode.M)) {
            //     foreach (var tile in _boardSystem.Board.GetAllTiles()) {
            //         if (tile.Propbits.HasAny(Propbits.Walkable)) {
            //             Debug.DrawRay(tile.Address.Pos(), Vector3.up, Color.red, 10f);
            //         }
            //     }
            // }
            //
            // if (Input.GetKeyDown(KeyCode.N)) {
            //     foreach (var tile in _boardSystem.Board.GetAllTiles()) {
            //         if (tile.Propbits.HasAny(Propbits.Blocks)) {
            //             Debug.DrawRay(tile.Address.Pos(), Vector3.up, Color.blue, 10f);
            //         }
            //     }
            // }
        }
    }

}