using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Extensions;
using Ce.Core.System;
using Ce.Core.System.Job;
using Ce.Core.Util;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Links;
using Ce.Game.Components.Tags;
using Ce.Game.Message.Command;
using Ce.Game.Models;
using UnityEngine;
using NotImplementedException = System.NotImplementedException;

namespace Ce.Game.System
{
    /// <summary>
    /// Camera module will update the position of the active camera based on the position of the active character
    /// </summary>
    public class CameraSystem: BaseSystem {

        private Group _activeCameraGroup;

        private TimeSystem _timeSystem;

        private MessageSystem _messageSystem;
        
                /// <summary> The speed in which the camera moves side to side or back and forth in m/s </summary>
        private float _maxMoveSpeed = 3;

        /// <summary> Range between -(_maxMoveSpeed) and +(_maxMoveSpeed), (0 ± threshold) being no lateral movement. </summary>
        private float _lateralMovement;

        /// <summary> Range between -(_maxMoveSpeed) and +(_maxMoveSpeed), (0 ± threshold) being no axial movement. </summary>
        private float _axialMovement;

        /// <summary> True when any forward movement promoter key is being held. </summary>
        private bool _forwardKeyHeld;

        /// <summary> True when any backward movement promoter key is being held. </summary>
        private bool _backwardKeyHeld;

        /// <summary> True when any left strafe movement promoter key is being held. </summary>
        private bool _strafeLeftKeyHeld;

        /// <summary> True when any right strafe movement promoter key is being held. </summary>
        private bool _strafeRightKeyHeld;

        private bool _mouseForwardPan, _mouseBackwardPan, _mouseLeftPan, _mouseRightPan;

        /// <summary> The speed in which the camera rotates around its y-axis in rad/s </summary>
        private float _maxRotateSpeed = (2 * Mathf.PI) * 0.2f; // => 0.25 Hz.

        /// <summary> Range in rad between -(_maxRotateSpeed) and +(_maxRotateSpeed), (0 ± threshold) being no left or right rotation. </summary> 
        private float _rotation;

        /// <summary> True when any rotate clockwise promoter key is being held. </summary>
        private bool _rotateClockwiseKeyHeld;

        /// <summary> True when any rotate counterclockwise promoter key is being held. </summary>
        private bool _rotateCounterclockwiseKeyHeld;

        /// <summary> Speed at which the camera will change its FOV in degrees/scrollDelta. Notice that it's per event and it isn't suppose to take delta time into account.  </summary>
        private float _scrollSpeed = -1f;

        /// <summary> Max field of view in degrees. </summary>
        private float _maxFov = 90f;

        /// <summary> Min field of view in degrees. </summary>
        private float _minFov = 2f;

        protected override void OnInitialize() {
            
            _timeSystem = Universe.GetSystem<TimeSystem>();
            _messageSystem = Universe.GetSystem<MessageSystem>();
            
            _messageSystem.SubscribeToMessage<BeginGroundCombatCommand>(this, HandleBeginGroundCombatCommand);
                
            _timeSystem.UpdateEvent += TimeSystemOnUpdateEvent;
            
            RegisterGroup(Matcher.New(Universe).All<TransformComponent, CameraComponent>(),"TrackCameras")
                .OnEntityAdded(OnCameraAdded)
                .OnEntityRemoved(OnCameraRemoved);

            _activeCameraGroup = RegisterGroup(Matcher.New(Universe).All<TransformComponent, ActiveCameraComponent>(), "TrackActiveCamera");
            // .EntityAdd(e => OnActiveCameraAdd((CameraEntity) e))
            // .EntityRemove(e => OnActiveCameraRemove((CameraEntity) e));

            var ce = Object.FindObjectOfType<CameraController>();
            if (ce != null) {

                var go = ce.gameObject;

                Universe.AcquireEntity()
                        .SetTransform(go.transform.position, go.transform.rotation)
                        .SetGameObject(go)
                        .SetCameraTag();
            }
            else {
                Debug.LogError("Add a camera to your scene.");
                //Note! Unity complains if the game starts with no camera so it is best if you add an object 
                //to be the camera before any system is loaded. Then I capture the camera here an create
                //a camera entity "for now"
            }

        }

        private void HandleBeginGroundCombatCommand(BeginGroundCombatCommand obj) {
            
            var gem = Universe.GetModels<GameEntityModels>();
            
            //Active camera
            var ac = _activeCameraGroup.Entities.First();

            //Find lt (local totem)
            var lp = Universe.Entities(gem.LocalPlayer).First();
            var lpg = lp.GetLinkPlayerToGod();
            var lt = lpg.GetLinkGodToTotem();

            //Initial and final position
            var p0 = ac.GetTransform().Position;
            var p1 = lt.GetTransform().Position;
            
            //Program interpolation
            const float tf = 1f;
            var ts = Universe.TimeSystem();
            var t0 = ts.Time;
            
            Universe.RegisterJob<LambdaJob>()
                    .OnTick(job => {
                                var dt = ts.Time - t0;
                                var t = dt * tf;
                                var pos = Vector3.Lerp(p0, p1, t);
                                ac.SetTransform(pos);
                                if (t >= 1f) {
                                    job.TriggerAfter(JobTrigger.Success);
                                }
                            });
            
        }

        private Entity _currentServant;
        
        /// <summary>
        /// TODO Move this to a proper system
        /// </summary>
        private void MoveCameraToNextServant() {
            var gem = Universe.GetModels<GameEntityModels>();
            
            //Active camera
            var ac = _activeCameraGroup.Entities.First();

            //Find lt (local totem)
            var lp = Universe.Entities(gem.LocalPlayer).First();
            var lpg = lp.GetLinkPlayerToGod();
            var lt = lpg.GetLinkGodToServants();
            if (_currentServant == null) {
                _currentServant = lt.First();
            }
            else {
                var result = lt.ItemAfter(_currentServant);
                if (result.IsSet) {
                    _currentServant = result.Value;
                }
                else {
                    _currentServant = lt.First();
                }
            }
            
            //Initial and final position
            var p0 = ac.GetTransform().Position;
            var p1 = _currentServant.GetTransform().Position;
            
            //Program interpolation
            const float tf = 3f;
            var ts = Universe.TimeSystem();
            var t0 = ts.Time;
            
            Universe.RegisterJob<LambdaJob>()
                    .OnTick(job => {
                                var dt = ts.Time - t0;
                                var t = dt * tf;
                                var pos = Vector3.Lerp(p0, p1, t);
                                ac.SetTransform(pos);
                                if (t >= 1f) {
                                    job.TriggerAfter(JobTrigger.Success);
                                }
                            });
            
            
        }

        protected override void OnTerminate() {
            _messageSystem.UnsubscribeFromMessage<BeginGroundCombatCommand>(this);
            _timeSystem.UpdateEvent -= TimeSystemOnUpdateEvent;
            _timeSystem = null;
            base.OnTerminate();
        }

        private bool KeyDownHold(KeyCode key) {
            return Input.GetKeyDown(key) || Input.GetKey(key);
        }

        private void TimeSystemOnUpdateEvent(float deltaTime) {

            var tabKeyPressed = Input.GetKeyDown(KeyCode.Tab);
            if (tabKeyPressed) {
                MoveCameraToNextServant();
                return;
            }
            
            _forwardKeyHeld = KeyDownHold(KeyCode.W) || KeyDownHold(KeyCode.UpArrow);
            _backwardKeyHeld = KeyDownHold(KeyCode.S) || KeyDownHold(KeyCode.DownArrow);
            _strafeLeftKeyHeld = KeyDownHold(KeyCode.A) || KeyDownHold(KeyCode.LeftArrow);
            _strafeRightKeyHeld = KeyDownHold(KeyCode.D) || KeyDownHold(KeyCode.RightArrow);

            _rotateCounterclockwiseKeyHeld = KeyDownHold(KeyCode.E);
            _rotateClockwiseKeyHeld = KeyDownHold(KeyCode.Q);
            
            UpdateCameraPosition(deltaTime);
        }

        /// <summary>
        /// Set the first camera as the active camera
        /// </summary>
        private void OnCameraAdded(Entity camera)
        {
            if (!_activeCameraGroup.Entities.Any()) {
                camera.SetActiveCameraTag();
            }
            
        }
        
        /// <summary>
        /// It will unset the ActiveCamera tag if the camera has the tag
        /// </summary>
        private void OnCameraRemoved(Entity camera)
        {
            if (!camera.Active) return;
            
            if (camera.HasActiveCameraTag()) {
                camera.RemoveActiveCameraTag();
            }
            
        }        


        /// <summary>
        /// Make the camera follow the character.
        /// </summary>
        private void UpdateCameraPosition(float deltaTime) {
            var cam = _activeCameraGroup.Entities.FirstOrDefault();
            if (cam == null) {
                return;
            }
            
            var forward = _forwardKeyHeld || _mouseForwardPan;
            var backward = _backwardKeyHeld || _mouseBackwardPan;
            var strafeLeft = _strafeLeftKeyHeld || _mouseLeftPan;
            var strafeRight = _strafeRightKeyHeld || _mouseRightPan;

            var moved = false;

            var pos = cam.GetTransform().Position;
            var rot = cam.GetTransform().Rotation;
            
            //Compute movement back/forth and lateral
            {
                _axialMovement += ((forward ? 1 : 0) - (backward ? 1 : 0)) * _maxMoveSpeed * deltaTime;
                _lateralMovement += ((strafeRight ? 1 : 0) - (strafeLeft ? 1 : 0)) * _maxMoveSpeed * deltaTime;

                _axialMovement = Mathf.Clamp(_axialMovement, -_maxMoveSpeed, _maxMoveSpeed);
                _lateralMovement = Mathf.Clamp(_lateralMovement, -_maxMoveSpeed, _maxMoveSpeed);

                const float threshold = 0.01f; //1 cm

                //Camera lateral and back and forth movement will happen only if it's above a minimum threshold.
                if ((Mathf.Abs(_lateralMovement) > threshold) || (Mathf.Abs(_axialMovement) > threshold)) {
                    
                    var deltaPos = new Vector3(_lateralMovement, 0, _axialMovement);
                    pos += (rot * deltaPos);
                    moved = true;
                }
            }
            
            //Rotation
            {
                _rotation += ((_rotateClockwiseKeyHeld ? 1 : 0) - (_rotateCounterclockwiseKeyHeld ? 1 : 0)) * _maxRotateSpeed * deltaTime;
                _rotation = Mathf.Clamp(_rotation, -_maxRotateSpeed, _maxRotateSpeed);

                const float threshold = 0.001f; //1 "cm"

                //Camera rotation will happen only if it's above a minimum threshold.
                if (Mathf.Abs(_rotation) > threshold) {
                    
                    rot *= Quaternion.AngleAxis(_rotation * Mathf.Rad2Deg, Vector3.up);
                    moved = true;
                }
            }

            //Amortization so that the movement doesn't stop abruptly 
            _lateralMovement *= 0.85f;
            _axialMovement *= 0.85f;
            _rotation *= 0.80f;


            // Reset the move variables
            if (moved) {
                cam.SetTransform(pos, rot);
            }
        }
    }
}