using System;
using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Core.World;
using Ce.Game.Components.Collection;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.String;
using Ce.Game.Components.Tags;
using Ce.Game.Components.Value;
using Ce.Game.Message.Command;
using Ce.Game.World;
using UnityEngine;
using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game.System {

    public class BuildSystem : BaseSystem {

        private MessageSystem _messageSystem;

        private Group _hpGroup;
        
        protected override void OnInitialize() {
            
            base.OnInitialize();
            _messageSystem = Universe.GetSystem<MessageSystem>();
            _messageSystem.SubscribeToMessage<ActorBuildCommand>(this, HandleBuildCommand);

            _hpGroup = Universe.GetGroupByName(GCG.HitPointEntities);
        }

        private void HandleBuildCommand(ActorBuildCommand c) {
            
            var e = FindEntity(c.ActorRid);

            if (e.HasPath()) {
                e.RemovePath();
            }

            if (e.HasLinkSelectedToGod()) {
                e.RemoveLinkSelectedToGod();
            }
            
            //Remove AP from attacker
            var currAp = e.GetActionPoints();
            var finalAp = currAp - GameConstants.GameParameters.ApPerBuild;
            finalAp = Math.Max(0, finalAp);
            e.SetActionPoints(finalAp);

            //Animations
            foreach (var child in e.GetGameObject().GetComponentsInChildren<Animator>()) {
                child.SetTrigger("Fire");
            }

            var god = e.GetLinkServantsToGod();

            var pos = e.GetAddress().Pos();
            var rot = Quaternion.identity;
            
            var tower = Universe.AcquireEntity()
                    .SetNid(c.NewEntityRid)
                    .SetDisplayName("Tower")
                    .SetGClass(GClasses.Tower)
                    .SetHitPoints(110)
                    .SetPortrait("Media/Portraits/Tower_S0")
                    .SetPrefab("Buildings/Tower_S0")
                    .SetTransform(pos, rot)
                    .SetShape(BoardSystem.GenerateShape(BoardSystem.EntitySize.One, BoardSystem.EntityShape.CenteredCircle))
                    .SetPropbits(Propbits.Blocks | Propbits.Building)
                    .SetOnBoardTag()
                    .LinkServantsToGod(god)
                    .SetSelectableTag()
                    .SetGameObjectDisplay(DisplayTypes.Shown)
                    .DefineName("Tower");
            
            tower.LinkSelectedToGod(god);

            var newPos = GetFirstNeighbour(e.GetAddress());
            e.SetTransform(newPos.Pos());

            
        }
        
        private Address GetFirstNeighbour(Address current) {

            var board = Universe.GetSystem<BoardSystem>().Board;
            
            var a = current;
            for (var i = 0; i < 8; i++) {
                var dir = ((Directions) i);
                var nbAdd = a + dir.DirToAdd();
                
                if (Universe.Param.IsValid(nbAdd)) {
                    var tile = board.GetElement(nbAdd);
                    var propbits = tile.Propbits;
                    
                    if (propbits.HasAll(Propbits.Walkable)) {
                        if (propbits.HasNone(Propbits.Blocks)) {
                            return nbAdd;
                        }   
                    }
                }
            }

            return current;
        }

        private Entity FindEntity(ulong actorRid) {
            return _hpGroup.First(e=>e.GetNid() == actorRid);
        }

        protected override void OnTerminate() {
            _messageSystem.UnsubscribeFromMessage<AttackCommand>(this);
            
            base.OnTerminate();
        }
    }
}