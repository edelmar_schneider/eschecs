using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Message;
using Ce.Core.System;
using Ce.Core.System.Job;
using Ce.Core.Util;
using Ce.Game.Components;
using Ce.Game.Components.Data;
using Ce.Game.Components.Job;
using Ce.Game.Components.Numeric;
using Ce.Game.Message.Command;


namespace Ce.Game.System {

    /// <summary>
    /// Will handle the actions of the actors when they need to do something
    /// </summary>
    public class ActionSystem: BaseSystem {

        private MessageSystem _messageSystem;
        private JobSystem _jobSystem;

        private Group _actionPoints;

        protected override void OnInitialize() {
            base.OnInitialize();

            _jobSystem = Universe.GetSystem<JobSystem>();
            _messageSystem = Universe.GetSystem<MessageSystem>();
            
            _messageSystem.SubscribeToAll(this, MessageHandler);

            _actionPoints = RegisterGroup(Matcher.New(Universe).All<ActionPointsComponent>(),
                                          "ActionPointsEntities_ActionSystem");
            
            RegisterGroup(Matcher.New(Universe).All<ActionComponent>(), "ActionComponentEntities_ActionSystem")
                .OnEntityAdded(OnAction)
                .OnComponentUpdated<ActionComponent>((g, e, oc, nc) => OnAction(e));
        }

        private void MessageHandler(IMessage m) {

            if (m is IActorCommand c) {
                var am = (AMessage) c;
                var e = FindEntity(c.ActorRid);
                e.SetAction(am);
                //This will trigger this system and cause the creation of an Action
            }
        }

        private Entity FindEntity(ulong actorRid) {
            return _actionPoints.FirstOrDefault(e => e.HasNid() && e.GetNid() == actorRid);
        }

        private void OnAction(Entity e) {
            var c = e.GetAction();

            //Move command
            if (c is MoveToActorCommand mc) {
                _jobSystem.RegisterJob<TravelJob>()
                          .SetActor(e)
                          .SetCommand(mc);
            }
        }

        protected override void OnTerminate() {
            _messageSystem.UnsubscribeToAll(this);
            base.OnTerminate();
        }

    }

}