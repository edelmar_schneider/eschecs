using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Core.System.Job;
using Ce.Game.Components.Collection;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.Tags;
using Ce.Game.Extensions;
using Ce.Game.Message.Command;
using Ce.Game.Message.Request;
using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game.System {
    
    /// <summary>
    /// This system is responsible for coordinating the turns for the players.
    /// </summary>
    public class TurnControlSystem: BaseSystem {

        private Group _gods;

        private MessageSystem _messageSystem;

        protected override void OnInitialize() {
            base.OnInitialize();
        
            _gods = Universe.GetGroupByName(GCG.Gods)
                    .OnComponentAdded<TurnDoneComponent>((group, entity, arg3) => OnTurnDoneUpdate());
            
            _messageSystem = Universe.GetSystem<MessageSystem>();
            _messageSystem.SubscribeToMessage<GodPassTurnCommand>(this, HandleGodPassTurnCommand);
            _messageSystem.SubscribeToMessage<PassTurnCommand>(this, HandlePassTurnCommand);
        }

        protected override void OnTerminate() {
            _messageSystem.UnsubscribeFromMessage<GodPassTurnCommand>(this);
            _messageSystem.UnsubscribeFromMessage<PassTurnCommand>(this);
            base.OnTerminate();
        }
        
        private void HandlePassTurnCommand(PassTurnCommand c) {
            foreach (var god in _gods.ToArray()) {
                
                //Reset the Action Points of all entities controlled by the gods
                foreach (var servant in god.GetLinkGodToServants()) {
                    if (servant.HasPath()) {
                        servant.RemovePath();
                    }
                    if (servant.HasActionPoints()) {
                        servant.SetActionPoints(servant.GetActionPointsRefuel());
                    }
                }

                //Remove the TurnDoneTag from all gods.
                god.RemoveTurnDoneTag();
            }
            var gde = Universe.GetGameDefinitionsEntity();
            if (gde.GetInteractionMode() == IntModes.WaitingTurnToPass) {
                gde.SetInteractionMode(IntModes.Select);
            }
        }

        private void HandleGodPassTurnCommand(GodPassTurnCommand c) {
            
            var god = _gods.First(g => g.GetNid() == c.GodRid);
            god.SetTurnDoneTag();
        }

        private void OnTurnDoneUpdate() {

            var numberOfGods = _gods.Count();
            var numberOfDoneGods = _gods.Count(g => g.HasTurnDoneTag());

            if (numberOfGods == numberOfDoneGods) {
                if (Universe.IsServer()) {
                    //Time to pass the turn.
                    TryAndPassTurn();
                }
            }
            else {
                var gde = Universe.GetGameDefinitionsEntity();
                var lg = Universe.GetLocalGod();
                if (lg.HasTurnDoneTag()) {
                    
                    PassTurnOfAllAiGods();
                    
                    if (gde.GetInteractionMode() != IntModes.WaitingTurnToPass) {
                        var ls = Universe.GetLocallySelected();
                        ls?.RemoveLinkSelectedToGod();
                        gde.SetInteractionMode(IntModes.WaitingTurnToPass);
                    }
                }
                else {
                    if (gde.GetInteractionMode() == IntModes.WaitingTurnToPass) {
                        gde.SetInteractionMode(IntModes.Select);
                    }
                }
            }
        }

        private void TryAndPassTurn() {
            
            //But first I need to ensure that nobody is doing anything
            if (Universe.IsThereAnyAction()) {
                //If so I try again in 1 secong.
                Universe.QueueTask(TryAndPassTurn, 1.0f);
            }
            else {
                Universe.Send().Command(new PassTurnCommand());    
            }
        }


        /// <summary>
        /// if you are playing alone "since I don't have AI yet" I will pass the turn of all AI gods
        /// </summary>
        private void PassTurnOfAllAiGods() {

            if (!Universe.IsServer()) {
                return;
            }
            
            foreach (var god in _gods.ToArray()) {
                if (!god.HasTurnDoneTag()) {
                    if (god.IsAiGod()) {
                        Universe.QueueTask(() => {
                                               Universe.Send().Command( new GodPassTurnCommand(god.GetNid()));
                                           }, 0.2f);
                        return;
                    }
                }
            }
        }
    }
}