using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Game.Components.Collection;
using Ce.Game.Components.Links;
using Ce.Game.Extensions;
using Ce.Game.World;
using UnityEngine;
using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game.System {

    /// <summary>
    /// Responsible for showing actors and buildings that are locally selected
    /// </summary>
    public class MovementRangeDisplaySystem: BaseSystem {

        private GameObject _movementRangePrefab;

        private Entity _selectedEntity;

        private GameObject _selectionGo;

        private Group _selectedGroup;
        
        private GameObject _greenPlanePrefab;
        
        private readonly Stack<GameObject> _greenPlanesStack = new Stack<GameObject>();
        private readonly List<GameObject> _greenPlanesUsed = new List<GameObject>();

        private BoardSystem _boardSystem;
        private TileBoard _tileBoard;

        protected override void OnInitialize() {
            base.OnInitialize();

            _boardSystem = Universe.GetSystem<BoardSystem>();

            _greenPlanePrefab = Resources.Load<GameObject>("Ui/FlatSelectionMark");
            _movementRangePrefab = Resources.Load<GameObject>("Ui/FlatSelectionMark");
            
            _selectedGroup = Universe.GetGroupByName(GCG.SelectedEntities)
                    .OnEntityAdded(OnSelectedActorUpdate)
                    .OnEntityRemoved(OnSelectedActorUpdate)
                    .OnComponentUpdated<SelectedToGodComponent>((group, entity, arg3, arg4) => OnSelectedActorUpdate())
                    .OnComponentAdded<PathComponent>((group, entity, arg3) => UpdatePath())
                    .OnComponentRemove<PathComponent>((group, entity, arg3) => UpdatePath())
                    .OnComponentUpdated<PathComponent>((group, entity, arg3, arg4) => UpdatePath());
            
            for (var i = 0; i < 100; i++) {
                var plane = Object.Instantiate(_greenPlanePrefab, Vector3.zero, Quaternion.identity);
                plane.SetActive(false);
                _greenPlanesStack.Push(plane);
            }

            
        }

        private void UpdatePath() {
            if (_tileBoard == null) {
                _tileBoard = _boardSystem.Board;
            }
            
            foreach (var plane in _greenPlanesUsed) {
                plane.SetActive(false);
                _greenPlanesStack.Push(plane);
            }
            _greenPlanesUsed.Clear();
            
            var ls = Universe.GetLocallySelected();
            if (ls == null || !ls.HasPath() ) {
                return;
            }

            var path = ls.GetPath();
            foreach (var waypoint in path) {
                if (!_greenPlanesStack.Any()) {
                    break;
                }

                var tile = _tileBoard.GetElement(waypoint.AddressId);
                
                var planeGo = _greenPlanesStack.Pop();
                planeGo.transform.position = tile.Address.Pos();
                planeGo.SetActive(true);
                _greenPlanesUsed.Add(planeGo);
            }
        }

        private void OnSelectedActorUpdate() {
            
            //Remove current selection reference if it exists.
            if (_selectionGo != null) {
                Object.Destroy(_selectionGo);
                _selectionGo = null;
            }
            
            //Let's produce the selection reference if there is a new locally selected entity (Actor only)
            _selectedEntity = _selectedGroup.FirstOrDefault(e=>e.IsLocalEntity() && e.IsActor() && e.Active);
            if (_selectedEntity == null) {
                UpdatePath();
                return;
            }

            if (_selectedEntity.HasPath()) {
                UpdatePath();
                return;
            }



            //_selectionGo = Object.Instantiate(_movementRangePrefab);
            //
            // if (_selectionGo != null) {
            //     _selectionGo.transform.position = _selectedEntity.GetTransform().Position;
            // }
        }
        

    }

}