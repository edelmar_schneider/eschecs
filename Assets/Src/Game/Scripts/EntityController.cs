using Ce.Core.Ecs;
using UnityEngine;

namespace Ce.Game.Scripts {

    public class EntityController : MonoBehaviour{
        /// <summary>
        /// The entity connected to this GameObject
        /// </summary>
        public Entity entity;
    }

}