using UnityEngine;
using Random = UnityEngine.Random;

namespace Ce.Game.Scripts
{
    public class RandomColor : MonoBehaviour
    {
        public Color[] colors;

        private static Material[] _materials;
        
        public GameObject body;
        

        private void OnEnable() {
            
        if (_materials == null) {
                _materials = new Material[colors.Length];
                var baseMaterial = body.GetComponent<Renderer>().material;
                for (var index = 0; index < colors.Length; index++) {
                    var color = colors[index];
                    var material = new Material(baseMaterial) {
                        color = color
                    };
                    _materials[index] = material;
                }
            }
        
            
            SetRandomFloorColor();
        }

        private void OnDisable() {
            if (_materials != null) {
                foreach (var material in _materials) {
                    Destroy(material);
                }

                _materials = null;
            }
        }

        private void SetRandomFloorColor() {
            var materialIndex = Random.Range(0, _materials.Length - 1);
            body.GetComponent<Renderer>().material = _materials[materialIndex];

        } 
    }
}
