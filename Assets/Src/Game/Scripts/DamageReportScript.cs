using Ce.Core.Extensions;
using TMPro;
using UnityEngine;

namespace Ce.Game.Scripts
{
    public class DamageReportScript : MonoBehaviour {

        public float speed = 1f;
        public float lifeTime = 1.5f;
        
        public TMP_Text label;

        public GameObject body;

        private float _timeCounter;

        public void SetDamage(float damage) {
            var d = damage.RoundToInt();
            label.text = d.ToString();
        }

        public void SetMessage(string message) {
            label.text = message;
        }
        
        void Update() {
            _timeCounter += Time.deltaTime;

            if (_timeCounter >= lifeTime) {
                Destroy(this.gameObject);
            }
            
            body.transform.Translate(0,speed,0);

        }
    }
}
