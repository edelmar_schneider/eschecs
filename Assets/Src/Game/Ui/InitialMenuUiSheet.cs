using System.Linq;
using Ce.Core.Ui;
using Ce.Game.Components.Tags;
using Ce.Game.Models;
using UnityEngine;
using UnityEngine.UI;

namespace Ce.Game.Ui {

    public class InitialMenuUiSheet: BaseUiSheet {

        public Button btnServer;
        public Button btnClient;
        public Button btnExit;

        protected override void OnSetup() {
            base.OnSetup();
            
            btnServer.onClick.AddListener(OnBtnServerClicked);
            btnClient.onClick.AddListener(OnBtnClientClicked);
            btnExit.onClick.AddListener(OnBtnExitClicked);
        }

        private void OnBtnServerClicked() {
            //This should make the menu move forward so the player can configure the server properties
            var gd = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            gd.SetServerTag();
        }
        
        private void OnBtnClientClicked() {
            //This should make the menu move forward so the player can configure the client properties
            var gd = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            gd.SetClientTag();
        }

        private void OnBtnExitClicked() {
            Application.Quit();
        }

    }

}