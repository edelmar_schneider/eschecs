using Ce.Core.Ui;
using UnityEngine;
using UnityEngine.UI;

namespace Ce.Game.Ui.Game {
    
    public class LoseUiSheet : BaseUiSheet {

        public Button _btnExit;
        
        protected override void OnSetup() {
            base.OnSetup();
            _btnExit.onClick.AddListener(() => Application.Quit());
        }
    }

}