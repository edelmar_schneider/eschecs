using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Extensions;
using Ce.Core.System;
using Ce.Core.System.Job;
using Ce.Core.Ui;
using Ce.Core.Util;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.String;
using Ce.Game.Components.Tags;
using Ce.Game.Extensions;
using Ce.Game.Message.Request;
using Ce.Game.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using GCG = Ce.Game.GameConstants.NamedGroups;

namespace Ce.Game.Ui.Game {
    
    public class GroundCombatUiSheet: BaseUiSheet {
        
        public Button btnExit;
        public Button btnPassTurn;
        public Button btnBuild;

        public Slider sliderHp;
        public Slider sliderAp;

        public TMP_Text lblActorName;
        public TMP_Text lblActorHp;
        public TMP_Text lblActorAp;

        public Image lblActorImage;
        
        public Canvas entityCanvas;
        public CanvasGroup entityCanvasGroup;

        public GameObject workerPanel;

        private Group _selectedGroup;
        private Entity _selectedEntity;

        private LambdaJob _panelJob;
        
        private readonly StateMachine<PanelState, PaneTriggers> _machine = new StateMachine<PanelState, PaneTriggers>(PanelState.Hidden);
        
        protected override void OnSetup() {
            base.OnSetup();
            
            //Buttons
            btnExit.onClick.AddListener(OnBtnExitClicked);
            btnPassTurn.onClick.AddListener(OnBtnPassTurn);
            btnBuild.onClick.AddListener(OnBtnBuild);
           
            ClearEntity();

            SetupMachine();
        }

        protected override void OnShow() {
            base.OnShow();

            Universe.GetGroupByName(GCG.Gods)
                    .OnComponentAdded<TurnDoneComponent>((group, entity, arg3) => OnTurnDoneUpdate())
                    .OnComponentUpdated<TurnDoneComponent>((group, entity, arg3, arg4) => OnTurnDoneUpdate())
                    .OnComponentRemove<TurnDoneComponent>((group, entity, arg3) => OnTurnDoneUpdate());
            
            _selectedGroup = Universe.GetGroupByName(GCG.SelectedEntities)
                                     .OnEntityAdded(OnSelectedActorUpdate)
                                     .OnEntityRemoved(OnSelectedActorUpdate)
                                     .OnComponentUpdated<SelectedToGodComponent>((group, entity, arg3, arg4) => OnSelectedActorUpdate())
                                     .OnComponentUpdated<HitPointsComponent>((group, entity, arg3, arg4) => SemiUpdate())
                                     .OnComponentUpdated<ActionPointsComponent>((group, entity, arg3, arg4) => SemiUpdate());

            if (_selectedGroup.Any()) {
                OnSelectedActorUpdate();
            }
        }

        private void OnTurnDoneUpdate() {
            var lg = Universe.GetLocalGod();
            if (lg.HasTurnDoneTag()) {
                btnPassTurn.interactable = false;
                btnPassTurn.GetComponentInChildren<UiElementScript>().GetComponent<TMP_Text>().text = "WAIT";
            }
            else {
                btnPassTurn.GetComponentInChildren<UiElementScript>().GetComponent<TMP_Text>().text = "Pass Turn";
                btnPassTurn.interactable = true;
            }
        }

        private void SetupMachine() {
            _machine.Configure()
                    .State(PanelState.Hidden)
                    .Trigger(PaneTriggers.Show, PanelState.Showing)
                    .Return()
                    .State(PanelState.Shown)
                    .Trigger(PaneTriggers.Hide, PanelState.Hiding)
                    .Return()
                    .State(PanelState.Showing)
                    .Trigger(PaneTriggers.Hide, PanelState.Hiding)
                    .OnEnter((oldState, newState) => {
                                 if (oldState == PanelState.Hiding) {
                                     _panelJob.SetState(JobState.Aborted);
                                     ShowUpEntityPanel();
                                     return;
                                 }

                                 if (oldState == PanelState.Hidden) {
                                     ShowUpEntityPanel();
                                     return;
                                 }
                             })
                    .Return()
                    .State(PanelState.Hiding)
                    .Trigger(PaneTriggers.Show, PanelState.Showing)
                    .OnEnter((oldState, newState) => {
                                 if (oldState == PanelState.Showing) {
                                     _panelJob.SetState(JobState.Aborted);
                                     HideAwayEntityPanel();
                                     return;
                                 }

                                 if (oldState == PanelState.Shown) {
                                     HideAwayEntityPanel();
                                     return;
                                 }
                             });
        }


        /// <summary>
        /// Updates only ActionPoints and HitPoints.
        /// </summary>
        private void SemiUpdate() {
            if (_selectedEntity == null) {
                return;
            }

            var e = _selectedEntity;
            
            var isActor = e.IsActor();

            if (isActor) {
                sliderAp.value = e.GetActionPoints() / e.GetActionPointsRefuel();
                lblActorAp.text = e.GetActionPoints().RoundToInt().ToString();
                
                sliderAp.gameObject.SetActive(true);
                lblActorAp.gameObject.SetActive(true);
            }
            else {
                sliderAp.gameObject.SetActive(false);
                lblActorAp.gameObject.SetActive(false);
            }
            
            sliderHp.value = e.GetHitPoints() / 100f;
            lblActorHp.text = e.GetHitPoints().ToString();
        }

        private void OnSelectedActorUpdate() {
            
            //Let's produce the selection reference if there is a new locally selected entity (Actor or Building)
            //We don't want to show the selection when the actor is performing any action.
            _selectedEntity = _selectedGroup.FirstOrDefault(e=>e.IsLocalEntity() && e.Active);
            if (_selectedEntity == null) {
                _machine.Trigger(PaneTriggers.Hide);
                return;
            }

            UpdateEntityPanelInformation(_selectedEntity);
            _machine.Trigger(PaneTriggers.Show);
        }

        private void UpdateEntityPanelInformation(Entity e) {

            var isActor = e.IsActor();

            if (isActor) {
                sliderAp.value = e.GetActionPoints() / e.GetActionPointsRefuel();
                lblActorAp.text = e.GetActionPoints().RoundToInt().ToString();
                
                sliderAp.gameObject.SetActive(true);
                lblActorAp.gameObject.SetActive(true);
            }
            else {
                sliderAp.gameObject.SetActive(false);
                lblActorAp.gameObject.SetActive(false);
            }
            
            sliderHp.value = e.GetHitPoints() / 100f;
            lblActorName.text = e.GetDisplayName();
            
            lblActorHp.text = e.GetHitPoints().ToString();

            var image = Resources.Load<Sprite>(e.GetPortrait());
            lblActorImage.sprite = image;
        }
        
        private void OnBtnBuild() {
            var e = _selectedEntity;
            
            var isWorker = e.IsWorker();
            if (!isWorker) {
                return;
            }

            if (e.GetActionPoints() < GameConstants.GameParameters.ApPerBuild) {
                
                var prefab = Resources.Load<GameObject>("Ui/DamageReport");
                var sp = Camera.main.WorldToScreenPoint(e.GetTransform().Position + Vector3.up);
                var dre = Instantiate(prefab, Vector3.zero, Quaternion.identity)
                                .GetComponent<DamageReportScript>();
                dre.SetMessage("No Action Points");
                dre.speed = 0.2f;
                dre.body.transform.position = sp;

                return;
            }
            
            Universe.Send().Request(new ActorBuildRequest(e.GetNid()) );
        }

        private void OnBtnPassTurn() {
            var lg = Universe.GetLocalGod();
            if (lg.HasTurnDoneTag()) {
                return;
            }
            
            Universe.Send().Request(new GodPassTurnRequest(lg.GetNid()));
            //lg.SetTurnDoneTag();
        }

        private void OnBtnExitClicked() {
            Application.Quit();
        }

        private void ShowUpEntityPanel() {

            if (_selectedEntity.IsWorker()) {
                btnBuild.interactable = _selectedEntity.GetActionPoints() >= GameConstants.GameParameters.ApPerBuild;
                workerPanel.SetActive(true);
            }
            else {
                workerPanel.SetActive(false);
            }
            

            var tf = 1f / fadeInTime;
            var ts = Universe.TimeSystem();
            var t0 = ts.Time;
            var initialAlpha = 0f;

            _panelJob = Universe.RegisterJob<LambdaJob>()
                                .OnActivate(job => {
                                                entityCanvas.enabled = true;
                                                entityCanvasGroup.blocksRaycasts = true;
                                                initialAlpha = entityCanvasGroup.alpha;
                                                job.TriggerAfter(JobTrigger.Tick);
                                            })
                                .OnTick(job => {

                                            var dt = ts.Time - t0;
                                            var t = dt * tf;
                                            entityCanvasGroup.alpha = Mathf.Lerp(initialAlpha, 1f, t);
                                            if (t >= 1f) {
                                                job.TriggerAfter(JobTrigger.Success);
                                            }
                                        });


        }

        private void HideAwayEntityPanel() {
            
            var tf = 2f;
            var ts = Universe.TimeSystem();
            var t0 = ts.Time;
            var initialAlpha = 1f;

            _panelJob = Universe.RegisterJob<LambdaJob>()
                                .OnActivate(job => {
                                                entityCanvasGroup.blocksRaycasts = false;
                                                initialAlpha = entityCanvasGroup.alpha;
                                                job.TriggerAfter(JobTrigger.Tick);

                                            })
                                .OnTick(job => {
                                            var dt = ts.Time - t0;
                                            var t = dt * tf;
                                            entityCanvasGroup.alpha = Mathf.Lerp(initialAlpha, 0f, t);
                                            if (t >= 1f) {
                                                entityCanvas.enabled = false;
                                                job.TriggerAfter(JobTrigger.Success);
                                            }
                                        });
        }

        private void ClearEntity() {
            //Slider
            sliderAp.value = 0;
            sliderHp.value = 0;
            
            //labels
            lblActorName.text = "Unknown";
            lblActorAp.text = "0";
            lblActorHp.text = "0";
            
            //Picture
            var texture = Sprite.Create(Texture2D.grayTexture, new Rect(0, 0, 4, 4), Vector2.one * 0.5f);
            lblActorImage.sprite = texture;
        }

        public enum PanelState {
            Shown,
            Hidden,
            Hiding,
            Showing
        }

        public enum PaneTriggers {
            Show,
            Hide,
        }
    }
    
    
}