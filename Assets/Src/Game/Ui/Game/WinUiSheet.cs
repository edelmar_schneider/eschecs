using Ce.Core.Ui;
using UnityEngine;
using UnityEngine.UI;

namespace Ce.Game.Ui.Game {

    public class WinUiSheet:BaseUiSheet {
        public Button _btnExit;
        
        protected override void OnSetup() {
            base.OnSetup();
            _btnExit.onClick.AddListener(() => Application.Quit());
        }
    }

}