using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Ui;
using Ce.Game.Components.String;
using Ce.Game.Models;
using TMPro;
using UnityEngine.UI;

namespace Ce.Game.Ui.Client {

    public class ClientLobbyUiSheet: BaseUiSheet {
        
        public Button btnReturn;
        public TMP_InputField inStatus;

        private Group _clientsGroup;

        protected override void OnShow() {
            base.OnShow();
            
            var aem = Universe.GetModels<AppEntityModels>();
            _clientsGroup = Universe.RegisterGroup(Group.New(aem.ServerClient))
                                    .OnEntityAdded(UpdateStatus);
            
            //TODO Check connection status
            inStatus.text = "espere começar";
        }

        private void UpdateStatus() {
            
        }

        protected override void OnHide() {
            base.OnHide();
            Universe.UnregisterGroup(_clientsGroup);
        }

        protected override void OnSetup() {
            base.OnSetup();
            
            btnReturn.onClick.AddListener(OnBtnReturnClicked);
        }

        private void OnBtnReturnClicked() {
            //This should make the menu return to its previous state
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            gDef.RemoveIp();
        }
    }
}