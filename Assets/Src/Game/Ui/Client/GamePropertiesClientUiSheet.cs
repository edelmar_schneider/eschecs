using System.Linq;
using Ce.Core.Ui;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.String;
using Ce.Game.Components.Tags;
using Ce.Game.Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ce.Game.Ui.Client {

    public class GamePropertiesClientUiSheet: BaseUiSheet  {
        
        public Button btnReturn;
        public Button btnConnect;
        
        public TMP_InputField inIp;
        public TMP_InputField inPort;

        protected override void OnShow() {
            base.OnShow();
            //By this point the GameNetworkSystem must have set he default port already.
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            inPort.text = gDef.GetPort().ToString(); 
        }

        protected override void OnSetup() {
            base.OnSetup();
            
            btnReturn.onClick.AddListener(OnBtnReturnClicked);
            btnConnect.onClick.AddListener(OnBtnConnectClicked);
        }

        private void OnBtnReturnClicked() {
            //This should make the menu return to its previous state
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            gDef.RemoveClientTag();
            gDef.RemovePort();
        }
        
        private void OnBtnConnectClicked() {
            if (!InputIsValid()) {
                Debug.LogError("Input is not valid!");
                return;
            }
            
            //Update the game definitions entity so it moves forwards.
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            var ip = inIp.text;
            var port = int.Parse(inPort.text);
            
            gDef.SetIp(ip);
            gDef.SetPort(port);
        }

        private bool InputIsValid() {
            
            //Parse port
            {
                if (int.TryParse(inPort.text, out var val)) {
                    if (val < 0 || val > 65535) {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }        
            
            //TODO Parse ip
            {
                
            }

            return true;
        }        

    }

}