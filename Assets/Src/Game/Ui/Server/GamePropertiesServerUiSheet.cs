using System.Linq;
using Ce.Core.Ui;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.Tags;
using Ce.Game.Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ce.Game.Ui.Server {

    public class GamePropertiesServerUiSheet: BaseUiSheet  {
        public Button btnReturn;
        public Button btnNext;

        public TMP_InputField inMapSize;
        public TMP_InputField inPlayers;
        public TMP_InputField inPort;

        protected override void OnShow() {
            base.OnShow();
            //By this point the GameNetworkSystem must have set he default port already.
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            inPort.text = gDef.GetPort().ToString(); 
        }

        protected override void OnSetup() {
            base.OnSetup();
            
            btnReturn.onClick.AddListener(OnBtnReturnClicked);
            btnNext.onClick.AddListener(OnBtnNextClicked);
        }

        private void OnBtnReturnClicked() {
            //This should make the menu return to its previous state
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            gDef.RemoveServerTag();
            gDef.RemovePort();
        }
        
        private void OnBtnNextClicked() {
            if (!InputIsValid()) {
                Debug.LogError("Input is not valid!");
                return;
            }
            
            //Update the game definitions entity so it moves forwards.
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            var players = int.Parse(inPlayers.text);
            var port = int.Parse(inPort.text);
            var size = int.Parse(inMapSize.text);
            
            gDef.SetPlayers(players);
            gDef.SetPort(port);
            gDef.SetMapSize(size);
        }

        private bool InputIsValid() {
            {
                if (int.TryParse(inMapSize.text, out var val)) {
                    if (val < 16 || val > 2048) {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }

            {
                if (int.TryParse(inPlayers.text, out var val)) {
                    if (val < 1 || val > 8) {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
        
            {
                if (int.TryParse(inPort.text, out var val)) {
                    if (val < 0 || val > 65535) {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }        

            return true;
        }        

    }

}