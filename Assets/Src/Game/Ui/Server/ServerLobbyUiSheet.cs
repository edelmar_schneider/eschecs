using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Message.Command;
using Ce.Core.Ui;
using Ce.Game.Components.Numeric;
using Ce.Game.Extensions;
using Ce.Game.Message.Command;
using Ce.Game.Models;
using Ce.Game.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ce.Game.Ui.Server {

    public class ServerLobbyUiSheet: BaseUiSheet {
        
        public Button btnReturn;
        public Button btnStart;

        public TMP_Text inMapSize;
        public TMP_Text inPlayers;

        /// <summary>
        /// The element that will be duplicated to represent each player connected via network
        /// </summary>
        public GameObject clientLabelPrefab;

        /// <summary>
        /// The element where the client label objects will be attached to.
        /// </summary>
        public Transform clientLabelList;

        private Group _clientsGroup;

        protected override void OnShow() {
            base.OnShow();
            var aem = Universe.GetModels<AppEntityModels>();
            
            //Let's show to the user the size of the map and how many players are expected
            var gDef = Universe.Entities(aem.GameDefinitionAny).First();
            inMapSize.text = gDef.GetMapSize().ToString(); 
            inPlayers.text = gDef.GetPlayers().ToString();

            _clientsGroup = Universe.RegisterGroup(Group.New(aem.ServerClient))
                                    .OnEntityAdded(UpdateClientsList)
                                    .OnEntityRemoved(UpdateClientsList);

            UpdateClientsList();
        }

        private void UpdateClientsList() {
            
            //Destroy all children that represent connected players
            var children = clientLabelList.GetComponentsInChildren<UiElementScript>();
            foreach (var child in children) {
                Destroy(child.gameObject);    
            }
            
            foreach (var client in _clientsGroup.Entities) {
                var cl = Instantiate(clientLabelPrefab, clientLabelList, true);
                cl.GetComponent<TMP_Text>().text = $"Client ID: {client.GetNetworkClientId()}";
            }
        }

        protected override void OnHide() {
            base.OnHide();
            Universe.UnregisterGroup(_clientsGroup);
        }

        protected override void OnSetup() {
            base.OnSetup();
            
            btnReturn.onClick.AddListener(OnBtnReturnClicked);
            btnStart.onClick.AddListener(OnBtnStartClicked);
        }

        private void OnBtnReturnClicked() {
            //This should make the menu return to its previous state
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            gDef.RemoveMapSize();
            gDef.RemovePlayers();
        }
        
        private void OnBtnStartClicked() {
            
            //From this point on clients and server will be synchronized
            var gDef = Universe.Entities(Universe.GetModels<AppEntityModels>().GameDefinitionAny).First();
            var totalPlayers = gDef.GetPlayers();
            var mapSize = gDef.GetMapSize();
            
            var players = new List<PlayerData>();
            var clients = _clientsGroup.Entities.ToList();

            for (var i = 0; i < totalPlayers; i++) {
                var player = new PlayerData();

                if (clients.Any()) {
                    var client = clients[0];
                    clients.RemoveAt(0);

                    player.NetworkClientId = client.GetNetworkClientId();
                    player.Name = "Human_"+player.NetworkClientId;
                    player.IsAi = false;
                }
                else {
                    player.NetworkClientId = (ulong) i + 100;
                    player.Name = "Ai_"+ player.NetworkClientId;
                    player.IsAi = true;
                }
                
                players.Add(player);
            }
            
            //Reset time to ensure everyone have the same time.
            Universe.Send().Command(new ResetTimeCommand());
            
            //Move to new Game UI
            Universe.Send().Command(new MoveToGroundCommand());
            
            //Create players
            foreach (var p in players) {
                Universe.Send().Command(new CreatePlayerCommand(p.NetworkClientId, p.Name, p.IsAi));
            }
            
            //Create world
            Universe.Send().Command(new CreateBoardCommand(mapSize, players.Count));
            
            //Players can start
            Universe.Send().Command(new BeginGroundCombatCommand());
            
        }
        
        private class PlayerData {

            public ulong NetworkClientId { get; set; }
        
            public string Name { get; set; }
        
            public bool IsAi { get; set; }

        }
    }
}