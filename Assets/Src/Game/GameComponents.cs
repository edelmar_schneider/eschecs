using System;
using System.Collections.Generic;
using Ce.Game.Components.Collection;
using Ce.Game.Components.Custom;
using Ce.Game.Components.Data;
using Ce.Game.Components.Enum;
using Ce.Game.Components.Links;
using Ce.Game.Components.Numeric;
using Ce.Game.Components.String;
using Ce.Game.Components.Tags;
using Ce.Game.Components.Value;

namespace Ce.Game {

    /// <summary>
    /// All components need to be registered here in order to be used.
    /// </summary>
    public static class GameComponents {
        
        public static readonly List<Type> Components = new List<Type> {
            typeof(DisplayNameComponent),
            typeof(MapSizeComponent),
            typeof(PlayersComponent),
            typeof(PortComponent),
            typeof(ClientComponent),
            typeof(GameDefinitionsComponent),
            typeof(ServerComponent),
            typeof(NidComponent),
            typeof(ServerSideComponent),
            typeof(IpComponent),
            typeof(PlayerComponent),
            typeof(GroundCombatComponent),
            typeof(PrefabComponent),
            typeof(GameObjectComponent),
            typeof(GameObjectDisplayComponent),
            typeof(TransformComponent),
            typeof(PropbitsComponent),
            typeof(OnBoardComponent),
            typeof(AddressComponent),
            typeof(ShapeComponent),
            typeof(FootprintComponent),
            typeof(CameraComponent),
            typeof(ActiveCameraComponent),
            typeof(GodToTotemComponent),
            typeof(TotemToGodComponent),
            typeof(PlayerToGodComponent),
            typeof(GodToPlayerComponent),
            typeof(GodComponent),
            typeof(LocalPlayerComponent),
            typeof(GodToServantsComponent),
            typeof(ServantsToGodComponent),
            typeof(GodToAreaComponent),
            typeof(AreaToGodComponent),
            typeof(InteractionModeComponent),
            typeof(SelectableComponent),
            typeof(GodToSelectedComponent),
            typeof(SelectedToGodComponent),
            typeof(ActionPointsComponent),
            typeof(ActionPointsRefuelComponent),
            typeof(PathComponent),
            typeof(ActionComponent),
            typeof(HitPointsComponent),
            typeof(GClassComponent),
            typeof(TurnDoneComponent),
            typeof(PortraitComponent),
            typeof(VictoryStatusComponent),
            typeof(NetworkClientIdComponent),
            
        };
    }
}