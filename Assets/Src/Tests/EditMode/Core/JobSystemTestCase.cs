using System;
using Ce.Core.Ecs;
using NUnit.Framework;
using System.Collections.Generic;
using Ce.Core.System;
using Ce.Core.System.Job;
using Assert = UnityEngine.Assertions.Assert;

namespace Ce.EditTests.Core {

    [TestFixture]
    public class JobSystemTestCase {

        private Universe _universe;
        private TimeSystem _timeSystem;
        private JobSystem _jobSystem;

        [SetUp]
        public void Setup() {
            _universe = new Universe(new List<Type> {
                typeof(NameComponent),
                typeof(AgeComponent),
                typeof(ActiveComponent),
            });

            _universe.RegisterSystem<TimeSystem>()
                     .RegisterSystem<MessageSystem>();

            _universe.RegisterSystem<JobSystem>();

            _timeSystem = _universe.GetSystem<TimeSystem>();
            _jobSystem = _universe.GetSystem<JobSystem>();
        }

        [TearDown]
        public void Teardown() {
            _universe.Terminate();
            _universe = null;
            _timeSystem = null;
        }

        [Test]
        public void JobActivationTermination() {
            var name = string.Empty;

            //Ensure there are no jobs
            Assert.AreEqual(0, _jobSystem.Jobs.Length);
            //Create a job to set the name when activated and then terminate
            _universe.RegisterJob<LambdaJob>()
                     .OnActivate(job => {
                                     name = "Test";
                                     job.TriggerAfter(JobTrigger.Success);
                                 });

            //Ensure the job got added to the correct system 
            Assert.AreEqual(1, _jobSystem.Jobs.Length);

            //Verify that the name is not set yet
            Assert.AreEqual(string.Empty, name);

            //Update should cause the job to run
            _timeSystem.FixedUpdate();
            Assert.AreEqual("Test", name);

            //And not the job has to have been eliminated
            _timeSystem.FixedUpdate();
            Assert.AreEqual(0, _jobSystem.Jobs.Length);
        }

        [Test]
        public void JobQueueTask() {
            var name = string.Empty;

            //Let's create a job with a delay of 3 ticks
            _universe.QueueTask(() => { name = "Test"; }, 3);

            //Verify that the name is not set yet
            Assert.AreEqual(string.Empty, name);

            _timeSystem.FixedUpdate();
            //Name should not be set yet
            Assert.AreEqual(string.Empty, name);

            _timeSystem.FixedUpdate();
            //Name should not be set yet
            Assert.AreEqual(string.Empty, name);

            _timeSystem.FixedUpdate();
            //Name should be set now
            Assert.AreEqual("Test", name);

            Assert.AreEqual(0, _jobSystem.Jobs.Length);
        }
        
        [Test]
        public void JobQueueTaskNoDelay() {
            var name = string.Empty;

            //Let's create a job with a delay of 3 ticks
            _universe.QueueTask(() => { name = "Test"; }, 0);

            //Verify that the name is not set yet
            Assert.AreEqual(string.Empty, name);

            _timeSystem.FixedUpdate();
            
            //Name should be set now
            Assert.AreEqual("Test", name);

            Assert.AreEqual(0, _jobSystem.Jobs.Length);

        }
    }

}