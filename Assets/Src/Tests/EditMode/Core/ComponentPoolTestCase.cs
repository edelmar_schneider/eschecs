using Ce.Core.Util;
using NUnit.Framework;

namespace Ce.EditTests.Core {

    public class ComponentPoolTestCase {

        private Pool<NameComponent> _componentPool;
        
        [SetUp]
        public void Setup() {
            _componentPool = new Pool<NameComponent>(() => new NameComponent());
        }

        [TearDown]
        public void TearDown() {
            _componentPool = null;
        }

        [Test]
        public void InstantiateReturnComponents() {
            
            var a = _componentPool.Acquire();
            var b = _componentPool.Acquire();
            
            Assert.AreNotEqual(null, a);
            Assert.AreNotEqual(null, b);
            
            //Pool should be empty
            Assert.AreEqual(0, _componentPool.Count);
            
            //Returned one to the pool
            _componentPool.Recycle(a);
            Assert.AreEqual(1, _componentPool.Count);
            
            //Returned two to the pool
            _componentPool.Recycle(b);
            Assert.AreEqual(2, _componentPool.Count);
            
            //Picked one from the pool
            a = _componentPool.Acquire();
            Assert.AreEqual(1, _componentPool.Count);
            
            //Picked two from the pool
            b = _componentPool.Acquire();
            Assert.AreEqual(0, _componentPool.Count);
        }
    }

}