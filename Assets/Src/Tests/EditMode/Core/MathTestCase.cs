using System;
using Ce.Core.Ecs;
using Ce.Core.Util;
using NUnit.Framework;
using System.Collections.Generic;

namespace Ce.EditTests.Core {

    [TestFixture]
    public class MathTestCase {
        
        private Universe _universe; 

        [SetUp]
        public void Setup() {
            _universe = new Universe(new List<Type> {
                typeof(NameComponent),
                typeof(AgeComponent),
                typeof(ActiveComponent),
            });
        }

        [TearDown]
        public void Teardown() {
            _universe.Terminate();
            _universe = null;
        }


        [Test]
        public void TestAllMatcher() {
            var mat = Matcher.New(_universe).All<NameComponent, AgeComponent>();
            
            //Exact match
            Assert.IsTrue(mat.Match(new[] {typeof(NameComponent), typeof(AgeComponent)}));
            //More components than the match set but that is ok. It's expected that the entity will have many more.
            Assert.IsTrue(mat.Match(new[] {typeof(NameComponent), typeof(AgeComponent), typeof(ActiveComponent)}));
            //Too few
            Assert.IsFalse(mat.Match(new[] {typeof(NameComponent)}));
            //Does not contain
            Assert.IsFalse(mat.Match(new[] {typeof(ActiveComponent)}));
        }
        
        [Test]
        public void TestAnyMatcher() {
            var mat = Matcher.New(_universe).Any<NameComponent, AgeComponent>();
            
            //Exact match
            Assert.IsTrue(mat.Match(new[] {typeof(NameComponent), typeof(AgeComponent)}));
            //More components than the match set but that is ok. It's expected that the entity will have many more.
            Assert.IsTrue(mat.Match(new[] {typeof(NameComponent), typeof(AgeComponent), typeof(ActiveComponent)}));
            //Only ony, should be ok
            Assert.IsTrue(mat.Match(new[] {typeof(NameComponent)}));
            //Only ony, should be ok
            Assert.IsTrue(mat.Match(new[] {typeof(AgeComponent)}));
            //Does not contain
            Assert.IsFalse(mat.Match(new[] {typeof(ActiveComponent)}));
        }
        
        [Test]
        public void TestNoneMatcher() {
            var mat = Matcher.New(_universe).None<NameComponent, AgeComponent>();
            
            //All components that I shouldn't have
            Assert.IsFalse(mat.Match(new[] {typeof(NameComponent), typeof(AgeComponent)}));
            //All components that I shouldn't have and more.
            Assert.IsFalse(mat.Match(new[] {typeof(NameComponent), typeof(AgeComponent), typeof(ActiveComponent)}));
            //Only one that I shouldn't have
            Assert.IsFalse(mat.Match(new[] {typeof(NameComponent)}));
            //Only one that I shouldn't have
            Assert.IsFalse(mat.Match(new[] {typeof(AgeComponent)}));
            //This should be ok
            Assert.IsTrue(mat.Match(new[] {typeof(ActiveComponent)}));
        }
        
        [Test]
        public void TestAllAndNoneMatcher() {
            var mat = Matcher.New(_universe)
                             .All<NameComponent>()
                             .None<ActiveComponent>();
            
            //All components that I should have and none that I shouldn't
            Assert.IsTrue(mat.Match(new[] {typeof(NameComponent), typeof(AgeComponent)}));
            //All components that I should have and one that I shouldn't
            Assert.IsFalse(mat.Match(new[] {typeof(NameComponent), typeof(AgeComponent), typeof(ActiveComponent)}));
            //Only one that I should have
            Assert.IsTrue(mat.Match(new[] {typeof(NameComponent)}));
            //None components that I should have and none that I shouldn't have.
            Assert.IsFalse(mat.Match(new[] {typeof(AgeComponent)}));
            //Only components that I shouldn't have
            Assert.IsFalse(mat.Match(new[] {typeof(ActiveComponent)}));
        }
    }
}