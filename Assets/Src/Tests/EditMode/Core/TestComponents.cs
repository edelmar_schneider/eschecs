using System;
using System.Xml.Linq;
using Ce.Core.Ecs;

namespace Ce.EditTests.Core {
        
    public class NameComponent : IComponent {
            
        public string Value { get; set; }
        public bool IsSerializable => false;
        public XElement ToXml(string name) => throw new NotImplementedException();

        public void FromXml(XElement element) { throw new NotImplementedException(); }

        public NameComponent() { }
        public NameComponent(string name) => Value = name;

        public IComponent Default() => new NameComponent();

        public object DeepCopy() => new NameComponent(string.Copy(Value));

        public void CleanUp() {
            Value = null;
        }
    }
    
    public static class NameComponentHelpers {
        public static Entity SetName(this Entity e, string name) {
            var u = e.Universe;
            var c = u.AcquireComponent<NameComponent>();
            c.Value = name;
            e.Update(c);
            return e;
        }

        public static string GetName(this Entity e) => e.Get<NameComponent>().Value;

        public static bool HasName(this Entity e) => e.Has<NameComponent>();

        public static Entity RemoveName(this Entity e) => e.Remove<NameComponent>();

    }

        
    public class AgeComponent : IComponent {
        
        
        public bool IsSerializable => false;
        public XElement ToXml(string name) => throw new NotImplementedException();

        public void FromXml(XElement element) { throw new NotImplementedException(); }
        
        public int Value { get; set; }

        public AgeComponent() { }
        public AgeComponent(int age) => Value = age;
        
        public IComponent Default() => new AgeComponent();

        public object DeepCopy() => new AgeComponent(Value);
        
        public void CleanUp(){}
    }
    
    public static class AgeComponentHelpers {
        public static Entity SetAge(this Entity e, int age) {
            var u = e.Universe;
            var c = u.AcquireComponent<AgeComponent>();
            c.Value = age;
            e.Update(c);
            return e;
        }
        
        public static int GetAge(this Entity e) => e.Get<AgeComponent>().Value;

        public static bool HasAge(this Entity e) => e.Has<AgeComponent>();

        public static Entity RemoveAge(this Entity e) => e.Remove<AgeComponent>();
    }
        
    public class ActiveComponent : IComponent {
        public bool IsSerializable => false;
        public XElement ToXml(string name) => throw new NotImplementedException();

        public void FromXml(XElement element) { throw new NotImplementedException(); }
        public IComponent Default() => new ActiveComponent();
        
        public object DeepCopy() => new ActiveComponent();
        public void CleanUp(){}
    }
    
    public static class ActiveComponentHelpers {
        public static Entity SetActive(this Entity e) {
            var u = e.Universe;
            var c = u.AcquireComponent<ActiveComponent>();
            e.Update(c);
            return e;
        }

        public static bool HasActive(this Entity e) => e.Has<ActiveComponent>();
        
        public static Entity RemoveActive(this Entity e) => e.Remove<ActiveComponent>();
    }
    

}