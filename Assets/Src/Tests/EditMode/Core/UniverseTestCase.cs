using System;
using System.Collections.Generic;
using Ce.Core.Ecs;
using NUnit.Framework;

namespace Ce.EditTests.Core {

    [TestFixture]
    public class UniverseTestCase {

        private Universe _universe; 

        [SetUp]
        public void Setup() {
            _universe = new Universe(new List<Type> {
                typeof(NameComponent),
                typeof(AgeComponent),
                typeof(ActiveComponent),
            });
        }

        [TearDown]
        public void Teardown() {
            _universe.Terminate();
            _universe = null;
        }

        [Test]
        public void CreateEntity() {
            //Create empty entity
            var e = _universe.AcquireEntity();
            
            //Ensure that the Rid is zero
            Assert.AreEqual(0, e.Rid);
            
            //Add components
            e.SetName("TestName");
            e.SetActive();
            
            //Check if the components exist
            Assert.IsTrue(e.HasName());
            Assert.IsTrue(e.HasActive());
            
            //Check if some components do not exist
            Assert.IsFalse(e.HasAge());
            
            //Get the component directly
            Assert.IsNotNull(e.Get<NameComponent>());
            
            //Check if the value is correct
            Assert.AreEqual("TestName", e.GetName());
            
            //Remove the component
            e.RemoveName();
                        
            //Check if the component got removed
            Assert.IsFalse(e.HasName());
            
            //Check if the other component is still there
            Assert.IsTrue(e.HasActive());
            
            //Recycle entity, should remove components and reset the Rid
            _universe.RecycleEntity(e);
            
            //Ensure that the Rid has changed to one
            Assert.AreEqual(1, e.Rid);
            
            //And that it has no components
            Assert.AreEqual(0, e.ComponentCount);
            
            //Now let's ask to get a new Entity, we should get the same from before
            e = _universe.AcquireEntity();
            
            //Ensure that the Rid is one
            Assert.AreEqual(1, e.Rid);

        }
    }
}