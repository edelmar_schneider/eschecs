using System;
using System.Collections.Generic;
using Ce.Core;
using Ce.Core.Ecs;
using Ce.Core.Util;
using NUnit.Framework;

namespace Ce.EditTests.Core {

    public class EntityPoolTestCase {

        private Universe _universe;
        private Pool<Entity> _entityPool;
        
        [SetUp]
        public void Setup() {
            _universe = new Universe(new List<Type> {
                typeof(NameComponent),
                typeof(AgeComponent),
                typeof(ActiveComponent),
            });
            _entityPool = new Pool<Entity>(CoreConstants.Entities, () => new Entity(_universe.GenerateNewEntityRid(), _universe));
        }

        [TearDown]
        public void TearDown() {
            _entityPool = null;
            _universe.Terminate();
            _universe = null;
        }

        [Test]
        public void InstantiateReturnComponents() {
            
            var a = _entityPool.Acquire();
            var b = _entityPool.Acquire();
            
            Assert.AreNotEqual(null, a);
            Assert.AreNotEqual(null, b);
            
            //Check the Rids of the entity
            Assert.AreEqual(0, a.Rid);
            Assert.AreEqual(1, b.Rid);
            
            //Pool should be empty
            Assert.AreEqual(0, _entityPool.Count);
            
            //Returned one to the pool
            _entityPool.Recycle(a);
            Assert.AreEqual(1, _entityPool.Count);
            
            //Returned two to the pool
            _entityPool.Recycle(b);
            Assert.AreEqual(2, _entityPool.Count);
            
            //Picked one from the pool
            a = _entityPool.Acquire();
            Assert.AreEqual(1, _entityPool.Count);
            
            //Picked two from the pool
            b = _entityPool.Acquire();
            Assert.AreEqual(0, _entityPool.Count);
            
            //Check the Rids of the entity
            Assert.AreEqual(3, a.Rid);
            Assert.AreEqual(2, b.Rid);
            
            
        }
    }

}