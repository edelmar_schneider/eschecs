using System;
using Ce.Core.Ecs;
using System.Collections.Generic;
using NUnit.Framework;

namespace Ce.EditTests.Core {

    [TestFixture]
    public class EntityTestCase {

        private Universe _universe; 

        [SetUp]
        public void Setup() {
            _universe = new Universe(new List<Type> {
                typeof(NameComponent),
                typeof(AgeComponent),
                typeof(ActiveComponent),
            });
        }

        [TearDown]
        public void Teardown() {
            _universe.Terminate();
            _universe = null;
        }
        
        
        
    }

}