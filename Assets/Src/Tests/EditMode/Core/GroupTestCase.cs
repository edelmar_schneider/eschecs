using System;
using Ce.Core.Ecs;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Ce.Core.Util;

namespace Ce.EditTests.Core {

    [TestFixture]
    public class GroupTestCase {
        
        private Universe _universe; 

        [SetUp]
        public void Setup() {
            _universe = new Universe(new List<Type> {
                typeof(NameComponent),
                typeof(AgeComponent),
                typeof(ActiveComponent),
            });
        }

        [TearDown]
        public void Teardown() {
            _universe.Terminate();
            _universe = null;
        }

        [Test]
        public void OnAddRemoveToTheGroup() {

            Entity addedEntity = null;
            Entity removedEntity = null;
            var g = Group.New(Matcher.New(_universe).All<NameComponent, AgeComponent>());
            _universe.RegisterGroup(g);

            //The entity should be added here
            g.OnEntityAdded((group, entity) => { addedEntity = entity;})
             .OnEntityRemoved((group, entity) => { removedEntity = entity;});
                
            var e = _universe
                    .AcquireEntity()
                    .SetName("TestName")
                    .SetAge(33);
            
            //The entity should be part of the group now.
            var eg = g.Entities.FirstOrDefault();
            
            Assert.IsNotNull(eg);
            Assert.IsNotNull(addedEntity);
            Assert.AreEqual(eg,addedEntity);
            Assert.AreEqual(eg,e);
            //And the removed entity must be null, because it hasn't got called yet.
            Assert.IsNull(removedEntity);

            //But now if I remove the component the entity should be removed from the group
            e.Remove<NameComponent>();
            
            //And the OnEntityRemoved method must have been called.
            Assert.IsNotNull(removedEntity);
            
            //Just to be sure let us check whether the entity got removed from the group.
            Assert.IsNull(g.Entities.FirstOrDefault());
        }
        
        [Test]
        public void OnAddRemoveComponent() {
            
            var g = Group.New(Matcher.New(_universe).All<NameComponent, AgeComponent>());
            _universe.RegisterGroup(g);

            //Component received from the call back functions
            ActiveComponent ac = null;
            NameComponent nc = null;
            AgeComponent agc = null;

            g.OnComponentAdded<ActiveComponent>((group, entity, comp) => { ac = comp; })
             .OnComponentAdded<NameComponent>((group, entity, comp) => { nc = comp; })
             .OnComponentAdded<AgeComponent>((group, entity, comp) => { agc = comp; });

            var e = _universe
                    .AcquireEntity()
                    .SetName("TestName")
                    .SetAge(33)
                    .SetActive();

            //For this first entity it should be added to the group and then ActiveComponent is added.
            //Only OnComponentAdded<ActiveComponent> should be called.
            Assert.IsNotNull(ac);
            Assert.IsNull(nc);
            Assert.IsNull(agc);
            
            //Now I will remove the components
            //ActiveComponent Will be removed before the others so the call back function should be called for it
            //the other functions should not be called
            ac = null;
            nc = null;
            agc = null;
            
            g.OnComponentRemove<ActiveComponent>((group, entity, comp) => { ac = comp; })
             .OnComponentRemove<NameComponent>((group, entity, comp) => { nc = comp; })
             .OnComponentRemove<AgeComponent>((group, entity, comp) => { agc = comp; });

            e.RemoveActive();
            
            Assert.IsNotNull(ac);
            Assert.IsNull(nc);
            Assert.IsNull(agc);
            
            //Now I'll make sure that the entity is still there.
            Assert.NotNull(g.Entities.FirstOrDefault());
            
            //And then I'll remove the NameComponent. The Entity should be removed from the group 
            //before OnComponentRemove<NameComponent> is called

            e.RemoveName();
            
            //Lets make sure it hasn't got called
            Assert.IsNull(nc);
            
            //And make sure the entity got removed from the group.
            Assert.IsNull(g.Entities.FirstOrDefault());

        }

        [Test]
        public void OnComponentUpdate() {
            var g = Group.New(Matcher.New(_universe).All<NameComponent, AgeComponent>());
            _universe.RegisterGroup(g);

            //Component received from the call back functions
            NameComponent ncN = null;
            NameComponent ncO = null;
            
            g.OnComponentUpdated<NameComponent>((group, entity, oldComp, newComp) => {
                                                    ncN = newComp;
                                                    ncO = (NameComponent)oldComp.DeepCopy();
                                                });
            
            //Will add now the components, the entity should be added to the group but the update function should not be called
            var e = _universe
                    .AcquireEntity()
                    .SetName("TestName")
                    .SetAge(33)
                    .SetActive();
            
            Assert.IsNull(ncN);
            Assert.IsNull(ncO);
            
            //But the component and the values should be there
            Assert.IsTrue(e.HasName());
            Assert.AreEqual("TestName", e.GetName());
            
            //Now I will update the name, the functions should be triggered.
            e.SetName("NewName");
            
            //Now the old component must be set with a copy 
            //And the new component should have the new name.
            Assert.IsNotNull(ncO);
            Assert.IsNotNull(ncN);
            Assert.IsTrue(e.HasName());
            Assert.AreEqual("NewName", e.GetName());
            Assert.AreEqual("TestName", ncO.Value);
            Assert.AreEqual("NewName", ncN.Value);
        }
    }
}