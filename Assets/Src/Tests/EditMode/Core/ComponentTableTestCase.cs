using System.Linq;
using Ce.Core.Ecs;
using NUnit.Framework;

namespace Ce.EditTests.Core {

    [TestFixture]
    public class ComponentTableTestCase {

        private ComponentTable _ct;

        [SetUp]
        public void Setup() {
            _ct = new ComponentTable {
                typeof(NameComponent),
                typeof(AgeComponent),
            };
        }

        [TearDown]
        public void Teardown() {
            _ct = null;
        }
        
        [Test]
        public void AddComponentsInConstruction() {
            var totalAdded = _ct.Count();
            Assert.AreEqual(2, totalAdded);
        }
        
        [Test]
        public void AddComponentsAfterConstruction() {
            _ct.Add(typeof(ActiveComponent));
            var totalAdded = _ct.Count();
            Assert.AreEqual(3, totalAdded);
        }
        
        [Test]
        public void AddComponentsAvoidDuplicationAndSidChange() {
            _ct.Add(typeof(AgeComponent));
            var totalAdded = _ct.Count();
            Assert.AreEqual(2, totalAdded);
        }
        
        [Test]
        public void FindComponentSid() {
            _ct.Add(typeof(ActiveComponent));

            var caSid = _ct.FindComponentSid<NameComponent>();
            var cbSid = _ct.FindComponentSid(typeof(AgeComponent));
            
            Assert.AreEqual(0, caSid);
            Assert.AreEqual(1, cbSid);
        }
        
        [Test]
        public void FindComponentType() {
            _ct.Add(typeof(ActiveComponent));

            var caType = _ct.FindComponentType(0);
            var ccType = _ct.FindComponentType(2);
            
            Assert.AreEqual(typeof(NameComponent), caType);
            Assert.AreEqual(typeof(ActiveComponent), ccType);
        }
    }

}