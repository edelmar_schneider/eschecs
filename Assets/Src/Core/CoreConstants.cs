namespace Ce.Core {

    public static class CoreConstants {

        /// <summary>
        /// The max number of components expected, this number can be raised manually if needed
        /// Try to keep it as low as possible for performance.
        /// </summary>
        public static int Components = 512;

        /// <summary>
        /// The max number of entities expected, this number can be raised manually if needed
        /// Try to keep it as low as possible for performance.
        /// </summary>
        public static int Entities = 32768;

        /// <summary>
        /// Default size of a pool
        /// </summary>
        public static int PoolSize = 128;
        
        /// <summary>
        /// Seconds per tick
        /// </summary>
        public static float SecondsPerTick = 0.0133349335253564f; //74.991 Hz

        /// <summary>
        /// Server adds this amount of ticks to commands before sending them to clients.
        /// </summary>
        public static ulong TicksOfDelay = 7;

    }

}