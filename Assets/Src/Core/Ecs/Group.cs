using System;
using System.Collections;
using System.Collections.Generic;
using Ce.Core.Util;

namespace Ce.Core.Ecs {

    /// <summary>
    /// A group keeps a collection of entities that are grouped based on the components that they have.
    /// A group allows you to defined methods to be called when entities are added, removed or have their
    /// components updated.
    /// Groups are one of the driving force of this ECS system
    /// </summary>
    public sealed class Group : IEnumerable<Entity> {

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<Entity> GetEnumerator() => _entities.GetEnumerator();

        public string Name { get; set; } = "Undefined";
        
        /// <summary>
        /// All entities of this group. All passed the matcher test.
        /// </summary>
        private readonly HashSet<Entity> _entities = new HashSet<Entity>();

        /// <summary>
        /// The object that decides if an entity stays or are rejected/removed
        /// </summary>
        private readonly Matcher _matcher;

        /// <summary>
        /// The functions registered here will be called when an entity gets added to this group.
        /// </summary>
        private event Action<Group, Entity> _entityAdded;
        
        /// <summary>
        /// The functions registered here will be called when an entity gets removed from this group.
        /// </summary>
        private event Action<Group, Entity> _entityRemoved;
        
        /// <summary>
        /// The functions registered here will be called when the registered component on the entity in the group gets updated.
        /// Not using a dictionary because I can have multiple functions registered here for the same type of component
        /// </summary>
        private readonly List<(Type/*component type*/, Action<Group/*this group*/, Entity/*Target entity*/, IComponent/*Old component*/, IComponent/*new component*/>)> _componentUpdateFunctions = new List<(Type, Action<Group, Entity, IComponent, IComponent>)>();
        
        /// <summary>
        /// The functions registered here will be called when the target component gets added to the entity.
        /// The component should not be part of the matcher otherwise it won't be called. 
        /// Not using a dictionary because I can have multiple functions registered here for the same type of component
        /// </summary>        
        private readonly List<(Type/*component type*/, Action<Group/*this group*/, Entity/*Target entity*/, IComponent/*new component*/>)> _componentAddFunctions = new List<(Type, Action<Group, Entity, IComponent>)>();
        
        /// <summary>
        /// The functions registered here will be called when the target component gets removed from the entity.
        /// The component should not be part of the matcher otherwise it won't be called. 
        /// Not using a dictionary because I can have multiple functions registered here for the same type of component
        /// </summary>
        private readonly List<(Type/*component type*/, Action<Group/*this group*/, Entity/*Target entity*/, IComponent/*old component*/>)> _componentRemoveFunctions = new List<(Type, Action<Group, Entity, IComponent>)>();        
        
        /// <summary>
        /// Access the entities of this group.
        /// </summary>
        public IEnumerable<Entity> Entities => _entities;
        
        private Group(Matcher matcher) {
            _matcher = matcher;
        }
        
        /// <summary>
        /// Create a new group
        /// </summary>
        public static Group New(Matcher matcher) => new Group(matcher);
        
        /// <summary>
        /// Will check, remove or keep if the Entity fits the bag, or doesn't fit anymore. 
        /// </summary>
        public void EvaluateEntity(Entity entity) {
            
            var match = entity.DoesMatchWithComponents(_matcher);

            if (match) {
                if (_entities.Add(entity)) {
                    _entityAdded?.Invoke(this, entity);
                }
            }
            else {
                if (_entities.Remove(entity)) {
                    _entityRemoved?.Invoke(this, entity);
                }
            }
        }
        
        
        /// <summary>
        /// Call this when the Entity gets one of its components changed.
        /// </summary>
        public void EntityComponentUpdate(Entity entity, Type componentType, IComponent oldComponent, IComponent newComponent)
        {
            //Check if I'm actually following this guy
            if (!_entities.Contains(entity)) {
                return;
            }

            foreach (var (type, action) in _componentUpdateFunctions) {
                if (type == componentType) {
                    action.Invoke(this, entity, oldComponent, newComponent);
                }
            }
        }
                
        /// <summary>
        /// Call this when the Entity gets a new component added.
        /// </summary>
        public void EntityComponentAdded(Entity entity, Type componentType, IComponent newComponent)
        {
            //Check if I'm actually following this guy
            if (!_entities.Contains(entity)) {
                return;
            }

            foreach (var (type, action) in _componentAddFunctions) {
                if (type == componentType) {
                    action.Invoke(this, entity, newComponent);
                }
            }
        }
        
        /// <summary>
        /// Call this when the Entity gets a component removed.
        /// </summary>
        public void EntityComponentRemoved(Entity entity, Type componentType, IComponent oldComponent)
        {
            //Check if I'm actually following this guy
            if (!_entities.Contains(entity)) {
                return;
            }

            foreach (var (type, action) in _componentRemoveFunctions) {
                if (type == componentType) {
                    action.Invoke(this, entity, oldComponent);
                }
            }
        }        
        
        /// <summary>
        /// Register an action to be called when an entity gets added to this group
        /// </summary>
        public Group OnEntityAdded(Action<Group, Entity> onAdd) {
            _entityAdded += onAdd;
            return this;
        }
        
        /// <summary>
        /// Register an action to be called when an entity gets added to this group
        /// </summary>
        public Group OnEntityAdded(Action<Entity> onAdd) {
            _entityAdded += (family, entity) => onAdd(entity);
            return this;
        }

        /// <summary>
        /// Register an action to be called when an entity gets added to this group
        /// </summary>
        public Group OnEntityAdded(Action onAdd) {
            _entityAdded += (family, entity) => onAdd();
            return this;
        }

        /// <summary>
        /// Register an action to be called when an entity gets removed from this group
        /// </summary>
        public Group OnEntityRemoved(Action<Group, Entity> onRemove) {
            _entityRemoved += onRemove;
            return this;
        }

        /// <summary>
        /// Register an action to be called when an entity gets removed from this group
        /// </summary>
        public Group OnEntityRemoved(Action<Entity> onRemove) {
            _entityRemoved += (family, entity) => onRemove(entity);
            return this;
        }
        
        /// <summary>
        /// Register an action to be called when an entity gets removed from this group
        /// </summary>
        public Group OnEntityRemoved(Action onRemove) {
            _entityRemoved += (family, entity) => onRemove();
            return this;
        }

        /// <summary>
        /// The functions registered here will be called when the registered component on the entity in the group gets updated.
        /// The old component will be cleaned up and recycled right after the call.
        /// </summary>
        public Group OnComponentUpdated(Type componentType,
                                        Action<
                                            Group /*this group*/, 
                                            Entity /*Target entity*/, 
                                            IComponent /*Old component*/, 
                                            IComponent /*new component*/
                                        > onUpdate) {
            _componentUpdateFunctions.Add((componentType, onUpdate));
            return this;
        }
        
        /// <summary>
        /// The functions registered here will be called when the registered component on the entity in the group gets updated.
        /// The old component will be cleaned up and recycled right after the call.
        /// </summary>
        public Group OnComponentUpdated<T> (
                                        Action<
                                            Group /*this group*/, 
                                            Entity /*Target entity*/, 
                                            T /*Old component*/, 
                                            T /*new component*/
                                        > onUpdate) where T: IComponent {

            var componentType = typeof(T);
            _componentUpdateFunctions.Add((componentType, (g, e, oc, nc) => onUpdate(g, e, (T) oc, (T) nc)));

            return this;
        }
        
        /// <summary>
        /// The functions registered here will be called when the target component gets added to the entity.
        /// The component should not be part of the matcher otherwise it won't be called.
        /// </summary>     
        public Group OnComponentAdded(Type componentType,
                                        Action<
                                            Group /*this group*/, 
                                            Entity /*Target entity*/,
                                      
                                            IComponent /*new component*/
                                        > onAdded) {
            _componentAddFunctions.Add((componentType, onAdded));
            return this;
        }
        
        /// <summary>
        /// The functions registered here will be called when the target component gets added to the entity.
        /// The component should not be part of the matcher otherwise it won't be called.
        /// </summary>     
        public Group OnComponentAdded<T> (
            Action<
                Group /*this group*/, 
                Entity /*Target entity*/,
                T /*new component*/
            > onRemoved) where T: IComponent {

            var componentType = typeof(T);
            _componentAddFunctions.Add((componentType, (g, e, c) => onRemoved(g, e, (T) c)));

            return this;
        }
        
        /// <summary>
        /// The functions registered here will be called when the target component gets removed from the entity.
        /// The component should not be part of the matcher otherwise it won't be called.
        /// <remarks>Check if the Entity is Active when responding to this, if the Entity is not active it is because it's being recycled. </remarks>
        /// </summary>     
        public Group OnComponentRemove(Type componentType,
                                      Action<
                                          Group /*this group*/, 
                                          Entity /*Target entity*/,
                                          IComponent /*old component*/
                                      > onRemoved) {
            _componentRemoveFunctions.Add((componentType, onRemoved));
            return this;
        }
        
        /// <summary>
        /// The functions registered here will be called when the target component gets removed from the entity.
        /// The component should not be part of the matcher otherwise it won't be called.
        /// <remarks>Check if the Entity is Active when responding to this, if the Entity is not active it is because it's being recycled. </remarks>
        /// </summary>     
        public Group OnComponentRemove<T> (
            Action<
                Group /*this group*/, 
                Entity /*Target entity*/,
                T /*old component*/
            > onRemoved) where T: IComponent {

            var componentType = typeof(T);
            _componentRemoveFunctions.Add((componentType, (g, e , c ) => onRemoved(g,e,(T)c)));

            return this;
        }

        public override string ToString() {
            return $"Group: {Name}";
        }
    }
}