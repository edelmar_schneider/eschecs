using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Ce.Core.Ecs {

    public class ComponentTable : IEnumerable<Type> {

        /// <summary>
        /// Table to relate a component type to an unique id (Static Id)
        /// </summary>
        private readonly Dictionary<Type, int> _typeToSid = new Dictionary<Type, int>(CoreConstants.Components);
        
        /// <summary>
        /// Table to relate a unique ID (Static id) to a component type
        /// </summary>
        private readonly Type[] _sidToType = new Type[CoreConstants.Components];

        /// <summary>
        /// Find the type of a component by its Sid
        /// </summary>
        /// <param name="sid">Static Id</param>
        public Type FindComponentType(int sid) => _sidToType[sid];

        /// <summary>
        /// Fund the Sid of a component by its type.
        /// </summary>
        /// <param name="componentType">A type of component</param>
        public int FindComponentSid(Type componentType) {

            if (!_typeToSid.ContainsKey(componentType)) {
                Debug.LogError($"Component not registered: {componentType.Name}");
            }
          
            return _typeToSid[componentType];
        } 
        
        /// <summary>
        /// Find the Sid of a Component.
        /// </summary>
        public int FindComponentSid<T>() => _typeToSid[typeof(T)];

        /// <summary>
        /// Add a component to the table.
        /// </summary>
        public ComponentTable Add(Type componentType) => Add(componentType, _typeToSid.Count);

        /// <summary>
        /// Add a component to the table.
        /// </summary>
        public ComponentTable Add(Type componentType, int componentSid) {
            if(_typeToSid.ContainsKey(componentType)) {
                //Avoid sid change
                return this;
            }

            if (_typeToSid.Count == CoreConstants.Components) {
                throw new Exception("Component table is full, increase capacity ");
            }
            
            _typeToSid[componentType] = componentSid;
            _sidToType[componentSid] = componentType;

            return this;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<Type> GetEnumerator() => _typeToSid.Keys.GetEnumerator();

    }

}