using System.Collections.Generic;
using Ce.Core.Util;

namespace Ce.Core.Ecs {

    /// <summary>
    /// This is the base class for all systems in the ECS.
    /// </summary>
    public abstract class BaseSystem {

        public Universe Universe;

        /// <summary>
        /// Register the groups here so they will be disposed automatically when the module is terminated.
        /// </summary>
        private readonly List<Group> _groups = new List<Group>();

        protected BaseSystem(Universe universe) => Universe = universe;

        protected BaseSystem() { }

        /// <summary>
        /// This is the best method to register a group, because the group will be unregistered automatically when the system is termianted 
        /// </summary>
        protected Group RegisterGroup(Matcher matcher) {
            var group = Group.New(matcher);
            Universe.RegisterGroup(group);
            _groups.Add(group);
            return group;
        }
        
        protected Group RegisterGroup(Matcher matcher, string name) {
            var group = Group.New(matcher);
            Universe.RegisterGroup(group, name);
            _groups.Add(group);
            return group;
        }

        private void UnregisterAllGroups() {
            foreach (var group in _groups) {
                Universe.UnregisterGroup(group);
            }
        }
        
        /// <summary>
        /// After this function is called the system is considered ready to function.
        /// <see cref="OnInitialize"/>
        /// <see cref="Terminate"/>
        /// </summary>
        public void Initialize(Universe universe) {
            Universe = universe;
            OnInitialize();
        }
        
        /// <summary>
        /// Perform the initialization tasks. Register groups and events.
        /// </summary>
        protected virtual void OnInitialize()
        {
            
        }

        /// <summary>
        /// This should be called to give the system an opportunity to unregister itself
        /// </summary>
        public void Terminate()
        {
            OnTerminate();

            UnregisterAllGroups();

            Universe = null;
        }

        protected virtual void OnTerminate()
        {
            
        }        
        
    }

}