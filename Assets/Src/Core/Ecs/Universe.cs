using System;
using System.Collections.Generic;
using System.Linq;
using Ce.Core.Util;
using Ce.Game.World;
using Random = System.Random;

namespace Ce.Core.Ecs {

    public sealed class Universe {
        
        /// <summary>
        /// The Random object is set when the map is loaded and the seed is set by the article that keeps the seed on the map.
        /// </summary>
        public Random Rand { get; set; }
        
        /// <summary>
        /// This is a reference to the table param of the game
        /// The table params is cached here because of its importance to the game.
        /// </summary>
        public BoardParam Param { get; set; }
        
        public ComponentTable ComponentTable { get; private set; }

        private ulong NextEntityRid { get; set; }
        private ulong NextEntityNid { get; set; }

        /// <summary>
        /// Entities that are clean (and have a new Rid) and awaiting to be reused.
        /// </summary>
        private Pool<Entity> _entityPool;

        /// <summary>
        /// Components that are clean and awaiting to be reused.
        /// </summary>
        private Dictionary<Type, Pool<IComponent>> _componentPoolsByType;
        
        /// <summary>
        /// Components that are clean and awaiting to be reused.
        /// </summary>
        private Pool<IComponent>[] _componentPoolsBySid;
        
        /// <summary>
        /// All registered articles in this universe.
        /// </summary>
        private ISet<Entity> _entities;

        /// <summary>
        /// All registered groups
        /// </summary>
        private LinkedList<Group> _groups;

        
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, Group> _namedGroups;
            
        /// <summary>
        /// Systems are the working horse of the game and all of them are registered here.
        /// </summary>
        private Dictionary<Type, BaseSystem> _systems;

        public bool Initialized { get; private set; }

        /// <summary>
        /// All the components this universe knows about
        /// </summary>
        public readonly List<Type> Components;
        
        private readonly Dictionary<Type, IModels> _models = new Dictionary<Type, IModels>();
        
        /// <summary>
        /// To ensure that the behaviour is predictable groups are not added during any change to the groups themselves
        /// In other words, if you create a new groups during a group call back function it won't be added until the
        /// all other groups have been notified of the last update.
        /// Note. The same doesn't happen if you remove a group, this is done instantly.
        /// </summary>
        private bool _lockedToGroupAdding;
        
        private readonly Queue<Group> _queuedGroupsToAdd = new Queue<Group>(256);

        public Universe(List<Type> components, bool autoInitialize = true) {

            Components = components;

            if (autoInitialize) {
                Initialize();
            }
        }

        public T GetModels<T>() where T : IModels {
            return (T)_models[typeof(T)];
        }

        public Universe RegisterModels<T>() where T: IModels, new() {
            var models = new T();
            models.Setup(this);
            _models[typeof(T)] = models;
            return this;
        }
        
        /// <summary>
        /// Use this to generate local run-time ids.
        /// </summary>
        /// <returns></returns>
        public ulong RidGenerator() {
            return ++NextEntityRid;
        }
        
        /// <summary>
        /// Use this to generate run-time networks safe ids.  
        /// </summary>
        public ulong NidGenerator() {
            return ++NextEntityNid;
        }

        public void ResetNidGenerator(ulong nid) {
            NextEntityRid = nid;
        }
        
        /// <summary>
        /// Most of the time you should avoid direct contact from system to system. Sometimes the benefits might
        /// outweigh the drawbacks. In those cases you can retrieve a system using the GetSystem function.
        /// </summary>
        public T GetSystem<T>() where T: BaseSystem {
            return (T)_systems[typeof(T)];
        }
        
        public Universe RegisterSystem<T>() where T: BaseSystem ,new(){
            var system = new T{Universe = this};
            _systems[typeof(T)] = system;
            system.Initialize(this);
            return this;
        }

        public Universe UnregisterSystem(BaseSystem system) {
            system.Terminate();
            _systems.Remove(system.GetType());
            return this;
        }

        public Group GetGroupByName(string name) {
            return _namedGroups.ContainsKey(name)?_namedGroups[name]:null;
        }

        /// <summary>
        /// You can give a name to group so you can find it easily later. This should be a good way
        /// to register groups that will be used in multiple places.
        /// </summary>
        public Group RegisterGroup(Group g, string name) {
            _namedGroups[name] = g;
            g.Name = name;
            return RegisterGroup(g);
        }

        /// <summary>
        /// Groups need to be registered in order to be updated.
        /// When you add a new groups all existent entities are evaluated against the group.
        /// Note that groups registration might be delayed
        /// </summary>
        public Group RegisterGroup(Group g) {
            
            if (_lockedToGroupAdding) {
                _queuedGroupsToAdd.Enqueue(g);
                return g; 
            }
            
            _groups.AddLast(g);
            
            foreach (var article in _entities) {
                g.EvaluateEntity(article);
            }

            return g;
        }

        /// <summary>
        /// The groups is removed, no event is generated.
        /// </summary>
        public Universe UnregisterGroup(Group g) {
            
            //Groups can be named and in theory can have more than one name
            List<string> names = default;
            foreach (var pair in _namedGroups) {
                if (pair.Value == g) {
                    if (names == null) {
                        names = new List<string>(_namedGroups.Count); //List is created only if needed
                    }
                    names.Add(pair.Key);
                }
            }

            if (names != null) {
                foreach (var name in names) {
                    _namedGroups.Remove(name);
                }
            }

            _groups.Remove(g);
            return this;
        }
        
        private void RegisterQueuedGroups(){
            while (_queuedGroupsToAdd.Any()) {
                var g = _queuedGroupsToAdd.Dequeue();
                RegisterGroup(g);
            }
        }
        
                
        /// <summary>
        /// Call this to communicate the groups when an entity gets one of its components updated.
        /// </summary>
        public void EntityComponentUpdate(Entity entity, Type componentType, IComponent oldComponent, IComponent newComponent) {
            _lockedToGroupAdding = true;
            
            //Deliver to all groups, they will know if they are interested or not.
            var g = _groups.First;
            while (g != null) {
                g.Value.EntityComponentUpdate(entity, componentType, oldComponent, newComponent);
                g = g.Next;
            }

            _lockedToGroupAdding = false;

            RegisterQueuedGroups();
        }
        
        /// <summary>
        /// Call this to communicate the groups when an entity gets any component added
        /// </summary>
        public void EntityComponentAdded(Entity entity, Type componentType, IComponent newComponent) {

            _lockedToGroupAdding = true;
            
            //Deliver to all groups, they will know if they are interested or not.
            var g = _groups.First;
            while (g != null) {
                g.Value.EntityComponentAdded(entity, componentType, newComponent);
                g = g.Next;
            }
            
            //EvaluateEntity is called after so EntityComponentAdded never triggers for components
            //the are adding an entity to a group. This is done to ensure the behaviour is always
            //predictable.
            g = _groups.First;
            while (g != null) {
                g.Value.EvaluateEntity(entity);
                g = g.Next;
            } 
            
            _lockedToGroupAdding = false;

            RegisterQueuedGroups();            
        }
        
              
        /// <summary>
        /// Call this to communicate the groups when an entity gets any component removed
        /// </summary>
        public void EntityComponentRemoved(Entity entity, Type componentType, IComponent newComponent) {
            
            _lockedToGroupAdding = true;
            
            //EvaluateEntity is called before so EntityComponentRemoved never triggers for components
            //the are removing an entity from a group. This is done to ensure the behaviour is always
            //predictable.
            var g = _groups.First;
            while (g != null) {
                g.Value.EvaluateEntity(entity);
                g = g.Next;
            } 
            
            //Deliver to all groups, they will know if they are interested or not.
            g = _groups.First;
            while (g != null) {
                g.Value.EntityComponentRemoved(entity, componentType, newComponent);
                g = g.Next;
            } 
            
            _lockedToGroupAdding = false;

            RegisterQueuedGroups();      
        }
        

        /// <summary>
        /// Get an entity from the pool or instantiate a new one if none is available.
        /// The Rid of the entity are always unique even if the entity came from the pool.
        /// </summary>
        /// <returns></returns>
        public Entity AcquireEntity() {
            var e = _entityPool.Acquire();
            _entities.Add(e);
            return e;
        }

        /// <summary>
        /// Take an entity back. The entity will be reset and returned to the pool
        /// </summary>
        public void RecycleEntity(Entity e) {
            _entities.Remove(e);
            _entityPool.Recycle(e);
        }

        /// <summary>
        /// Get a new instance of a component or an recycled instance from the pool of components
        /// </summary>
        public IComponent AcquireComponent(int componentSid) {
            return _componentPoolsBySid[componentSid].Acquire();
        }
        
        /// <summary>
        /// Get a new instance of a component or an recycled instance from the pool of components
        /// </summary>
        public IComponent AcquireComponent(Type componentType) {
            return _componentPoolsByType[componentType].Acquire();
        }
        
        /// <summary>
        /// Get a new instance of a component or an recycled instance from the pool of components
        /// </summary>
        public T AcquireComponent<T>() {
            return (T)_componentPoolsByType[typeof(T)].Acquire();
        }

        /// <summary>
        /// Return a component to the component pool so it can be reused.
        /// </summary>
        public Universe RecycleComponent(IComponent component) {
            var componentType = component.GetType();
            _componentPoolsByType[componentType].Recycle(component);
            return this;
        }

        /// <summary>
        /// Use this to request a new rid for an entity.
        /// </summary>
        public ulong GenerateNewEntityRid() {
            if (NextEntityRid == int.MaxValue) {
                throw new Exception("Entity Rid reached the max value allowed");
            }
            
            return NextEntityRid++;
        }
        
        /// <summary>
        /// The all entities
        /// </summary>
        public IEnumerable<Entity> Entities() => _entities;

        /// <summary>
        /// Get all entities that satisfy the requirement. Note that this operation might be slow for large groups.
        /// </summary>
        public IEnumerable<Entity> Entities(Matcher matcher) {
            return _entities.Where(e => e.DoesMatchWithComponents(matcher));
        }
        
        public void Initialize() {

            if (Initialized) {
                throw new Exception("This object was initialized already, check your lifecycle handling");
            }
            Initialized = true;
            
            _entities = new HashSet<Entity>();
            _systems = new Dictionary<Type, BaseSystem>(256);
            _groups = new LinkedList<Group>();
            _namedGroups = new Dictionary<string, Group>();
            
            _componentPoolsByType = new Dictionary<Type, Pool<IComponent>>(Components.Count);
            _componentPoolsBySid = new Pool<IComponent>[Components.Count];
            ComponentTable = new ComponentTable();
            
            //Create the default entity pool
            _entityPool = new Pool<Entity>(CoreConstants.Entities, () => new Entity(GenerateNewEntityRid(), this));
            
            foreach (var componentType in Components) {
                //Register the type of component at component type
                ComponentTable.Add(componentType);
                
                //Find the new sid assigned to the component
                var componentSid = ComponentTable.FindComponentSid(componentType);
                
                //Create a default instance of the component capable o generating new instances in a component pool
                var component = (IComponent)Activator.CreateInstance(componentType);
                var componentPool = new Pool<IComponent>(() => component.Default());
                _componentPoolsByType[componentType] = componentPool;
                _componentPoolsBySid[componentSid] = componentPool;
            }
        }

        public void Terminate() {
            
            if (!Initialized) {
                throw new Exception("This object was terminated already, check your lifecycle handling");
            }

            Initialized = false;
            
            //Remove all systems
            foreach (var system in _systems.Values.ToArray()) {
                UnregisterSystem(system);
            }
            
            //Theoretically most groups have been removed with the systems. But in case a manual groups was created it might be still here
            foreach (var @group in _groups.ToArray()) {
                UnregisterGroup(@group);
            }
            
            //Let's delete all entities.
            foreach (var entity in Entities().ToArray()) {
                RecycleEntity(entity);
            }

            _namedGroups = null;
            _componentPoolsByType = null;
            _componentPoolsBySid = null;
            ComponentTable = null;
            _entityPool = null;
            _systems = null;
            _entities = null;
            _systems = null;
            _groups = null;
        }
    }
}