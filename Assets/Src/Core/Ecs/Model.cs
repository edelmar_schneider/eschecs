using System;
using System.Collections.Generic;
using Ce.Core.Util;

namespace Ce.Core.Ecs {

    /// <summary>
    /// A model represents and archetype of an entity with the components that you expect to find or
    /// does not expect to find. 
    /// </summary>
    public sealed class Model {

        public Model Parent { get; private set; }
        
        public Matcher Matcher { get; private set; }
        
        private readonly Dictionary<Type, IComponent> _defaultComponents = new Dictionary<Type, IComponent>(CoreConstants.Components);
        
        private readonly List<int> _helperList = new List<int>(CoreConstants.Components);
        
        public Model(Matcher matcher) => Matcher = matcher;

        public Model(Model parent, Matcher matcher) {
            Parent = parent;
            Matcher = matcher;
            matcher.Add(parent.Matcher);
        }

        public static Model New(Matcher matcher) {
            var model = new Model(matcher);
            return model;
        }

        public static Model New(Model parent, Matcher matcher) {
            var model = new Model(parent, matcher );
            return model;
        }

        public Model AddDefault<T>(T component) where T : IComponent {
            var componentType = typeof(T);
            _defaultComponents[componentType] = component;
            return this;
        }
        
        public static implicit operator Matcher(Model model) {
            return model.Matcher;
        }

        /// <summary>
        /// Create an entity of the type defined in this model.
        /// </summary>
        public Entity MakeEntity(Universe universe) {
            var e = universe.AcquireEntity();
            var neededComponents = Matcher.GetAll();
            
            //Add components that we don't have a default defined
            foreach (var compSid in neededComponents) {
                var compType = universe.ComponentTable.FindComponentType(compSid);
                if (!_defaultComponents.ContainsKey(compType)) {
                    var comp = universe.AcquireComponent(compSid);
                    e.Update(comp, compSid);
                }
            }
            
            //Now let's include components with a default
            foreach (var defaultComp in _defaultComponents.Values) {
                var comp = (IComponent)defaultComp.DeepCopy();
                e.Update(comp, comp.GetType());
            }

            return e;
        }

        /// <summary>
        /// Check whether the entity conforms to the model requirements.
        /// </summary>
        public bool Conform(Entity e) {
            _helperList.Clear();
            e.GetAllComponentsSids(_helperList);
            return Matcher.Match(_helperList);
        }
    }
}