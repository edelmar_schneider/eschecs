using System.Xml.Linq;
using Ce.Core.Util;

namespace Ce.Core.Ecs {

    /// <summary>
    /// Defines a component of the Entity Component System 
    /// </summary>
    public interface IComponent : IDeepCopy, ICleanable {

        /// <summary>
        /// Create a new default instance of the component 
        /// </summary>
        IComponent Default();
        
        /// <summary>
        /// Some components should not be serialized.
        /// </summary>
        bool IsSerializable { get; }

        XElement ToXml(string name);

        void FromXml(XElement element);
    }

}