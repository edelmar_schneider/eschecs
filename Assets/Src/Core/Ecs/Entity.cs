using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml;
using System.Xml.Linq;
using Ce.Core.Util;

namespace Ce.Core.Ecs {

    /// <summary>
    /// The Entity of the entity component system.
    /// </summary>
    public sealed class Entity: ICleanable {
        
        /// <summary>
        /// Use for debugging.
        /// </summary>
        public string Name { get; private set; } = "Undefined";
        
        /// <summary>
        /// Run-time ID. This is the unique number that defines the unique identity of an entity.
        /// There can be no duplication of this number during run-time.
        /// NOTE that the this Rid is only valid locally, over network a different client will have different ids for similar entities. Use RidComponent for networking purposes.
        /// </summary>
        public ulong Rid { get; private set; }

        /// <summary>
        /// This functions is to be used only when loading a game to reset the rid based on the saved value
        /// </summary>
        public void ResetRid(ulong rid) {
            Rid = rid;
        }
        
        /// <summary>
        /// The Universe this Entity belongs to.
        /// </summary>
        public Universe Universe { get; }
        
        /// <summary>
        /// The Entity is active under normal usage. When the Entity is being recycled it won't be active.
        /// It's always a good idea to check if the Entity is active before a system reacts to it.
        /// </summary>
        public bool Active { get; private set; }

        /// <summary>
        /// All the instances of components that this entity has by component Sid
        /// </summary>
        private readonly IComponent[] _componentsBySid = new IComponent[CoreConstants.Components];
        
        /// <summary>
        /// All the instances of components that this entity has by component type
        /// </summary>
        private readonly Dictionary<Type, IComponent> _componentsByType = new Dictionary<Type, IComponent>(CoreConstants.Components);
        
        /// <summary>
        /// List with all components sids that exist in this entity 
        /// </summary>
        private readonly List<int> _componentsSids = new List<int>(CoreConstants.Components);

        /// <summary>
        /// The entity becomes locked when it is being changed and any further change will be queued
        /// until all groups have been notified.
        /// </summary>
        private bool _lockedToChange;
        
        private readonly Queue<QueuedEntityCommand> _queuedEntityCommands = new Queue<QueuedEntityCommand>(256);
        
        public Entity(ulong rid, Universe universe) {
            Rid = rid;
            Universe = universe;
            Active = true;
        }

        public Entity DefineName(string name) {
            Name = name;
            return this;
        }
        
        /// <summary>
        /// Check if the matcher checks with the components in the entity
        /// </summary>
        public bool DoesMatchWithComponents(Matcher matcher) {
            return matcher.Match(_componentsSids);
        }

        /// <summary>
        /// The entity will deactivated, the components will be returned and the entity will receive a new Rid to be reused
        /// </summary>
        public void CleanUp() {
            Active = false;
            Name = string.Empty;
            {
                //Now the entity is ready to be used again.
                foreach (var component in GetAllComponents()) {
                    Remove(component.GetType());
                }
                
                Rid = Universe.GenerateNewEntityRid();
            }
            Active = true;
        }

        /// <summary>
        /// Add/Update a component into the Entity
        /// Medium speed
        /// </summary>
        public Entity Update<T>(T component) where T : IComponent {
            
            var componentType = typeof(T);
            var componentSid = Universe.ComponentTable.FindComponentSid(componentType);

            return Update(component, componentSid);
        }
        
        public Entity Update(IComponent component, Type componentType){
            
            var componentSid = Universe.ComponentTable.FindComponentSid(componentType);
            return Update(component, componentSid);
        }
        
        /// <summary>
        /// Add/Update a component into the Entity
        /// Faster method
        /// </summary>
        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        public Entity Update<T>(T component, int componentSid) where T : IComponent {

            var componentType = component.GetType();
            
            if (_lockedToChange) {
                _queuedEntityCommands.Enqueue(new UpdateCommand(component, componentSid));
                return this; 
            }

            _lockedToChange = true;
            
            var isUpdate = _componentsBySid[componentSid] != null;
            var isAdding = !isUpdate;
            if (isAdding) {
                
                //Add the component to collections of the entity
                _componentsBySid[componentSid] = component;
                _componentsByType[componentType] = component;
                _componentsSids.Add(componentSid);
                
                //Notify groups that a component got added.
                Universe.EntityComponentAdded(this, componentType, component);
            } 
            else 
            {
                //Grab the old component
                var oldComponent = _componentsBySid[componentSid];
                
                //Replace the component in the collections of the entity
                _componentsBySid[componentSid] = component;
                _componentsByType[componentType] = component;
                
                //Notify groups that a component got replaced
                Universe.EntityComponentUpdate(this, componentType, oldComponent, component);

                //Recycle the old component.
                Universe.RecycleComponent(oldComponent);
            }
            
            _lockedToChange = false;

            ExecuteQueuedCommands();
            
            return this;
        }

        /// <summary>
        /// Remove a component from the entity
        /// </summary>
        public Entity Remove(Type componentType) {
            var componentSid = Universe.ComponentTable.FindComponentSid(componentType);
            
            return Remove(componentSid);
        }
        
        /// <summary>
        /// Remove a component from the entity
        /// </summary>
        public Entity Remove<T>() where T : IComponent {
            var componentType = typeof(T);
            var componentSid = Universe.ComponentTable.FindComponentSid(componentType);
            
            return Remove(componentSid);
        }

        /// <summary>
        /// Remove a component from the entity
        /// </summary>
        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        public Entity Remove(int componentSid) {
            
            if (_lockedToChange) {
                _queuedEntityCommands.Enqueue(new RemoveCommand(componentSid));
                return this;
            }

            _lockedToChange = true;
            var componentType = Universe.ComponentTable.FindComponentType(componentSid);
            
            if (!Has(componentSid)) {
                throw new Exception($"Cannot remove component {componentType} because the entity doesn't have it.");
            }

            //Grab the oldComponent
            var oldComponent = _componentsBySid[componentSid];
            
            //Remove from the collections
            _componentsByType.Remove(componentType);
            _componentsBySid[componentSid] = null;
            _componentsSids.Remove(componentSid);
            
            //Notify the groups
            Universe.EntityComponentRemoved(this, componentType, oldComponent);

            //Recycle the component
            Universe.RecycleComponent(oldComponent);
            
            _lockedToChange = false;
            
            ExecuteQueuedCommands();

            return this;
        }

        /// <summary>
        /// Check whether the entity has a component;
        /// </summary>
        public bool Has(Type componentType) => _componentsByType.ContainsKey(componentType);
        
        /// <summary>
        /// Check whether the entity has a component;
        /// </summary>
        public bool Has(int componentSid) => _componentsBySid[componentSid] != null;
        
        /// <summary>
        /// Check whether the entity has a component;
        /// </summary>
        public bool Has<T>() where T: IComponent => _componentsByType.ContainsKey(typeof(T));


        /// <summary>
        /// Retrieve the component from the entity if it exists
        /// </summary>
        public IComponent Get(Type componentType) => _componentsByType[componentType];

        /// <summary>
        /// Retrieve the component from the entity if it exists
        /// </summary>
        public IComponent Get(int componentSid) => _componentsBySid[componentSid];

        /// <summary>
        /// Retrieve the component from the entity if it exists
        /// </summary>
        public T Get<T>() where T : IComponent => (T)_componentsByType[typeof(T)];
        
        
        private void ExecuteQueuedCommands(){
            while (_queuedEntityCommands.Any()) {
                var c = _queuedEntityCommands.Dequeue();
                if (c is UpdateCommand uc) {
                    Update(uc.Component, uc.ComponentSid);
                    continue;
                }

                if (c is RemoveCommand rc) {
                    Remove(rc.ComponentSid);
                    continue;
                }
            }
        }

        /// <summary>
        /// Count of how many components this entity has.
        /// </summary>
        public int ComponentCount => _componentsSids.Count;

        /// <summary>
        /// Retrieve all components in this entity.
        /// An optional list can be provided to improve performance, the list will be cleared.
        /// </summary>
        public List<IComponent> GetAllComponents(List<IComponent> list = default) {
            if (list != null) {
                list.Clear();
            }
            else {
                list = new List<IComponent>(CoreConstants.Components);
            }

            foreach (var componentsSid in _componentsSids) {
                list.Add(Get(componentsSid));
            }

            return list;
        }
        
        /// <summary>
        /// Retrieve all components in this entity.
        /// An optional list can be provided to improve performance, the list will be cleared.
        /// </summary>
        public List<int> GetAllComponentsSids(List<int> list = default) {
            if (list != null) {
                list.Clear();
            }
            else {
                list = new List<int>(CoreConstants.Components);
            }
            
            list.AddRange(_componentsSids);

            return list;
        }

        public override string ToString() {
            return $"Entity_{Rid.ToString()}_{Name}";
        }


        public class QueuedEntityCommand {
        }

        public class UpdateCommand : QueuedEntityCommand {
            public IComponent Component { get; private set; }
            public int ComponentSid { get; private set; }

            public UpdateCommand( IComponent component, int componentSid) {
                Component = component;
                ComponentSid = componentSid;
            }
        }

        public class RemoveCommand : QueuedEntityCommand {
            public int ComponentSid { get; private set; }

            public RemoveCommand(int componentSid) => ComponentSid = componentSid;
        }


        public XElement ToXml(string elementName) {
            
            var root = new XElement(elementName);
            
            foreach (var component in GetAllComponents()) {
                if (!component.IsSerializable) {
                    continue;
                }

                var name = component.GetType().Name.Replace("Component", "");
                var cXml = component.ToXml(name);
                cXml.Add(new XAttribute("Tid",Universe.ComponentTable.FindComponentSid(component.GetType())));
                root.Add(cXml);

            }

            return root;
        }
    }

}