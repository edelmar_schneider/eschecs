using System;
using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;

namespace Ce.Core.Util
{
    /// <summary>
    /// Evaluate if a set of tags matches the requirements defined in _all, _any and _none
    /// </summary>
    public class Matcher: IDeepCopy
    {
        List<string> _allNames = new List<string>(256);
        
        private readonly ISet<int> _all = new HashSet<int>();
        private readonly ISet<int> _any = new HashSet<int>();
        private readonly ISet<int> _none = new HashSet<int>();

        private readonly Universe _universe;

        private readonly List<int> _helperList = new List<int>(64);

        public int[] GetAll() => _all.ToArray();
        
        public static Matcher New(Universe universe) => new Matcher(universe);

        /// <summary>
        /// Check if the collection matches with the Matcher selection
        /// </summary>
        public bool Match(ICollection<int> other) {
            
            if (_none.Overlaps(other)) {
                return false;
            }

            if (_any.Any()) {
                if (!_any.Overlaps(other)) {
                    return false;
                }
            }

            if (_all.IsSubsetOf(other)) {
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// Check if the collection matches with the Matcher selection
        /// Slower method, use the int collection version if you can.
        /// </summary>
        public bool Match(IEnumerable<Type> other) {
            _helperList.Clear();
            foreach (var componentType in other) {
                _helperList.Add(_universe.ComponentTable.FindComponentSid(componentType));
            }
            return Match(_helperList);
        }

        private Matcher(Universe universe) {
            _universe = universe;
        }
        
        public Matcher DeepCopy() {
            var m = new Matcher(_universe);
            
            foreach (var i in _all) {
                m._all.Add(i);
            }
            
            foreach (var i in _any) {
                m._any.Add(i);
            }
            
            foreach (var i in _none) {
                m._none.Add(i);
            }

            return m;
        }
        
        object IDeepCopy.DeepCopy() => DeepCopy();

        /// <summary>
        /// Add All, Any and None of the other into this.
        /// </summary>
        public Matcher Add(Matcher other) {
            foreach (var i in other._all) {
                _all.Add(i);
            }
            
            foreach (var i in other._any) {
                _any.Add(i);
            }
            
            foreach (var i in other._none) {
                _none.Add(i);
            }

            return this;
        }
        
        public Matcher All(params Type[] values) {
            foreach (var value in values) {
                _allNames.Add(value.Name);
            }
            _all.UnionWith(values.Select(c => _universe.ComponentTable.FindComponentSid(c)));
            return this;
        }
        
        public Matcher Any(params Type[] values){
            _any.UnionWith(values.Select(c => _universe.ComponentTable.FindComponentSid(c)));
            return this;
        }
        
        public Matcher None(params Type[] values) { 
            _none.UnionWith(values.Select(c => _universe.ComponentTable.FindComponentSid(c)));
            return this;
        }
        
        public Matcher All(params int[] values) {
            foreach (var value in values) {
                _allNames.Add(value.GetType().Name);
            }
            _all.UnionWith(values);
            return this;
        }
        
        public Matcher Any(params int[] values) {
            _any.UnionWith(values);
            return this;
        }
        
        public Matcher None(params int[] values){ 
            _none.UnionWith(values);
            return this;
        }

        public Matcher All<A>() 
            where A : IComponent 
        {
            return All(typeof(A));
        }
        
        public Matcher All<A, B>()  
            where A : IComponent 
            where B : IComponent 
        {
            return All(typeof(A), typeof(B));
        }
        
        public Matcher All<A, B, C>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
        {
            return All(typeof(A), typeof(B), typeof(C));
        }        
        
        public Matcher All<A, B, C, D>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
        {
            return All(typeof(A), typeof(B), typeof(C), typeof(D));
        }
        
        public Matcher All<A, B, C, D, E>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
        {
            return All(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E));
        }
        
        public Matcher All<A, B, C, D, E, F>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
            where F : IComponent 
        {
            return All(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E), typeof(F));
        }
        
        public Matcher All<A, B, C, D, E, F, G>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
            where F : IComponent 
            where G : IComponent 
        {
            return All(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E), typeof(F), typeof(G));
        }
        

        
        
        public Matcher Any<A>() 
            where A : IComponent 
        {
            return Any(typeof(A));
        }
        
        public Matcher Any<A, B>()  
            where A : IComponent 
            where B : IComponent 
        {
            return Any(typeof(A), typeof(B));
        }
        
        public Matcher Any<A, B, C>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
        {
            return Any(typeof(A), typeof(B), typeof(C));
        }        
        
        public Matcher Any<A, B, C, D>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
        {
            return Any(typeof(A), typeof(B), typeof(C), typeof(D));
        }
        
        public Matcher Any<A, B, C, D, E>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
        {
            return Any(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E));
        }
        
        public Matcher Any<A, B, C, D, E, F>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
            where F : IComponent 
        {
            return Any(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E), typeof(F));
        }
        
        public Matcher Any<A, B, C, D, E, F, G>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
            where F : IComponent 
            where G : IComponent 
        {
            return Any(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E), typeof(F), typeof(G));
        }
        
        
        
        public Matcher None<A>() 
            where A : IComponent 
        {
            return None(typeof(A));
        }
        
        public Matcher None<A, B>()  
            where A : IComponent 
            where B : IComponent 
        {
            return None(typeof(A), typeof(B));
        }
        
        public Matcher None<A, B, C>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
        {
            return None(typeof(A), typeof(B), typeof(C));
        }        
        
        public Matcher None<A, B, C, D>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
        {
            return None(typeof(A), typeof(B), typeof(C), typeof(D));
        }
        
        public Matcher None<A, B, C, D, E>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
        {
            return None(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E));
        }
        
        public Matcher None<A, B, C, D, E, F>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
            where F : IComponent 
        {
            return None(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E), typeof(F));
        }
        
        public Matcher None<A, B, C, D, E, F, G>()  
            where A : IComponent 
            where B : IComponent 
            where C : IComponent 
            where D : IComponent 
            where E : IComponent 
            where F : IComponent 
            where G : IComponent 
        {
            return None(typeof(A), typeof(B), typeof(C), typeof(D), typeof(E), typeof(F), typeof(G));
        }            
    }
}