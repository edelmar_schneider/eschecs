namespace Ce.Core.Util {

    /// <summary>
    /// Objects that can/must be clean up before destruction or when they are going to be recycled.
    /// </summary>
    public interface ICleanable {

        /// <summary>
        /// Clean up the object releasing resources leaving it ready to be collected by GC or recycled.
        /// </summary>
        void CleanUp();

    }

}