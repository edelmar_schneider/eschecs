namespace Ce.Core.Util {

    /// <summary>
    /// The object that implements this interface must provide a deep copy of the object.
    /// </summary>
    public interface IDeepCopy {

        /// <summary>
        /// Perform a deep copy of the object.
        /// </summary>
        object DeepCopy();
        
        
    }

}