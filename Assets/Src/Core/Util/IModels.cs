using Ce.Core.Ecs;

namespace Ce.Core.Util {

    public interface IModels {

        void Setup(Universe universe);
    }
}