using System;
using System.Collections.Generic;
using System.Linq;

namespace Ce.Core.Util {

    /// <summary>
    /// A generic pool for object recycling.
    /// Objects that implement <see cref="ICleanable"/> will have the clean up method called.
    /// </summary>
    public class Pool<T>  {

        /// <summary>
        /// Objects in the pool awaiting to be reused.
        /// </summary>
        private readonly Stack<T> _objects;
        
        /// <summary>
        /// You need to define a method capable of constructing an object of type T
        /// </summary>
        private readonly Func<T> _constructorMethod;

        public Pool(Func<T> constructorMethod) : this(CoreConstants.PoolSize, constructorMethod){}

        public Pool(int initialSize, Func<T> constructorMethod) {
            _constructorMethod = constructorMethod;
            _objects = new Stack<T>(initialSize);
        }

        /// <summary>
        /// Check how many objects are in the pool.
        /// </summary>
        public int Count => _objects.Count;

        /// <summary>
        /// Get an object of the type T from the pool or a new instance.
        /// </summary>
        public T Acquire() {
            if (_objects.Any()) {
                return _objects.Pop();
            }
            else {
                return _constructorMethod();
            }
            
        }

        /// <summary>
        /// Return an object of type T to the pool.
        /// </summary>
        public void Recycle(T obj) {

            if (obj == null) {
                throw new Exception("Recycled object cannot be null");
            }
            
            if (obj is ICleanable clean) {
                clean.CleanUp();
            }
            
            _objects.Push(obj);
        }

    }

}