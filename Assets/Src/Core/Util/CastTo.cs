using System;
using System.Linq.Expressions;

namespace Ce.Core.Util {


    /// <summary>
    /// Courtesy: https://stackoverflow.com/questions/1189144/c-sharp-non-boxing-conversion-of-generic-enum-to-int
    /// Class to cast to type <see cref="T"/>
    /// </summary>
    public static class CastTo<T> {

        /// <summary>
        /// Casts <see cref="S"/> to <see cref="T"/>.
        /// This does not cause boxing for value types.
        /// Useful in generic methods.
        /// </summary>
        /// <typeparam name="S">Source type to cast from. Usually a generic type.</typeparam>
        public static T From<S>(S s) {
            return Cache<S>.Caster(s);
        }

        private static class Cache<S> {

            public static readonly Func<S, T> Caster = Get();

            public static Func<S, T> Get() {
                var p = Expression.Parameter(typeof(S), "Input");
                var c = Expression.Convert(p, typeof(T));
                return Expression.Lambda<Func<S, T>>(c, p).Compile();
            }

        }

    }

}