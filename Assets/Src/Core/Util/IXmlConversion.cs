using System.Xml.Linq;

namespace Ce.Core.Util {

    public interface IXmlConversion
    {
        XElement ToXml(string elementName);

        void FromXml(XElement element);

    }

}