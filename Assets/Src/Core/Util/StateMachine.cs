using System;
using System.Collections.Generic;
using System.Linq;

namespace Ce.Core.Util
{
    /// <summary>
    /// A generic state machine, instantiate and call <see cref="Configure"/> to
    /// set it up.
    /// </summary>
    /// <typeparam name="TState">Define the states you want</typeparam>
    /// <typeparam name="TTrigger">Define the triggers you need</typeparam>
    public sealed class StateMachine<TState, TTrigger>
    {
        /// <summary>
        /// This is used only if an external setter/getter is not provided. 
        /// </summary>
        private TState _state;

        /// <summary>
        /// Set current state of the machine
        /// </summary>
        private Action<TState> SetState { get; set; }

        /// <summary>
        /// Get current state of the machine
        /// </summary>
        public Func<TState> State { get; } 

        /// <summary>
        /// When <see cref="Trigger"/ is called>
        /// </summary>
        public Action<TTrigger> OnTriggerRequested { get; set; } = t => { };
        
        /// <summary>
        /// If <see cref="Trigger"/> is rejected for any reason
        /// </summary>
        public Action<TTrigger, TriggerRejectionReason> OnTriggerRejected { get; set; } = (t,rr) => { };
        
        /// <summary>
        /// When <see cref="Trigger"/> is called and accepted.
        /// </summary>
        public Action<TTrigger> OnTriggerIgnored { get; set; } = t => { };
        
        /// <summary>
        /// When <see cref="Trigger"/> is called and accepted.
        /// </summary>
        public Action<TTrigger> OnTriggerExecuted { get; set; } = t => { };

        /// <summary>
        /// When you call <see cref="Trigger"/> the trigger is set to locked until the function finishes.
        /// </summary>
        private bool _triggerLocked;

        /// <summary>
        /// If this is set the trigger will be called before the <see cref="Trigger"/> function returns
        /// </summary>
        private (bool IsSet, TTrigger Value) _queuedTrigger;
        
        /// <summary>
        /// To be used when a trigger is called
        /// </summary>
        private readonly IDictionary<TState, List<TriggerResponseDescriptor>> _stateAndTransitions = new Dictionary<TState, List<TriggerResponseDescriptor>>();
        
        /// <summary>
        /// To be called when entering/exiting a state.
        /// </summary>
        private readonly IDictionary<TState, StateChangeResponseDescriptor> _stateBehaviours = new Dictionary<TState, StateChangeResponseDescriptor>();

        /// <summary>
        /// Check whether you will have success calling a trigger right now.
        /// </summary>
        public bool CanTrigger(TTrigger trigger)
        {
            return CanTrigger(trigger, false, out var dec);
        }
        
        /// <summary>
        /// Try and call a trigger to promote a state change.
        /// <seealso cref="TriggerAfter"/>
        /// </summary>
        public bool Trigger(TTrigger trigger)
        {
            OnTriggerRequested(trigger);
            if (_triggerLocked) {
                OnTriggerRejected(trigger, TriggerRejectionReason.TriggerFunctionIsLocked);
                return false;
            }

            if (!CanTrigger(trigger, true, out var descriptor)) {
                return false;
            }
            
            _triggerLocked = true;
            {
                ExecuteStateChange(State(), descriptor.TargetState);
                OnTriggerExecuted(trigger);
            }
            _triggerLocked = false;

            if (_queuedTrigger.IsSet) {
                var trig = _queuedTrigger.Value;
                _queuedTrigger = (false, default);
                return Trigger(trig);
            }
            
            return true;    
            
        }
        
        /// <summary>
        /// If you try to trigger something while a StateChange is happening the trigger function
        /// will return false. But you can use this function to queue a trigger call to be called right
        /// after the state change
        /// </summary>
        public bool TriggerAfter(TTrigger trigger) {
            if (_queuedTrigger.IsSet) {
                return false;
            }

            if (!_triggerLocked) {
                //Because you can only queue when the trigger is locked
                return false;
            }
            
            _queuedTrigger = (true, trigger);
            return true;
        }

        /// <summary>
        /// This was meant to be used with serialized object when you want to
        /// execute the OnEnter functions to reenter the state you already are in
        /// </summary>
        public bool ReenterCurrentState()
        {
            _triggerLocked = true;
            EnterSate(State(), State());
            _triggerLocked = false;
            
            if (_queuedTrigger.IsSet) {
                var trig = _queuedTrigger.Value;
                _queuedTrigger = (false, default);
                return Trigger(trig);
            }
            
            return true;                
        }

        private void ExecuteStateChange(TState oldState, TState targetState)
        {
            ExitState(oldState, targetState);
            SetState(targetState);
            EnterSate(oldState, targetState);
        }

        private void EnterSate(TState oldState, TState newState )
        {
            if (!_stateBehaviours.ContainsKey(newState))
            {
                return;
            }

            var behaviour = _stateBehaviours[newState];
            behaviour.OnEnter(oldState, newState);
        }

        private void ExitState(TState oldState, TState newState)
        {
            if (!_stateBehaviours.ContainsKey(oldState))
            {
                return;
            }

            var behaviour = _stateBehaviours[oldState];
            behaviour.OnExit(oldState, newState);
        }

        private bool CanTrigger(TTrigger trigger, bool report, out TriggerResponseDescriptor descriptor)
        {
            if (!_stateAndTransitions.ContainsKey(State()))
            {
                if (report)
                {
                    OnTriggerRejected(trigger, TriggerRejectionReason.StateIsNotConfigured);
                }
                
                descriptor = TriggerResponseDescriptor.Empty();
                return false;
            }
            
            //Let's find the single element (if exists) that allows the transition for the trigger
            var desc = _stateAndTransitions[State()]
                .SingleOrDefault(ste => ste.Trigger.Equals(trigger));

            if (desc != null)
            {
                if (desc.Ignore)
                {
                    descriptor = TriggerResponseDescriptor.IgnoreTrigger(trigger);
                    OnTriggerIgnored(trigger);
                    return false;
                }
                
                if (desc.Validate())
                {
                    descriptor = desc;
                    return true;
                }
                else
                {
                    descriptor = TriggerResponseDescriptor.Empty();
                    OnTriggerRejected(trigger, TriggerRejectionReason.ValidationFunctionRejected);
                    return false;
                }
            }
            else
            {
                descriptor = TriggerResponseDescriptor.Empty();
                OnTriggerRejected(trigger, TriggerRejectionReason.TransitionIsNotDefined);
                return false;
            }
        }
        
        public StateMachine(Action<TState> stateSetter, Func<TState> stateGetter)
        {
            //Default setter/getter for the state.
            SetState = stateSetter;
            State = stateGetter;
        }
        
        public StateMachine(TState initialState)
        {
            //Default setter/getter for the state.
            SetState = newState => _state = newState;
            State = () => _state;
            
            SetState(initialState);
        }

        public IMachineConfiguration Configure()
        {
            return new StateMachineBuilder(this);
        }

        public enum TriggerRejectionReason
        {
            StateIsNotConfigured,
            TransitionIsNotDefined,
            ValidationFunctionRejected,
            TriggerFunctionIsLocked
        }

        #region Auxiliary classes
        
        public interface IStateConfiguration
        {
            IStateConfiguration Ignore( TTrigger trigger);
            
            IStateConfiguration Trigger( TTrigger trigger, TState state);
            
            IStateConfiguration Trigger( TTrigger trigger, TState state, Func<bool> checkCondition);

            IStateConfiguration OnEnter(Action<TState, TState> action);
            IStateConfiguration OnExit(Action<TState, TState> action);
            
            IMachineConfiguration Return();
        }
        
        public interface IMachineConfiguration
        {
            IStateConfiguration State(TState state);

            StateMachine<TState, TTrigger> Build();
        }

        private class StateMachineBuilder: IStateConfiguration, IMachineConfiguration
        {
            private readonly StateMachine<TState, TTrigger> _stateMachine;

            /// <summary>
            /// The behaviour of the state that we are building now
            /// </summary>
            private StateChangeResponseDescriptor _currentStateChangeResponseDescriptor;

            /// <summary>
            /// The state that we are building now
            /// </summary>
            private TState _currentState;

            public StateMachineBuilder(StateMachine<TState, TTrigger> stateMachine)
            {
                _stateMachine = stateMachine;
            }

            /// <summary>
            /// If the trigger is called and rejected because of <see cref="TriggerRejectionReason.TransitionIsNotDefined"/> it won't be reported.
            /// Use this when you expect a trigger to be called but you don't want to report that the trigger has failed. 
            /// </summary>
            public IStateConfiguration Ignore(TTrigger trigger)
            {
                var descriptors = _stateMachine._stateAndTransitions[_currentState];

                //Remove any old trigger that might be present
                descriptors.RemoveAll(descriptor => descriptor.Trigger.Equals(trigger));
                
                //Add the new transition
                descriptors.Add( TriggerResponseDescriptor.IgnoreTrigger(trigger));

                return this;
            }

            public IStateConfiguration Trigger( TTrigger trigger, TState state)
            {
                Trigger(trigger, state, () => true);
                return this;
            }

            public IStateConfiguration Trigger(TTrigger trigger, TState state, Func<bool> checkCondition)
            {
                var descriptors = _stateMachine._stateAndTransitions[_currentState];

                //Remove any old trigger that might be present
                descriptors.RemoveAll(descriptor => descriptor.Trigger.Equals(trigger));
                
                //Add the new transition
                descriptors.Add(new TriggerResponseDescriptor(trigger, state, checkCondition));

                return this;
            }

            public IStateConfiguration State(TState state)
            {
                _currentStateChangeResponseDescriptor = new StateChangeResponseDescriptor();
                _currentState = state;
                _stateMachine._stateBehaviours[state] = _currentStateChangeResponseDescriptor;
                _stateMachine._stateAndTransitions[state] = new List<TriggerResponseDescriptor>();
                return this;
            }

            public IMachineConfiguration Return()
            {
                return this;
            }

            public StateMachine<TState, TTrigger> Build()
            {
                return _stateMachine;
            }

            public IStateConfiguration OnEnter(Action<TState, TState> action)
            {
                _currentStateChangeResponseDescriptor.OnEnter = action;
                return this;
            }

            public IStateConfiguration OnExit(Action<TState, TState> action)
            {
                _currentStateChangeResponseDescriptor.OnExit = action;
                return this;
            }
        }

        private sealed class TriggerResponseDescriptor
        {
            /// <summary>
            /// Set this when you actually want to ignore the trigger.
            /// </summary>
            public bool Ignore { get; private set; }
            
            public TTrigger Trigger { get; private set; }
            
            public TState TargetState { get; private set; }

            /// <summary>
            /// Call/Define the function to ensure that the trigger request is valid.  
            /// </summary>
            public Func<bool> Validate { get; private set; }

            public TriggerResponseDescriptor( TTrigger trigger, TState targetState)
            {
                Trigger = trigger;
                TargetState = targetState;
                Validate = () => true;
            }

            public TriggerResponseDescriptor( TTrigger trigger,TState targetState,  Func<bool> validate)
            {
                Trigger = trigger;
                TargetState = targetState;
                Validate = validate;
            }

            private static readonly TriggerResponseDescriptor _empty =  new TriggerResponseDescriptor(default, default);
            public static TriggerResponseDescriptor Empty() => _empty;

            private static readonly IDictionary<TTrigger, TriggerResponseDescriptor> _ignoreTriggerCache = new Dictionary<TTrigger, TriggerResponseDescriptor>();
            public static TriggerResponseDescriptor IgnoreTrigger(TTrigger trigger)
            {
                if (!_ignoreTriggerCache.ContainsKey(trigger))
                {
                    var newIgnoreTriggerResponse = new TriggerResponseDescriptor(trigger, default){Ignore = true};
                    _ignoreTriggerCache[trigger] = newIgnoreTriggerResponse;
                    return newIgnoreTriggerResponse;
                }

                return _ignoreTriggerCache[trigger];
            }
        }

        private sealed class StateChangeResponseDescriptor
        {
            public Action<TState, TState> OnEnter { get; set; }
            public Action<TState, TState> OnExit { get; set; }

            public StateChangeResponseDescriptor()
            {
                OnEnter = (oldState, newState) => { };
                OnExit = (oldState, newState) => { };
            }

            public StateChangeResponseDescriptor(Action<TState, TState> onEnter, Action<TState, TState> onExit)
            {
                OnEnter = onEnter;
                OnExit = onExit;
            }
        }
        
        #endregion         
    }
}