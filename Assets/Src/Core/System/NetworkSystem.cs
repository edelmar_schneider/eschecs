using System;
using Ce.Core.Ecs;
using Ce.Core.Message.Command;
using Ce.Core.Message.Report;
using Ce.Core.Message.Request;
using Ce.Core.Network;
using MLAPI;

namespace Ce.Core.System {

    /// <summary>
    /// This class is designed to be fully integrated into the ECS a facilitate the network communication
    /// via a third-party object. The actual network code is handled by the IComInterface.
    /// </summary>
    public class NetworkSystem : BaseSystem {
        
        /// <summary>
        /// Inject here the proper object to handle the actual network communication
        /// </summary>
        public IComInterface ComInterface { get; set; }
        
        /// <summary>
        /// Inject here the proper object to handle the actual network services
        /// </summary>
        public INetTransport NetTransport { get; set; }

        private TimeSystem _timeSystem;
        
        protected override void OnInitialize() {
            base.OnInitialize();
            _timeSystem = Universe.GetSystem<TimeSystem>();
        }
        
        #region MessageInput
        
        /// <summary>
        /// This is how clients ask for things. 
        /// Such as: Request Unit_1 attack Unit_32.
        /// The server will receive the request and decide if it should be propagated to the clients in form of Command
        /// Note that the server can also be a client
        /// </summary>
        public void Request(IRequest request) {
            
            request.Tick = _timeSystem.Tick;
            var xml = request.ToXml("req");
            
            ComInterface.DispatchToServer(xml.ToString());
        }

        /// <summary>
        /// You should only call this if you are the server. Because the server makes all decisions 
        /// The server produce the command which are then delivered to the clients
        /// A delay will be introduced to ensure everyone is in sync
        /// </summary>
        public void Command(ICommand command) {
            if (!NetworkManager.Singleton.IsServer) {
                throw new Exception();
            }
            
            if (command is ResetTimeCommand) {
                _timeSystem.ResetTicks(0);
            }
            
            command.Tick += _timeSystem.Tick + CoreConstants.TicksOfDelay;
            
            var xml = command.ToXml("com");
            ComInterface.DispatchToClients(xml.ToString());
        }
        
        /// <summary>
        /// You should only call this if you are the server. Because the server makes all decisions 
        /// </summary>
        public void Report(IReport report) {
            if (!NetworkManager.Singleton.IsServer) {
                throw new Exception();
            }
            
            report.Tick += _timeSystem.Tick;
            
            var xml = report.ToXml("rep");
            ComInterface.DispatchToClients(xml.ToString());
        }

        #endregion
        
    }

}