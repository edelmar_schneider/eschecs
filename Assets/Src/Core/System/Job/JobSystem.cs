using System;
using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;

namespace Ce.Core.System.Job {

    public class JobSystem: BaseSystem {
        
        private TimeSystem _timeSystem;
        
        private readonly LinkedList<IJob> _jobs = new LinkedList<IJob>();

        protected override void OnInitialize() {
            base.OnInitialize();
            _timeSystem = Universe.GetSystem<TimeSystem>();
            _timeSystem.FixedUpdateEvent += OnFixedUpdate;
        }

        private void OnFixedUpdate() {

            if (!_jobs.Any()) {
                return;
            }

            //Now let tick the remaining jobs
            {
                var job = _jobs.First;
                while (job != null) {
                    var nextJob = job.Next;
                    job.Value.Tick();
                    job = nextJob;
                }
            }

            //Lets remove finished jobs
            {
                var job = _jobs.First;
                while (job != null) {
                    var nextJob = job.Next;
                    if (job.Value.State == JobState.Aborted || job.Value.State == JobState.Success) {
                        _jobs.Remove(job);
                    }

                    job = nextJob;
                }
            }
        }

        protected override void OnTerminate() {
            _timeSystem.FixedUpdateEvent -= OnFixedUpdate;
            
            _jobs.Clear();
            
            base.OnTerminate();
        }
        
        public IJob[] Jobs => _jobs.ToArray();

        /// <summary>
        /// Create a new job in the system.
        /// Jobs are ticked at FixedUpdate event.
        /// </summary>
        public T RegisterJob<T>() where T : class, IJob, new() {
            var job = new T();
            _jobs.AddLast(job);
            return job;
        }

        /// <summary>
        /// Create a new job in the system.
        /// /// Jobs are ticked at FixedUpdate event.
        /// </summary>
        public IJob RegisterJob(IJob job) {
            _jobs.AddLast(job);
            return job;
        }
    }

    public static class JobSystemHelpers {
        
        public static IJob RegisterJob(this Universe u, IJob j) {
            return u.GetSystem<JobSystem>().RegisterJob(j);
        }
        
        public static T RegisterJob<T>(this Universe u) where T : class, IJob, new(){
            return u.GetSystem<JobSystem>().RegisterJob<T>();
        }

        public static void QueueTask(this Universe u, Action task, float timeDelay) {
            var ts = u.GetSystem<TimeSystem>();
            var job = u.GetSystem<JobSystem>().RegisterJob<LambdaJob>();
            var initialTime = ts.Time; 
            job.OnSuccess( j => task() )
               .OnTick(j => {
                             var currentTime = ts.Time;
                             var deltaTime = currentTime - initialTime;
                             if (deltaTime >= timeDelay) {
                                 j.TriggerAfter(JobTrigger.Success);
                             }
                         });
        }
        
        public static void QueueTask(this Universe u, Action task, ulong tickDelay) {
            var ts = u.GetSystem<TimeSystem>();
            var job = u.GetSystem<JobSystem>().RegisterJob<LambdaJob>();
            var initialTick = ts.Tick; 
            job.OnSuccess(j => task())
               .OnTick( j => {
                              var currentTick = ts.Tick;
                              var deltaTick = currentTick - initialTick;
                              if (deltaTick >= tickDelay) {
                                  j.TriggerAfter(JobTrigger.Success);
                              }
                          });
        }
    }
}