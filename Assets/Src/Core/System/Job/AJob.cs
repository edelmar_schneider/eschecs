using Ce.Core.Message;
using Ce.Core.Util;

namespace Ce.Core.System.Job {

    public abstract class AJob : IJob{

        public JobState State { get; protected set; }

        protected StateMachine<JobState, JobTrigger> Machine { get; private set; }

        protected AJob() {
            
            Machine = new StateMachine<JobState, JobTrigger>((state) => State = state, () => State);
            
            Machine.Configure()
                   .State(JobState.Initial)
                   .Trigger(JobTrigger.Activate, JobState.Activate)
                   .Trigger(JobTrigger.Abort, JobState.Aborted)
                   .Trigger(JobTrigger.Success, JobState.Success)
                   .Return()
                   .State(JobState.Activate)
                   .Trigger(JobTrigger.Abort, JobState.Aborted)
                   .Trigger(JobTrigger.Success, JobState.Success)
                   .Trigger(JobTrigger.Suspend, JobState.Suspended)
                   .Trigger(JobTrigger.Tick, JobState.Tick)
                   .OnEnter((oldState, newState) => OnActivate())
                   .Return()
                   .State(JobState.Tick)
                   .Trigger(JobTrigger.Abort, JobState.Aborted)
                   .Trigger(JobTrigger.Success, JobState.Success)
                   .Trigger(JobTrigger.Suspend, JobState.Suspended)
                   .Trigger(JobTrigger.Tick, JobState.Tick)
                   .OnEnter((oldState, newState) => OnTick())
                   .Return()
                   .State(JobState.Suspended)
                   .Trigger(JobTrigger.Reactivate, JobState.Reactivate)
                   .Trigger(JobTrigger.Abort, JobState.Aborted)
                   .Trigger(JobTrigger.Success, JobState.Success)
                   .OnEnter((oldState, newState) => OnSuspended())
                   .Return()
                   .State(JobState.Reactivate)
                   .Trigger(JobTrigger.Abort, JobState.Aborted)
                   .Trigger(JobTrigger.Success, JobState.Success)
                   .Trigger(JobTrigger.Suspend, JobState.Suspended)
                   .Trigger(JobTrigger.Tick, JobState.Tick)
                   .OnEnter((oldState, newState) => OnReactivate())
                   .Return()
                   .State(JobState.Aborted)
                   .OnEnter((oldState, newState) => OnAbort())
                   .Return()
                   .State(JobState.Success)
                   .OnEnter((oldState, newState) => OnSuccess());
        }
        
        protected virtual void OnSuccess() {
            State = JobState.Success;
        }

        protected virtual void OnAbort() {
            State = JobState.Aborted;
        }

        protected virtual void OnSuspended() {
            State = JobState.Suspended;
        }

        protected virtual void OnTick() {
            State = JobState.Tick;
        }

        protected virtual void OnActivate() {
            State = JobState.Activate;
        }
        
        protected virtual void OnReactivate() {
            State = JobState.Reactivate;
        }

        public JobState Tick() {

            switch (State) {
                case JobState.Initial:
                    Machine.Trigger(JobTrigger.Activate);
                    break;
                case JobState.Tick:
                    Machine.Trigger(JobTrigger.Tick);
                    break;
            }
                        
            return State;
        }

        public virtual void Handle(AMessage message) {
        }

    }

}