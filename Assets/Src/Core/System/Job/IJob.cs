using Ce.Core.Message;

namespace Ce.Core.System.Job {

    public enum JobState {
        /// <summary> Initial state before anything happens </summary>
        Initial,
        /// <summary> Before it can update it needs to be activated </summary>
        Activate,
        /// <summary> After suspension it has to be Reactivate </summary>
        Reactivate,
        /// <summary> Tick will be called multiple time so the Job can perform its task </summary>
        Tick,
        /// <summary> A job can be suspended for a while, it will await certain condition </summary>
        Suspended,
        /// <summary> A job can be terminated prematurely, in this case it ends in the aborted state </summary>
        Aborted,
        /// <summary> A job that reached its natural end terminates in Success </summary>
        Success
    }

    public enum JobTrigger {
        Activate,
        Reactivate,
        Tick,
        Abort,
        Suspend,
        Success
    }

    public interface IJob {
        
        /// <summary>
        /// Check that state of a job
        /// </summary>
        JobState State { get; }
        
        /// <summary>
        /// Needs to be called evey update cycle.
        /// </summary>
        JobState Tick();

        /// <summary>
        /// A priori this is the only way to initiate communication with a Job.
        /// </summary>
        void Handle(AMessage message);
    }

}