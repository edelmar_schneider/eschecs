using System;
using Ce.Core.Message;

namespace Ce.Core.System.Job {

    /// <summary>
    /// Lambda job offers the option to create quick jobs using lambda functions
    /// </summary>
    public sealed class LambdaJob: AJob {
        
        //NOTE: Success, Abort, Suspended are all final states
        private Action<LambdaJob /*Myself*/> _onSuccess;
        private Action<LambdaJob /*Myself*/> _onAbort;
        private Action<LambdaJob /*Myself*/> _onSuspended;
        
        private Action<LambdaJob /*Myself*/> _onTick;
        
        private Action<LambdaJob /*Myself*/> _onActivate;
        private Action<LambdaJob /*Myself*/> _onReactivate;
        
        private Action<LambdaJob /*Myself*/, AMessage /*message*/> _onHandle;
        
        /// <summary>
        /// A LambdaJob allows you to set the state externally. Use it with care.
        /// </summary>
        public void SetState(JobState state) {
            State = state;
        }

        /// <summary>
        /// Set a function to be execute on state enter
        /// <remarks>If you set the function you need to trigger yourself the state change, otherwise the job will remain in the current state</remarks>
        /// </summary>
        public LambdaJob OnSuccess(Action<LambdaJob /*Myself*/> action) {
            _onSuccess = action;
            return this;
        }
        
        /// <summary>
        /// Set a function to be execute on state enter
        /// <remarks>If you set the function you need to trigger yourself the state change, otherwise the job will remain in the current state</remarks>
        /// </summary>
        public LambdaJob OnAbort(Action<LambdaJob /*Myself*/> action) {
            _onAbort = action;
            return this;
        }
        
        /// <summary>
        /// Set a function to be execute on state enter
        /// <remarks>If you set the function you need to trigger yourself the state change, otherwise the job will remain in the current state</remarks>
        /// </summary>
        public LambdaJob OnSuspended(Action<LambdaJob /*Myself*/> action) {
            _onSuspended = action;
            return this;
        }
        
        /// <summary>
        /// Set a function to be execute on state enter
        /// <remarks>If you set the function you need to trigger yourself the state change, otherwise the job will remain in the current state</remarks>
        /// </summary>
        public LambdaJob OnTick(Action<LambdaJob /*Myself*/> action) {
            _onTick = action;
            return this;
        }
        
        /// <summary>
        /// Set a function to be execute on state enter
        /// <remarks>If you set the function you need to trigger yourself the state change, otherwise the job will remain in the current state</remarks>
        /// </summary>
        public LambdaJob OnActivate(Action<LambdaJob /*Myself*/> action) {
            _onActivate = action;
            return this;
        }
        
        /// <summary>
        /// Set a function to be execute on state enter
        /// <remarks>If you set the function you need to trigger yourself the state change, otherwise the job will remain in the current state</remarks>
        /// </summary>
        public LambdaJob OnReactivate(Action<LambdaJob /*Myself*/> action) {
            _onReactivate = action;
            return this;
        }
        
        public LambdaJob OnHandle(Action<LambdaJob /*Myself*/, AMessage /*message*/> action) {
            _onHandle = action;
            return this;
        }

        public void TriggerAfter(JobTrigger trigger) {
            Machine.TriggerAfter(trigger);
        }

        protected override void OnSuccess() {
            base.OnSuccess();
            _onSuccess?.Invoke(this);
        }

        protected override void OnAbort() {
            base.OnAbort();
            _onAbort?.Invoke(this);
        }

        protected override void OnSuspended() {
            base.OnSuspended();
            _onSuspended?.Invoke(this);
        }

        protected override void OnTick() {
            base.OnTick();
            
            if (_onTick == null) {
                Machine.TriggerAfter(JobTrigger.Success);
            }
            else {
                _onTick(this);
            }
        }

        protected override void OnActivate() {
            base.OnActivate();
            
            if (_onActivate == null) {
                Machine.TriggerAfter(JobTrigger.Tick);
            }
            else {
                _onActivate(this);
            }
        }

        protected override void OnReactivate() {
            base.OnReactivate();
            
            if (_onReactivate == null) {
                Machine.TriggerAfter(JobTrigger.Tick);
            }
            else {
                _onReactivate(this);
            }
        }

        public override void Handle(AMessage message) {
            base.Handle(message);
            _onHandle?.Invoke(this, message);
        }

    }

}