using System;
using System.Runtime.CompilerServices;
using Ce.Core.Ecs;

namespace Ce.Core.System {

    /// <summary>
    /// The game should support basic multi-player and all the clients will sync the game based on a clock that is
    /// controlled by this system.
    /// The fundamental unit of time is the tick from which the time of the game can be determined.
    /// You can understand the Tick as a frame number in a film, so each tick is a well defined point and all the clients
    /// will have the same things happening at the same tick. 
    /// </summary>
    public class TimeSystem: BaseSystem {
        
        /// <summary>
        /// Check how many ticks have passed since the beginning of the game. This is reset every time a new game
        /// is initiated or loaded.
        /// </summary>
        public ulong Tick { get; private set; }
        
        /// <summary>
        /// Check how much time (in seconds) have passed since the last time Tick had a reset.
        /// </summary>
        public float Time { get; private set; }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void UpdateTime() {
            Time = Tick * CoreConstants.SecondsPerTick;
        }
        
        public void ResetTicks(ulong newTime = 0) {
            //TODO Call this just before the game starts, so all the clients will start with the same clock     
            Tick = newTime;
            UpdateTime();
        }

        /// <summary>
        /// Have unity update this with its FixedUpdate function.
        /// </summary>
        public void FixedUpdate() {
            Tick += 1;
            UpdateTime();

            FixedUpdateEvent?.Invoke();
        }

        /// <summary>
        /// Have unity update this with its Update function.
        /// </summary>
        public void Update() {
            UpdateEvent?.Invoke(UnityEngine.Time.deltaTime);
        }

        public event Action FixedUpdateEvent;
        
        public event Action<float> UpdateEvent;

    }

    public static class TimeSystemHelpers {
        public static TimeSystem TimeSystem(this Universe u) {
            return u.GetSystem<TimeSystem>();
        } 
        
    }

}