using System;
using System.Collections.Generic;
using System.Linq;
using Ce.Core.Ecs;
using Ce.Core.Message;
using UnityEngine;

namespace Ce.Core.System {


    /// <summary>
    /// The message system will receive messages from the network and distribute it to the local system that are
    /// interested in the message. Messages are not delivered before the correct tick, if the local tick is smaller
    /// than the sender the message will be queued until the correct tick arrives.
    /// </summary>
    public class MessageSystem: BaseSystem {
        
        /// <summary>
        /// Private class used to keep track of system that are interested in some types of messages
        /// </summary>
        private class TargetObject {
            public BaseSystem TargetSystem { get; private set; }
            public Action<AMessage> TargetMethod { get; private set; }

            public TargetObject(BaseSystem targetSystem, Action<IMessage> targetMethod) {
                TargetSystem = targetSystem;
                TargetMethod = targetMethod;
            }
        }
        
        /// <summary>
        /// Systems that subscribed to receive messages are stored here.
        /// </summary>
        private readonly Dictionary<Type, List<TargetObject>> _subscribers = new Dictionary<Type,List<TargetObject>>(2048);
        
        /// <summary>
        /// Clients that subscribed to all messages  
        /// </summary>
        private readonly List<TargetObject> _subscribersToAll = new List<TargetObject>(2048);
        
        /// <summary>
        /// Messages to be handles in the future.
        /// </summary>
        private readonly List<AMessage> _messages = new List<AMessage>();

        private TimeSystem _timeSystem;

        protected override void OnInitialize() {
            base.OnInitialize();
            _timeSystem = Universe.GetSystem<TimeSystem>();
            _timeSystem.FixedUpdateEvent += OnFixedUpdate;
        }

        protected override void OnTerminate() {
            _timeSystem.FixedUpdateEvent -= OnFixedUpdate;
            base.OnTerminate();
        }
        
        /// <summary>
        /// The TimeSystem will be responsible for keep calling this method which will deliver the queued messages.
        /// </summary>
        private void OnFixedUpdate() {
            foreach (var message in _messages.Where(m=>m.Tick == _timeSystem.Tick)) {
                DispatchMessage(message);
            }

            _messages.RemoveAll(message => message.Tick == _timeSystem.Tick);

            //TODO I might want to remove this in the future if this problem never happens.
            if (_messages.Any(m => m.Tick < _timeSystem.Tick)) {
                Debug.LogError("Messages left behind, we might have a problem here.");
            }
        }

        /// <summary>
        /// The entry point for the messages that will be distributed to the systems.
        /// </summary>
        public void ReceiveMessage(AMessage m) {
            
            if (m.Tick > _timeSystem.Tick) {
                _messages.Add(m);
            } else {
                DispatchMessage(m);
            }
        }

        /// <summary>
        /// This will deliver the message to any interested target.
        /// </summary>
        private void DispatchMessage(AMessage m) {
            
            foreach (var tgt in _subscribersToAll) {
                tgt.TargetMethod(m);
            }
            
            var messageType = m.GetType();
            if (!_subscribers.ContainsKey(messageType)) {
                //no one is interested in the message
                return;
            }
            
            var systems = _subscribers[messageType];
            
            foreach (var targetObject in systems) {
                targetObject.TargetMethod(m);
            }
        }
        
        public void SubscribeToAll(BaseSystem targetSystem, Action<IMessage> handler) {
            _subscribersToAll.Add(new TargetObject(targetSystem, handler ));
        }

        public void UnsubscribeToAll(BaseSystem targetSystem) {
            _subscribersToAll.RemoveAll(to => to.TargetSystem == targetSystem);
        }
        
        /// <summary>
        /// Subscribe a method in a system to receive the specified type of message. 
        /// </summary>
        public void SubscribeToMessage<T>(BaseSystem targetSystem, Action<T> handler) 
            where T: IMessage {
            var messageType = typeof(T);
            if (!_subscribers.ContainsKey(messageType)) {
                _subscribers[messageType] = new List<TargetObject>(128);
            }
            
            _subscribers[messageType].Add(new TargetObject(targetSystem, message => { handler((T)message); }));
        }

        /// <summary>
        /// Will unsubscribe the system from all call for the type of message 
        /// </summary>
        public void UnsubscribeFromMessage<T>(BaseSystem targetSystem)
            where T: AMessage
        {
            var messageType = typeof(T);
            if (!_subscribers.ContainsKey(messageType)) {
                _subscribers[messageType] = new List<TargetObject>(128);
            }

            _subscribers[messageType].RemoveAll(to => to.TargetSystem == targetSystem);
        }

        public void ApplyDeltaToTick(ulong deltaTick) {
            foreach (var m in _messages) {
                m.Tick += deltaTick;
            }
        }
    }

}