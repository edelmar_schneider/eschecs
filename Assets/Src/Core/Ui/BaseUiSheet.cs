using Ce.Core.Ecs;
using Ce.Core.System;
using Ce.Core.System.Job;
using UnityEngine;
using UnityEngine.Assertions;

namespace Ce.Core.Ui
{
    /// <summary>
    /// All UI sheets should be instantiated and controlled directly by a system.
    /// </summary>
    public abstract class BaseUiSheet : MonoBehaviour {

        /// <summary>
        /// Set in the inspector so we can enable/disable the canvas itself
        /// </summary>
        public Canvas mainCanvas;
        
        /// <summary>
        /// Set in the inspector to control fade in/out and interaction
        /// </summary>
        public CanvasGroup mainCanvasGroup;

        /// <summary>
        /// Controls how long should it take to fade in/out.
        /// </summary>
        public float fadeInTime;
        /// <summary>
        /// Controls how long should it take to fade in/out.
        /// </summary>
        public float fadeOutTime;

        protected Universe Universe;

        private bool _defaultBlockRay;
        private bool _defaultInteractable;

        /// <summary>
        /// Call this after instantiation to enable de ui sheet for work.
        /// This function should be called only once.
        /// </summary>
        public BaseUiSheet Setup(Universe universe) {
            Universe = universe;
            _defaultBlockRay = mainCanvasGroup.blocksRaycasts;
            _defaultInteractable = mainCanvasGroup.interactable;
            OnSetup();
            return this;
        }

        /// <summary>
        /// Show the UI
        /// </summary>
        public BaseUiSheet Show() {
            Assert.IsNotNull(Universe,"You have to setup the UI sheet before using it");
            OnShow();
            return this;
        }
        
        /// <summary>
        /// Hide the UI
        /// </summary>
        public BaseUiSheet Hide() {
            OnHide();
            return this;
        }

        /// <summary>
        /// Initialize the object as you need it
        /// </summary>
        protected virtual void OnSetup() {
            
        }

        /// <summary>
        /// Override this to control how the ui sheet behave when show is called
        /// </summary>
        protected virtual void OnShow() {
            var tf = 1f / fadeInTime;
            var ts = Universe.TimeSystem();
            var t0 = ts.Time;

            Universe.RegisterJob<LambdaJob>()
                    .OnActivate(job => {
                                    mainCanvas.enabled = true;
                                    mainCanvasGroup.blocksRaycasts = _defaultBlockRay;
                                    mainCanvasGroup.interactable = _defaultInteractable;
                                    job.TriggerAfter(JobTrigger.Tick);
                                })
                    .OnTick(job => {
                                var dt = ts.Time - t0;
                                var t = dt * tf;
                                mainCanvasGroup.alpha = Mathf.Lerp(0f, 1f, t);
                                if (t >= 1f) {
                                    job.TriggerAfter(JobTrigger.Success);
                                }
                            });
        }

        /// <summary>
        /// Override this to control how the ui sheet behave when hide is called
        /// </summary>
        protected virtual void OnHide() {
                        
            var tf = 1f / fadeOutTime;
            var ts = Universe.TimeSystem();
            var t0 = ts.Time;

            Universe.RegisterJob<LambdaJob>()
                    .OnActivate(job => {
                                    mainCanvasGroup.interactable = false;
                                    mainCanvasGroup.blocksRaycasts = false;
                                    job.TriggerAfter(JobTrigger.Tick);
                                })
                    .OnTick(job => {
                                var dt = ts.Time - t0;
                                var t = dt * tf;
                                mainCanvasGroup.alpha = Mathf.Lerp(1f, 0f, t);
                                if (t >= 1f) {
                                    job.TriggerAfter(JobTrigger.Success);
                                }
                            })
                    .OnSuccess(job => { mainCanvas.enabled = false; });
        }

    }
}
