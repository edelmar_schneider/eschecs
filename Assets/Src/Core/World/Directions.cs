namespace Ce.Core.World {


    public enum Directions: int {
        Forward = 0,
        ForwardRight=1,
        Right=2,
        BackwardRight=3,
        Backward=4,
        BackwardLeft=5,
        Left=6,
        ForwardLeft=7
    }

}