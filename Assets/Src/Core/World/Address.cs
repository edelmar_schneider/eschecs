using System;
using System.Globalization;
using System.Xml.Linq;
using Ce.Core.Extensions;
using Ce.Core.Util;
using UnityEngine;

namespace Ce.Core.World
{
    [Serializable]
    public struct Address: IEquatable<Address>, IXmlConversion, IDeepCopy
    {
        public const float AddressPerMeter = 1f;
        public const float AddressLength = 1f / AddressPerMeter;     //0.333...
        public const float HalfLength = AddressLength * 0.5f;      //0.1666...
        public const float AddressHeight = 20.0f;

        public int X;
        public int Y;
        public int Z;

        public static Address Zero => new Address(0, 0, 0);
        public static Address One => new Address(1, 1, 1);
        public static Address Up => new Address(0, 1, 0);
        public static Address Down => new Address(0, -1, 0);
        public static Address Forward => new Address(0, 0, 1);
        public static Address Backward => new Address(0, 0, -1);
        public static Address Right => new Address(1, 0, 0);
        public static Address Left => new Address(-1, 0, 0);
        
        public static Address ForwardRight => new Address(1, 0, 1);
        public static Address BackwardRight => new Address(1, 0, -1);
        
        public static Address ForwardLeft => new Address(-1, 0, 1);
        public static Address BackwardLeft => new Address(-1, 0, -1);        
        
        public static Address NegativeExtreme => new Address(int.MinValue, int.MinValue, int.MinValue);

        object IDeepCopy.DeepCopy() => DeepCopy();

        public Address DeepCopy() => new Address(X,Y,Z);

        /// <summary>
        /// Build a Spot from a 3d coordinate.
        /// </summary>
        public static Address FromPosition(Vector3 pos) {

            var y = pos.y + 0.01f; //Avoid rounding error with floating point.

            var nX = (pos.x / AddressLength).Floor();
            var nY = (y / AddressHeight).Floor();
            var nZ = (pos.z / AddressLength).Floor();

            //Now I need to fix the negative case ... by subtracting 1 I'm ceiling the result which is what we need
            if (pos.x < 0) {
                nX -= 1;
            }
            
            if (y < 0) {
                nY -= 1;
            }
            
            if (pos.z < 0) {
                nZ -= 1;
            }

            return new Address(nX, nY, nZ);
        }


        /// <summary>
        /// Return the center of the Spot;
        /// </summary>
        public Vector3 Pos() {

            var x = X >= 0
                ? X
                : (X * -1) - 1;
            
            var z = Z >= 0
                ? Z
                : (Z * -1) - 1;

            var nX = X >= 0
                ? (x * AddressLength) + HalfLength
                : ((x * AddressLength) + HalfLength ) * -1;
            
            var nY = (Y * AddressHeight);

            var nZ = Z >= 0
                ? (z * AddressLength) + HalfLength
                : ((z * AddressLength) + HalfLength ) * -1;
            
            return new Vector3(nX, nY, nZ);
        }

        public Address(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public XElement ToXml(string elementName)
        {
            return new XElement(elementName,
                new XAttribute("X", X.ToString("0", CultureInfo.InvariantCulture)),
                new XAttribute("Y", Y.ToString("0", CultureInfo.InvariantCulture)),
                new XAttribute("Z", Z.ToString("0", CultureInfo.InvariantCulture))
            );
        }

        public void FromXml(XElement element)
        {
            var xAttribute = element.Attribute("X");
            var yAttribute = element.Attribute("Y");
            var zAttribute = element.Attribute("Z");

            X = int.Parse(xAttribute.Value, CultureInfo.InvariantCulture);
            Y = int.Parse(yAttribute.Value, CultureInfo.InvariantCulture);
            Z = int.Parse(zAttribute.Value, CultureInfo.InvariantCulture);
        }
        
        public int this[int index]
        {
            get
            {
                switch (index)
                {
                case 0:
                    return X;
                case 1:
                    return Y;
                case 2:
                    return Z;
                default:
                    throw new IndexOutOfRangeException("Invalid Location index");
                }
            }

            set
            {
                switch (index)
                {
                case 0:
                    X = value;
                    break;
                case 1:
                    Y = value;
                    break;
                case 2:
                    Z = value;
                    break;                
                default:
                    throw new IndexOutOfRangeException("Invalid Location index");
                }
            }
        }

        public static Address operator +(Address lhs, Address rhs)
        {
            return new Address(
                lhs.X + rhs.X,
                lhs.Y + rhs.Y,
                lhs.Z + rhs.Z
                );
        }
        
        public static Address operator -(Address lhs, Address rhs)
        {
            return new Address(
                lhs.X - rhs.X,
                lhs.Y - rhs.Y,
                lhs.Z - rhs.Z
            );
        }
        
        public static Address operator +(Address lhs, int rhs)
        {
            return new Address(
                lhs.X * rhs,
                lhs.Y * rhs,
                lhs.Z * rhs
            );
        }        
        
        public static Address operator -(Address lhs, int rhs)
        {
            return new Address(
                lhs.X - rhs,
                lhs.Y - rhs,
                lhs.Z - rhs
            );
        }
        
        public static Address operator *(Address lhs, int rhs)
        {
            return new Address(
                lhs.X * rhs,
                lhs.Y * rhs,
                lhs.Z * rhs
            );
        }
        
        public static Address operator /(Address lhs, int rhs)
        {
            return new Address(
                lhs.X / rhs,
                lhs.Y / rhs,
                lhs.Z / rhs
            );
        }

        public bool Equals(Address other) => X == other.X && Y == other.Y && Z == other.Z;

        public override bool Equals(object obj) => obj is Address other && Equals(other);


        public override int GetHashCode() {
            unchecked {
                var hashCode = X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 397) ^ Z;
                return hashCode;
            }
        }

        public static bool operator ==(Address left, Address right) {
            return Equals(left, right);
        }

        public static bool operator !=(Address left, Address right) {
            return !Equals(left, right);
        }

        public override string ToString() {
            return $"{nameof(X)}: {X}, {nameof(Y)}: {Y}, {nameof(Z)}: {Z}";
        }
    }
}