using System;
using System.Globalization;
using System.Xml.Linq;
using Ce.Core.Util;
using Ce.Core.World;

namespace Ce.Game.World
{
    public class BoardParam: IEquatable<BoardParam>, IXmlConversion
    {
        public int Width { get; set; }     //X
        public int Height { get; set; }    //Y
        public int Depth { get; set; }     //Z
        
        public BoardParam() { }

        public bool IsValid(Address address) {
            
            if ((address.Y < 0)||(address.X < 0)||(address.Z < 0)) { return false;}
            if ((address.Y >= Height)||(address.X >= Width)||(address.Z >= Depth)) { return false;}
            
            var id = ToId(address);
            return id < (Width * Depth * Height);
        }

        public int ToId(Address address) {
            return (address.Y * (Width * Depth)) + (address.Z * Width) + address.X;
        }

        public BoardParam(int width, int height, int depth) {
            Width = width;
            Height = height;
            Depth = depth;
        }

        public void CopyDataFrom(BoardParam other) {
            Width = other.Width;
            Depth = other.Depth;
            Height = other.Height;
        }

        public object Clone() => new BoardParam(Width,Height,Depth);
        
        public BoardParam Copy() => new BoardParam(Width,Height,Depth);

        public XElement ToXml(string elementName)
        {
            return new XElement(elementName,
                new XAttribute("Width", Width.ToString("0", CultureInfo.InvariantCulture)),
                new XAttribute("Height", Height.ToString("0", CultureInfo.InvariantCulture)),
                new XAttribute("Depth", Depth.ToString("0", CultureInfo.InvariantCulture))
            );
        }

        public void FromXml(XElement element)
        {
            var xAttribute = element.Attribute("Width");
            var yAttribute = element.Attribute("Height");
            var zAttribute = element.Attribute("Depth");

            Width = int.Parse(xAttribute.Value, CultureInfo.InvariantCulture);
            Height = int.Parse(yAttribute.Value, CultureInfo.InvariantCulture);
            Depth = int.Parse(zAttribute.Value, CultureInfo.InvariantCulture);
        }

        public bool Equals(BoardParam other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Width == other.Width && Height == other.Height && Depth == other.Depth;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((BoardParam) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Width;
                hashCode = (hashCode * 397) ^ Height;
                hashCode = (hashCode * 397) ^ Depth;
                return hashCode;
            }
        }

        public static bool operator ==(BoardParam left, BoardParam right) {
            return Equals(left, right);
        }

        public static bool operator !=(BoardParam left, BoardParam right) {
            return !Equals(left, right);
        }
    }
}