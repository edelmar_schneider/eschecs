using System;
using System.Collections.Generic;
using Ce.Game.World;
using UnityEngine;

namespace Ce.Core.World
{
    public static class AddressUtil
    {
        public static Address DirToAdd(this Directions dir) {
            switch (dir) {
                case Directions.Forward: return Address.Forward;
                case Directions.ForwardRight: return Address.ForwardRight;
                case Directions.Right: return Address.Right;
                case Directions.BackwardRight: return Address.BackwardRight;
                case Directions.Backward: return Address.Backward;
                case Directions.BackwardLeft: return Address.BackwardLeft;
                case Directions.Left: return Address.Left;
                case Directions.ForwardLeft: return Address.ForwardLeft;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        }
        
        public static Vector3 DirToVec(this Directions dir) {
            switch (dir) {
                case Directions.Forward: return Vector3.forward;
                case Directions.ForwardRight: return (Vector3.forward + Vector3.right).normalized;
                case Directions.Right:         return Vector3.right;
                case Directions.BackwardRight: return (Vector3.back + Vector3.right).normalized;
                case Directions.Backward: return Vector3.back;
                case Directions.BackwardLeft:  return (Vector3.back + Vector3.left).normalized;
                case Directions.Left:          return Vector3.left;
                case Directions.ForwardLeft:   return (Vector3.forward + Vector3.left).normalized;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        }
        
        public static ISet<Address> Rectangle(Address bottomLeftCorner, Address topRightCorner) {

            var spots = new HashSet<Address>();

            var blc = bottomLeftCorner;
            var trc = topRightCorner;

            for (var y = blc.Y; y <= trc.Y; y++) {
                for (var z = blc.Z; z <= trc.Z; z++) {
                    for (var x = blc.X; x <= trc.X; x++) {
                        spots.Add(new Address(x, y, z));
                    }
                }
            }

            return spots;
        }

        public static IEnumerable<Address> GetAddressesWithinCircle(Address center, int radius, BoardParam tp) {
            var output = new List<Address>();

            var mid = center.Pos();
            
            foreach (var a in Rectangle(new Address(-radius, 0, -radius),
                                        new Address(radius, 0, radius))) {
                var dist = Vector3.Distance(mid, a.Pos());
                if (dist <= radius) {
                    output.Add(a);
                }
            }
            
            return output;
        }


        /// <summary>
        /// Return all valid addresses in the perimeter of circle.
        /// Using Midpoint circle algorithm
        /// </summary>
        public static IEnumerable<Address> GetAddressesCirclePerimeter(Address center, int radius, BoardParam tp) {

            var output = new List<Address>();
            
            var cI = center.X;
            var cJ = center.Z;
            var cF = center.Y;
            
            var d = (5 - radius * 4) / 4;
            var i = 0;
            var j = radius;

            var adds = new List<Address>(8);

            do {

                adds.Clear();

                adds.Add(new Address(cI + i, cF,  cJ + j));
                adds.Add(new Address(cI + i, cF, cJ - j));
                adds.Add(new Address(cI - i, cF, cJ + j));
                adds.Add(new Address(cI - i, cF, cJ - j));
                adds.Add(new Address(cI + j, cF, cJ + i));
                adds.Add(new Address(cI + j, cF, cJ - i));
                adds.Add(new Address(cI - j, cF, cJ + i));
                adds.Add(new Address(cI - j, cF, cJ - i));
                
                foreach (var add in adds) {
                    if (tp == null) {
                        output.Add(add);
                        continue;
                    }
                    if (tp.IsValid(add)) {
                        output.Add(add);
                    }
                }
                 
                if (d < 0) {
                    d += 2 * i + 1;
                } else {
                    d += 2 * (i - j) + 1;
                    j--;
                }

                i++;
            } while (i <= j);

            return output;
        }

        
    }

}