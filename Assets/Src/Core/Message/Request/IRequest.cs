using Ce.Core.Message.Command;

namespace Ce.Core.Message.Request {

    public interface IRequest: IMessage {

        /// <summary>
        /// The request should be able to turn a request into a command when it makes sense.
        /// </summary>
        /// <returns></returns>
        ICommand MakeCommand();

        /// <summary>
        /// This is how you know if the request can be turned into a command.
        /// </summary>
        /// <returns></returns>
        bool CanMakeCommand();

    }


}