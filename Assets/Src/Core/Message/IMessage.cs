using System.Xml.Linq;
using Ce.Core.Util;

namespace Ce.Core.Message {

    public interface IMessage : IXmlConversion {
        ulong Tick { get; set; }
        
        ulong ClientId { get; set; }

        XElement ToXml(string elementName);

        void FromXml(XElement element);

    }

}