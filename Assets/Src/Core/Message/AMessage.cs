using System;
using System.Xml.Linq;

namespace Ce.Core.Message {

    public abstract class AMessage : IMessage {
        
        public ulong Tick { get; set; }
        
        public ulong ClientId { get; set; }

        public static AMessage Instantiate(XElement element) {
            
            var attribute = element.Attribute("DataType");
            if (attribute == null) {
                throw new Exception("Cannot instantiate AMessage from: " + element);
            }

            var tp = Type.GetType(attribute.Value);
            var instance = Activator.CreateInstance(tp) as AMessage;
            if (instance == null) {
                throw new Exception("Cannot instantiate AMessage from: " +element);
            }
            
            instance.FromXml(element);

            return instance;

        }
        
        public virtual XElement ToXml(string elementName) {
            var myName = GetType().AssemblyQualifiedName;
            if (myName == null) {
                throw new Exception();
            }
            
            var e = new XElement(elementName, 
                                 new XAttribute("DataType", myName),
                                 new XAttribute("ClientId", ClientId),
                                 new XAttribute("Tick", Tick));
            return e;
        }

        public virtual void FromXml(XElement element) {
            
            var tick = element.Attribute("Tick");
            Tick = ulong.Parse(tick.Value);
            
            var clientId = element.Attribute("ClientId");
            ClientId = ulong.Parse(clientId.Value);
            
        }

    }

}