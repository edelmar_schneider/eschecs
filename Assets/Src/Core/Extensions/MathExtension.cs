using System;
using Ce.Core.World;
using Ce.Game.World;
using UnityEngine;

namespace Ce.Core.Extensions
{
    public static class MathExtension
    {
        public const float Pi = 3.141593f;

        public const float TwoPi = 3.141593f * 2f;
        
        public const float Deg2Rad = 0.01745329f;
        
        public const float Rad2Deg = 57.29578f;
        
        
        public static Address ToAddress(this Vector3 v) => Address.FromPosition(v);
        
        public static float CopySign(float val, float sign) {
            val = Math.Abs(val);

            if (sign >= 0) {
                return val;
            }

            return val * -1f;
        }
 
        public static float RadToDeg(this float rad) {
            return rad * Rad2Deg;
        }

        public static float DegToRad(this float deg) {
            return deg * Deg2Rad;
        }
        
        /// <summary>
        /// Is the int Even?
        /// </summary>
        public static bool IsEven(this int self) {
            return self % 2 == 0;
        }

        /// <summary>
        /// Is the int Odd?
        /// </summary>
        public static bool IsOdd(this int self) {
            return self % 2 != 0;
        }
        
        /// <summary>
        /// Returns whether the given value is in (inclusive) range
        /// </summary>
        public static bool InRange(this float val, float min, float max) {
            return (val >= min && val <= max);
        }

        /// <summary>
        /// Returns whether the given value is in (inclusive) range
        /// </summary>
        public static bool InRange(this int val, int min, int max) {
            return (val >= min && val <= max);
        }
        
        /// <summary>
        /// Normalizes a value in the range of [min .. max] to the range of [0.0 .. 1.0]
        /// </summary>
        public static float Normalize(this float x, float min, float max) {
            if (min.NearlyEquals(max) ) return 1.0f;
            return (x - min) / (max - min);
        }
        
        
        /// <summary>
        /// Constrains a given value to a range
        /// </summary>
        /// <returns> The value constrained between [min .. max]</returns>
        public static float Clamp(this float val, float min, float max) {
            return Math.Max(Math.Min(val, max), min);
        }

        /// <summary><see cref="Clamp(float,float,float)"/></summary>
        public static double Clamp(this double val, double min, double max) {
            return Math.Max(Math.Min(val, max), min);
        }

        /// <summary><see cref="Clamp(float,float,float)"/></summary>
        public static int Clamp(this int val, int min, int max) {
            return Math.Max(Math.Min(val, max), min);
        }
        
        /// <summary>Rounds the given double to the nearest int.</summary>
        public static int Round(this double val) {
            return (int) Math.Round(val);
        }

        /// <summary>Lerps to the value represented by the value between the min and max value.</summary>
        public static float Lerp(this float a, float b, float t) {
            return a + ((b - a) * t);
        }

        /// <summary>Lerps to the value represented by the value between the min and max value.</summary>
        public static double Lerp(this double a, double b, double t) {
            return a + ((b - a) * t);
        }

        public static float EaseInQuad(this float a, float b, float t) {
            return a + ((b - a) * t*t);
        }
        
        public static float EaseOutQuad(this float a, float b, float t) {
            return a - (b - a) * (t * (t - 2));
        }
        
        public static float EaseInOutSin(this float a, float b, float t) {
            return a - (b - a) * (float)Math.Cos(Pi * t);
        }
        
        public static bool NearlyEquals(this float a, float b, float epsilon = 0.00001f) 
        {
            var absA = Math.Abs(a);
            var absB = Math.Abs(b);
            var diff = Math.Abs(a - b);

            if (Math.Abs(a - b) < 0.0001f) { // shortcut, handles infinities
                return true;
            } 
            
            if (Math.Abs(a) < 0.0001f || Math.Abs(b) < 0.0001f) {
                // a or b is zero or both are extremely close to it
                // relative error is less meaningful here
                return diff < (epsilon * float.MinValue);
            }
            
            // use relative error
            return diff / Math.Min((absA + absB), float.MaxValue) < epsilon;
        }

        public static bool NearlyDifferent(this float a, float b, float epsilon = 0.00001f)
        {
            return !NearlyEquals(a, b, epsilon);
        }

        public static int Floor(this float value) {
            return (int) value;
        }
        
        public static int RoundToInt(this float value) {
            return (int)Math.Floor(value);
        }
        
        public static int FloorToInt(this float value) {
            return (int) Math.Floor(value);
        }
        
        public static int Mod(this int value, int module)
        {
            var r = value % module;
            return r < 0 ? r + module : r;
        }

        public static int Abs(this int value) {
            return Math.Abs(value);
        }
        
        public static float Abs(this float value) {
            return Mathf.Abs(value);
        }
    }
}