using System;
using System.Collections.Generic;
using System.Linq;

namespace Ce.Core.Extensions
{
    public static class CollectionExtensions {

        public static Queue<TData> ToQueue<TData>(this IEnumerable<TData> list) {
            
            var q = new Queue<TData>();
            
            foreach (var data in list) {
                q.Enqueue(data);
            }

            return q;
        }
        
        public static HashSet<TData> ToHashSet<TData>(this IEnumerable<TData> list){
            
            var q = new HashSet<TData>();
            
            foreach (var data in list)
            {
                q.Add(data);
            }

            return q;
        }

        public static (bool IsSet, T Value) ItemAfter<T>(this IEnumerable<T> input, T target) {
            var enumerator = input.GetEnumerator();

            var found = false;
            while (enumerator.MoveNext()) {
                if (found)
                    return (true, enumerator.Current);
                found = Equals(enumerator.Current, target);
            }

            enumerator.Dispose();

            return (false, default);
        }
        
        public static (bool IsSet, T Value) ItemAfter<T>(this IEnumerable<T> input, Func<T, bool> check) {
            var enumerator = input.GetEnumerator();

            var found = false;
            while (enumerator.MoveNext()) {
                if (found)
                    return (true, enumerator.Current);

                found = check(enumerator.Current);
            }

            enumerator.Dispose();

            return (false, default);
        } 
        
       
        public static (bool IsSet, T Value)  ItemBefore<T>(this IEnumerable<T> input, T target) {
            using (var enumerator = input.GetEnumerator()) {

                // We skip the first one, if we match on that, we 
                if (!enumerator.MoveNext()) {
                    return (false, default);
                }

                if (Equals(enumerator.Current, target))
                    return (false, default);

                var previous = enumerator.Current;

                while (enumerator.MoveNext()) {
                    if (Equals(enumerator.Current, target))
                        return (true, previous);

                    previous = enumerator.Current;
                }
            }


            return (false, default);
        }
        
        public static (bool IsSet, T Value)  ItemBefore<T>(this IEnumerable<T> input, Func<T, bool> check) {
            using (var enumerator = input.GetEnumerator()) {

                // We skip the first one, if we match on that, we 
                if (!enumerator.MoveNext()) {
                    return (false, default);
                }

                if (check(enumerator.Current))
                    return (false, default);

                var previous = enumerator.Current;

                while (enumerator.MoveNext()) {
                    if (check(enumerator.Current))
                        return (true, previous);

                    previous = enumerator.Current;
                }
            }


            return (false, default);
        }
        
        public static T RandomlyPick<T>(this IEnumerable<T> source, Random r) {
            return RandomlyPick(source, 1, r)
                .FirstOrDefault<T>();
        }
        
        public static IEnumerable<T> RandomlyPick<T>(this IEnumerable<T> source, int toPick, Random r) {
            var values = source is List<T> list 
                ? list 
                : source.ToList();

            if (values.Count == 0) yield break;

            for (var i = 0; i < toPick; i++) {
                yield return values.ElementAt(r.Next(values.Count));
            }
        }
        
        
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random rng) {
            if (source == null) throw new ArgumentNullException("source");
            if (rng == null) throw new ArgumentNullException("rng");

            return source.ShuffleIterator(rng);
        }

        private static IEnumerable<T> ShuffleIterator<T>(
            this IEnumerable<T> source, global::System.Random rng) {
            var buffer = source.ToList();
            for (var i = 0; i < buffer.Count; i++) {
                var j = rng.Next(i, buffer.Count);
                yield return buffer[j];

                buffer[j] = buffer[i];
            }
        }
        
        public static T RandomlyPop<T>(this List<T> source, Random r) {
            return RandomlyPop(source, 1, r).FirstOrDefault();
        }
        
        public static IEnumerable<T> RandomlyPop<T>(this List<T> source, int toPick, Random r) {

            if (source.Count == 0) yield break;

            for (var i = 0; i < toPick && source.Count > 0; i++) {
                var idx = r.Next(source.Count);
                yield return source.ElementAt(idx);
                source.RemoveAt(idx);
            }
        }
        
            
    }
}