using System;
using System.IO;

namespace Ce.Core.Extensions
{
    public static class StringExtension
    {
        /// <summary>
        /// Remove the extension of the file if there is a extension.
        /// </summary>
        public static string RemoveFileExtension(this string fileName) {
            return !Path.HasExtension(fileName) 
                ? fileName 
                : Path.ChangeExtension(fileName, null);
        }
        
        public static string GetFileExtension(this string fileName) {
            if (!Path.HasExtension(fileName)) {
                throw new Exception("File {0} has no extension.");
            }

            return Path.GetExtension(fileName)?.Substring(1);
        }

        public static string AppendPath(this string path, string part) => Path.Combine(path, part);
    
    }
}