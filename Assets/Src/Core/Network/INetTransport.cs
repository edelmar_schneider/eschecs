namespace Ce.Core.Network {
    
    /// <summary>
    /// Provide basic network services
    /// </summary>
    public interface INetTransport {
        
        /// <summary>
        /// Provides a way to get/set the connection port of the game server
        /// </summary>
        int ServerListenPort { get; set; }
    }

}