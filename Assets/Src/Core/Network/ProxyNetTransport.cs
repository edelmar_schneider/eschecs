using MLAPI.Transports.UNET;
using UnityEngine;

namespace Ce.Core.Network {

    public class ProxyNetTransport: MonoBehaviour, INetTransport {

        /// <summary>
        /// Use the inspector to set the actual UNetTransport object
        /// </summary>
        public UNetTransport netTransport;

        public int ServerListenPort {
            get => netTransport.ServerListenPort;
            set => netTransport.ServerListenPort = value;
        }
    }

}