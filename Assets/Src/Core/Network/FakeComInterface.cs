using System.Xml.Linq;
using Ce.Core.Message;
using Ce.Core.Message.Command;
using Ce.Core.System;
using MLAPI;
using UnityEngine;

namespace Ce.Core.Network {

    /// <summary>
    /// FakeComInterface works as the regular ComInterface except that it only fakes the network comunication
    /// </summary>
    public class FakeComInterface: NetworkBehaviour, IComInterface {

        /// <summary>
        /// Have the main script set this
        /// </summary>
        public MessageSystem MessageSystem { get; set; }
        
        /// <summary>
        /// Have the main script set this
        /// </summary>
        public TimeSystem TimeSystem { get; set; }

        public void DispatchToServer(string message) {
            DeliverToServerFakeRpc(message);
        }

        public void DispatchToClients(string message) {
            DeliverToClientFakeRpc(message);
        }
        
        #region MessageOutput 
        
        private void DeliverToServerFakeRpc(string message) {
            var xml = XElement.Parse(message);
            var m = AMessage.Instantiate(xml);
            MessageSystem.ReceiveMessage(m);
            Debug.Log("As server I got message: " + xml);
        }
        
        private void DeliverToClientFakeRpc(string message) {
            
            var xml = XElement.Parse(message);
            var m = AMessage.Instantiate(xml);
            
            if (m is ResetTimeCommand) {
                var currentTick = TimeSystem.Tick;
                var deltaTick = (0 - currentTick) + 1; //+1 to ensure it will be execute at least one tick in the future;
                MessageSystem.ApplyDeltaToTick(deltaTick);
                TimeSystem.ResetTicks(0);
            }
            else {
                MessageSystem.ReceiveMessage(m);
            }

            Debug.Log("As client I got message: " + xml);
        }

        #endregion
    }

}