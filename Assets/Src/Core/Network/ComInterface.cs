using System.Xml.Linq;
using Ce.Core.Message;
using Ce.Core.Message.Command;
using Ce.Core.System;
using MLAPI;
using MLAPI.Messaging;
using UnityEngine;

namespace Ce.Core.Network {


    /// <summary>
    /// The ComInterface is the glue between unity's MLAPI for network and the ECS system.
    /// It hosts the RPC functions that allows communication between all the clients and the server.
    /// The ComInterface cannot be a regular System since it has to be an object of the NetworkBehaviour type.
    /// </summary>
    public class ComInterface: NetworkBehaviour, IComInterface {

        /// <summary>
        /// Have the main script set this
        /// </summary>
        public MessageSystem MessageSystem { get; set; }
        
        /// <summary>
        /// Have the main script set this
        /// </summary>
        public TimeSystem TimeSystem { get; set; }

        public void DispatchToServer(string message) {
            DeliverToServerRpc(message);
        }

        public void DispatchToClients(string message) {
            DeliverToClientRpc(message);
        }
        
        #region MessageOutput 

        /// <summary>
        /// Clients submit a request to the server and the server will validate it and send it back to all clients via <see cref="DeliverToClientRpc"/> 
        /// </summary>
        [ServerRpc(RequireOwnership = false)]
        private void DeliverToServerRpc(string message) {
            var xml = XElement.Parse(message);
            var m = AMessage.Instantiate(xml);
            MessageSystem.ReceiveMessage(m);
            Debug.Log("As server I got message: " + xml);
        }

        /// <summary>
        /// The server will use this function to deliver messages to the clients.
        /// </summary>
        [ClientRpc]
        private void DeliverToClientRpc(string message) {
            
            var xml = XElement.Parse(message);
            var m = AMessage.Instantiate(xml);
            
            if (m is ResetTimeCommand) {
                var currentTick = TimeSystem.Tick;
                var deltaTick = (0 - currentTick) + 1;//+1 to ensure it will be execute at least one tick in the future;
                MessageSystem.ApplyDeltaToTick(deltaTick);
                TimeSystem.ResetTicks(0);
            }
            else {
                MessageSystem.ReceiveMessage(m);
            }

            Debug.Log("As client I got message: " + xml);
        }

        #endregion
    }


}