namespace Ce.Core.Network {

    public interface IComInterface {
        
        void DispatchToServer(string message);

        void DispatchToClients(string message);
    }

}